# Kazhnuz.space

Le code source de mon site web/espace personnel numérique. Le but de cet espace est d'être un site static qui corresponde à mes attentes en terme de manière d'écrire et de faire fonctionner un site (notamment avec un workflow git et du markdown).

## Technologies utilisée.

kazhnuz.space utilise les technologies suivantes

- Spectre :: Framework CSS

- Fork Awesome :: Librairie d'icones

- Hexo :: Generateur de site statics

- Docsify :: Generateur de site de documentations.

- Gulp :: Outils pour générer les pages "en dur"

- Shell :: Glue utilisée pour que tout fonctionne ensemble
