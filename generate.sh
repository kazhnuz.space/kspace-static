echo "STEP 1 - Generation des fichiers statics"
gulp

echo "STEP 2 - Generation du blog"
cd src/blog
hexo generate

cd ../../

echo "STEP 3 - Generation de l'espace littéraire"
cd src/textes
hexo generate

# echo "STEP 4 - Générations des livres à télécharger"
# cd ../crowbook
# sh generate-text.sh

echo "STEP 5 - Remplacement dossier public par dossier temp"
cd ../../
rm -rf public
mv dist public
