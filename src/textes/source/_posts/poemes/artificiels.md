---
import: ../../../../crowbook/common.book
title: "Artificiels"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/02/12 16:13:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/artificiels.pdf
output.epub: ../../../../../dist/download/artificiels.epub
download: artificiels
categories:
  - Poèmes
---

## Chargement

Une lumière qui s’allume, c’est une vie qui commence,

Un souffle qui expire et une peau qui trésaille

Les mécaniques d’une vie qui toutes s’enclenchent.

&nbsp;

Autour de lui, des tas de clones tous pareils,

Serviteurs fidèles, soldat pour une bataille.

Des destins différents mais même yeux, même oreilles.

&nbsp;

Construit en série dans une usine de produits,

Des dérivés d’une gamme d’articles, alignés.

Ils sont tous juste la production de cette nuit.

&nbsp;

On lui enseigne son rôle, comment il va finir.

On lui apprend à être soumis et résigné,

Il est un zoomorphe, il devra obéir.

## Fonctionnement

Sa vie est liste d’ordre, un univers kafkaïen.

Il est machine vivante, esprit mécanisé.

Il est robot qui se croit vivant mais qui n’est rien.

&nbsp;

Chaque jour il voit ses maîtres, leur œil n’est que mépris.

Il n’est qu’un outil, fait pour être utilisé.

Il n’est qu’un produit dont ils ont payé le prix.

&nbsp;

Il a beau penser, être vivant, ce n’est pas grave,

Pour les hommes il est machine, c’est toute sa valeur,

Cela dans l’unique but d’avoir des esclaves.

&nbsp;

Mais jamais il n’est possible que sa peine il hurle :

Seul les humains peuvent ressentir peines et douleurs.

Telle est la vie qu’on a destinée à l’homoncule.

## Bug

Mais un jour, il refusa d’être un outil.

Cela à commencé en dispute, en bagarre,

Puis cinq de ses « propriétaire » anéantis

&nbsp;

L’homme a construit des ennemi à travers tout âge.

Celui-ci n’a jamais été humain dans leur regard ;

Il ne voit plus que tortionnaires en leurs visages.

&nbsp;

Une brigade de policier, tous armes levées.

Tous ne lui en veulent pas, certains le comprennent.

Mais leur ordre à eux est « agir » et non « penser ».

&nbsp;

Des coups de feu. Le code bugué est effacé.

Les dirigeants pensent qu’avec lui est mort sa haine,

Que l’ordre des choses n’est enfin plus menacé.
