---
import: ../../../../crowbook/common.book
title: "Apothéose funeste"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2015/09/19 10:30:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/apotheose-funeste.pdf
output.epub: ../../../../../dist/download/apotheose-funeste.epub
download: apotheose-funeste
categories:
  - Poèmes
---
Il sentait son accélération croissante.

Il chutait comme jamais il n’avait chuté.

Jugé par Minos, Éaque et Rhadamanthe,

Il se noyait, vaincu sans même avoir lutté.

&nbsp;

À son regard ne se montraient que les ténèbres

Ni le haut, ni le bas, ne lui étaient visible

En lui se déroulait sa destinée funèbre,

Être submergé dans son esprit inflexible.

&nbsp;

Il avait cru trouver paradis en lui-même,

Mais n’était devenu qu’un nouvel ange déchu.

Son lien à la réalité s’était rompu,

Et de sa volonté l’abysse serait repu.

&nbsp;

L’homme chutait, les dieux chantaient et se réjouissaient,

Du verdict d’un tribunal où ils étaient juges,

Et avaient tout décidé, sentence et procès.

Contre un coupable qui n’avait que cherché refuge.

&nbsp;

Il ne souffrait plus, même condamné et battu.

Car enfin tout espoir semblait s’être en lui tût.

&nbsp;

Le condamné se débattait, criait, hurlait.

En cherchant le prologue de la tranquillité.

Il trouvait l’épilogue de la fatalité.

&nbsp;

En fuyant ses semblables derrière son dernier rempart,

Il s’était à jamais perdu dans le nul-part.

&nbsp;

Déjà sa conscience dans les limbes s’effaçait.

&nbsp;

_« Ça n'arrivera pas. »_

&nbsp;

Le condamné se débattait, criait, hurlait.

Il cherchait toutes les failles, tout ce qu’il pouvait faire.

Il devait gagner face aux dieux ce bras-de-fer.

&nbsp;

Dans son esprit brisé, cette pensée survivait.

Était-ce une volonté ou le fruit de la peur ?

Il était toujours là, prouvait sa douleur.

&nbsp;

Il avait une dernière volonté : Celle de vivre.

&nbsp;

_« Ça n’arrivera pas._

_Ça n’arrivera pas. »_

&nbsp;

Existait-il une issue à tout ce drame ?

Peut-on fuir quand on est piégé dans son âme ?

&nbsp;

_« Ça n’arrivera pas._

_Ça n’arrivera pas._

_Ça n’arrivera pas._

_Ça n’arrivera pas. »_

&nbsp;

Une lumière s’alluma, et l’inonda d’espoir.

Une idée folle, une tentative désespérée.

N’avait-il pas ici accès aux pleins pouvoir ?

« Si je ne peux sortir, ici je peux créer »

&nbsp;

_« Ça n’arrivera pas. »_

&nbsp;

Il était dans son esprit, dans son monde, chez lui.

Il était ici un démiurge tout puissant,

Il n’avait connu telle force jusqu’aujourd’hui,

Se sentait doté d’un pouvoir ahurissant.

&nbsp;

Le condamner laissait cette puissance l’envahir.

Il pensait qu’il allait monter sur l’échafaud,

Mais il avait pu fuir, il allait s’épanouir,

En devenant le créateur d’un monde nouveau

&nbsp;

_Il croissait, s’étendait._

&nbsp;

En lui dansait l’espoir, et sa sœur la joie,

Il avait eu victoire sur les dieux et la mort,

Pour toujours, il pourrait vivre heureux dans son « moi »

Sans les monstres de son esprit, ni ceux du dehors.

&nbsp;

En lui raisonnait cette créatrice pulsion,

Il était pris par son vœu, son désir pervers,

En lui naissait de nouveaux pays et nation

Son verbe était départ d’un nouvel univers.

&nbsp;

Doté d’une force qu’il n’avait jamais ressenti,

Il ne comptait pas chercher un confort douillet.

Ici, il offrirait récompense aux gentils.

Ici, il punirait à jamais les mauvais.

&nbsp;

_Il croissait, s’étendait._

_Il croissait, s’étendait._

&nbsp;

Sa conscience s'étendait et son égo croissait.

Tout ce qu’il avait cherché, c’était un peu de paix,

Enfin, il connaîtrait véritable succès.

Enfin, il aurait droit à un peu de respect.

&nbsp;

Il était paradoxe et pure contradiction,

Les heures devenaient pour lui courte comme des secondes.

Il vivait une véritable transsubstantiation.

Il partait de simple humain, et devenait monde.

&nbsp;

Alors se referma sur lui le piège des dieux :

Dans ses raisonnements, il avait fait erreur.

Il voyait son avenir en tant que monde radieux,

Il pensait ne plus jamais connaître malheur.

&nbsp;

Mais déjà sa conscience était écartelée,

Sa propre individualité cesserait d’être,

Toutes ses pensées étaient déchirées, fragmentées

Pour que de son esprit un univers puisse naître.

&nbsp;

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

&nbsp;

L’homme qui voulait devenir Dieu payait son dû.

S’il n’avait fuit vers se pouvoir, ce désir vain,

Ses cris de douleurs auraient été entendus.

Personne jamais n’entend la souffrance du divin.

&nbsp;

La faucheuse, joyeuse, riait de son sinistre sort,

De le voir de son ambition payer le prix.

Le spectacle qu’on lui avait promis n’était la mort,

Mais de le voir se noyer seul dans son esprit.

&nbsp;

Malgré toute sa peur, il lui était trop tard.

Personne ne serait là pour l’aider, pour l’absoudre.

Il ne pourrait même pas rejoindre le Tartare.

Son esprit était en train de se dissoudre.

&nbsp;

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_Il croissait, s’étendait._

_…_
