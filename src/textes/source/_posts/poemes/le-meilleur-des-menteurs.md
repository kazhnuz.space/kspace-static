---
import: ../../../../crowbook/common.book
title: "Le meilleur des menteurs"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2012/08/31 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/le-meilleur-des-menteurs.pdf
output.epub: ../../../../../dist/download/le-meilleur-des-menteurs.epub
download: le-meilleur-des-menteurs
categories:
  - Poèmes
---
Je suis quelqu'un de bavard

Pas un beau parleur, pas un rhétoricien

Juste un mec qui a toujours aimer parler.

Raconter toute les petites anecdotes,

Petites histoires et blagues idiotes.

&nbsp;

Mais chaque mot que l'on dit

est une centaine d'autres que l'on tait.

Parfois les bavards sont ceux qui veulent dire le moins.

Cacher qui je suis, mes anciennes faiblesses.

Est ce que si les gens me voient autrement,

Je serais alors quelqu'un d'autre, le mirage que je cherche être ?

&nbsp;

Je suis le bavard qui se tait,

Le clown qui cache tout, au masque aux vives couleurs.

Le rigolard malhonnête, l'arnaqueur des relations.

Le menteur par omission qui jamais ne se tait.

&nbsp;

Quand tout va mal, j'ai une blague.

Quand j'ai peur, j'ai une anecdote.

Quand je ne veut pas voir quelqu'un, j'ai une histoire.

Quand je veux crier, j'ai un rire.

Quand je veux pleurer, j'ai un sourire.

&nbsp;

Ma mémoire n'est qu'un gilet pare-balle,

Face à d'autres souvenirs, ceux qui me hantent.

Toutes mes erreurs, toutes mes fautes.

Cruautés de ma part et de celle des autres.

Hontes, Déshonneurs, conneries et truanderies.

&nbsp;

Je suis le meilleurs des menteurs,

Un bon ami, le pire des connards.

Quelqu'un de gentil, un pur salaud.

Dichotomie entre ce que les autres pensent et ce que je sais.

&nbsp;

Peut-être vous me direz d'arrêter,

De me taire et d'oublier le passé.

Mais depuis quand peut on oublier ?

Si je ne m'en souviens pas, si je refoule,

Les cauchemars reviendront me hanter.

&nbsp;

Mais même mes souvenirs ne sont que d'autres personnages.

A une époque ou je trouvais ça cool d'être mauvais.

Et une où je pensais que c'était dans ma nature d'être un raté.

Je veux redevenir moi même... Mais j'ai oublié qui je suis.

Je veux me retrouvé, mais je ne sais pas ou chercher...

&nbsp;

Je suis le bavard qui se tait,

Le clown qui cache tout, au masque aux vives couleurs,

Le rigolard malhonnête, l'arnaqueur des relations,

Le menteur par omission qui jamais ne se tait,

Le meilleurs des menteurs,

Un bon ami, le pire des connards.

Quelqu'un de gentil, un pur salaud.

Dichotomie entre ce que les autres pensent et ce que je sais.
