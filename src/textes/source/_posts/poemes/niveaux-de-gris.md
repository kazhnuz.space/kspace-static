---
import: ../../../../crowbook/common.book
title: "Niveaux de Gris"
lang: fr
version: 0.1.5
license: CC-BY-SA 4.0
date: '2010/11/30 23:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/niveaux-de-gris.pdf
output.epub: ../../../../../dist/download/niveaux-de-gris.epub
download: niveaux-de-gris
categories:
  - Poèmes
---

Le soleil blanc était caché par les ternes nuages opaques qui faisaient s'abattre une pluie grisâtre sur la sombre Capitale, ou la foule compactes d'âmes vêtues du traditionnel noir, teinte sacralisée de pouvoir et des "hommes sérieux", avançait sur le béton.

Marcher, rien d'autre.

Marcher avec des chaussures de sport d'un blanc sali, ou avec de belles chaussures noires lustrées. L'unique but était d'atteindre un morne lieu de travail, avant de manger un repas presque coloré, petit instant de joie dans la journée, pour après reprendre un rythme gris et sombre.

Après, quelques pièces grises passeraient dans les mains, et quelques billets ternis par l'usage rejoindrais les plus chanceux de la population.

&nbsp;

Par contre, dans la sombre soirées, un rare éclatement de couleur accompagne dans les média de joyeuses nouvelles. Le chômage n'a pas augmenté, d'après les courbes d'un bleu sérieux. On parle de quelques initiatives personelles aux vives couleurs chaudes, et du blanc sépulcral d'anciens bâtiments, symboles d'un passé important et révéré. Quelques teintes sépia de nostalgie, rappelant un monde qui aurait été mieux mais qui est perdu. On désigne des coupables d'avoir fait disparaître ces bons souveirs.

Rapidement, le gris revient.

Un peu de rouge vif pour les violences, qui seraient partout. Teintes obscures de l'insécurité, du manque d'argent de l'état. Des "indésirables" aux visages sombres ou livides, des différents, qui sont accusés de ternir une économie, une nation, un monde qui aurait été jadis fleurissant. L'homme serait un loup gris pour l'homme, un ennemi, un compétiteur, derrière tous peut se cacher celui qui va t'agresser. On donne la paroles aux âmes les plus obscures, qui viennent cracher leur haines de l'autre.

Tout ce gris assombrit encore plus les esprits, quand soudain quelques couleurs unique leurs ait promise : l'or de la réussite, l'or régal, une couleur qui les envelopperaient de sa chaleur omniprésente et protectrice.

&nbsp;

Les discours finis, le monde retombe dans les ternes couleurs.

&nbsp;

Je regarde doucement ce cortège de couleurs et non-couleurs s'avancer à mes yeux. Je suis fatigué. Je sens que petit à petit je me ternis.

Je perd ma confiance, mes peurs s'amplifient et se décuplent. On ne parle que du gris, on se contente de m'effrayer avec une supposer noirceur du monde. Chaque nouveau jour, un nouvel enfoncement dans cet enfer monochrome. On me parle de la part de gris de chaque personne « méfie toi de l'autre », qui peut t'attaquer.

« L'enfer c'est les autres » disait Sartre.

Non. L'enfer, c'est ce gris, cette terreur qui vous étreint le cœur, qui vous cause terreurs et pleurs.

&nbsp;

Mais est-ce la vérité ?

Je vois du bleu.

Le bâtiment devant moi est brun.

Ses rebords de fenêtres oranges.

L'herbe est verte.

L'homme même est pleins de couleurs !

&nbsp;

Je me révolte en silence. Je veux des couleurs.

&nbsp;

Rejoins le rang des couleurs, libère ton rouge cœur du noir ambiant. Le monde est sombre, mais il est habité de gens pouvant faire office de lumière. Les problèmes sont réels, mais exigeons une véritable entraide, et surtout, arrêtons de vouloir que nous ne soit exposé qu'une peur, que les pires horreurs, que le plus exceptionnel. Mais il ne faut pas alors que nous n'ayons que la vision positive du monde. Il nous faut le bien et le mal, le bon et le mauvais. Les couleurs et le monochromes, ce n'est qu'ensemble qu'il forment un monde.

&nbsp;

Je veux le rouge du sang, mais également de la volonté.  

Je veux le vert de la maladie, mais également de l'espoir.  

Je veux le bleu du pouvoir autoritaire mais également du ciel.  

Je veux le jaune du soleil.  

Mais je veux aussi le noir des ténèbres, le blanc qui aveugle, le gris morne.  

Je ne veux juste pas d'un monde en niveau de gris.
