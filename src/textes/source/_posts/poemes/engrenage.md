---
import: ../../../../crowbook/common.book
title: "Engrenage"
lang: fr
version: 0.1.2
license: CC-BY-SA 4.0
date: '2010/11/30 21:12:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/engrenage.pdf
output.epub: ../../../../../dist/download/engrenage.epub
download: engrenage
categories:
  - Poèmes
---
Un champ de bataille. Vide de toute vie. Mais pas de toute trace de vie. Des corps gisent sur l'herbe grasse. Ils sont déjà froid. L'herbe est tacheté de sang. Une hirondelle vole dans le ciel. Elle est horrifié par ce macabre spectacle.

&nbsp;

_C'est un mensonge. Elle chante pour le vainqueur qui va ramener la paix. Nous avons marqué une victoire mémorable. Nous avons vu nos courageux soldats se faire assassiner par les sauvages Autres._

_Oser dire que chaque camp est l'autre est une horrible propagande._

&nbsp;

Regardez les, regardez la complainte de celui qui ne veut voir son fratricide, regardez la haine que ces gens vous dans les petites différences qu'ils voient dans un reflet, différence qui deviennent imperfections, imperfections qui deviennent tares.

&nbsp;

_« Je sais que tu penses qu'ils sont pareil. Tu ne vaux pas mieux qu'eux. Ils ont commis des crimes de guerre. Tu ne veux pas me croire ? Pourtant, tu l'a vu aussi bien que moi. Le champs de bataille. La mort. Les corps abandonné, car les Autres nous empêches de récupéré nos morts. Nous ne faisons que justice, en les empêchant d'offrir sépulture à leur misérable race. »_

&nbsp;

– Ils sont comme nous.

– _Ils n'ont rien de commun avec nous._  

– Ils sont notre visage dans le miroir.  

– _Ils sont notre opposé, notre négatif._  

– Ils sont né comme nous, et ils mourront comme nous.  

– _Ils mourront dans la lâcheté, et nous dans la gloire._  

– Ils possèdent une vie comme la notre, aussi précieuse.  

– _Ils seront tous condamné à mort pour crime contre l'humanité._  

– Ils sont humains, comme nous.  

– _Ils ont dénié leur humanité en nous attaquant._

– Ils ont été poussé à la guerre tout comme nous par leurs dirigeants.

– _Ils suivent sottement leurs tyrans, nos rois sont bons_  

– Ils ont la raison, tout comme nous.  

– _Ils n'en ont pas, sinon ils n'aurait pas dénié notre puissance._  

– Ils ne font que continuer le même cycle que nous.  

– _Si tu n'es des nôtres, tu es alors des leurs._

&nbsp;

Tourne, cycle amer de la haine, tourne à jamais...

&nbsp;

_« La roue tourne et t'emporte, tu ne peux réfléchir, maintenant méfies-toi, celui là te regarde, cet Autre qui t'en veux, cet Autre que tu déteste, il est toi tu es lui, cette horreur terrible, par lui vois l'enfer, la souffrance éternelle, elle est en son âme, image dans ton miroir, miroir de ton psyché, tue et dit de les tuer, fuit cette peur de toi même, fuis ce toi que tu ne peux voir que dans ses yeux._

_Et ainsi à jamais, l'engrenage tournera. »_

&nbsp;

Et l'engrenage tourna.

Brisez-le de vos masses.  

En nôtre nom sauvez les.  

En leurs nom sauvez vous.  

&nbsp;

Ils n'ont pas tout perdu.  

Vous n'avez pas tout perdu.  

Evitez le malheur.  

Réparez notre impardonnable échec.
