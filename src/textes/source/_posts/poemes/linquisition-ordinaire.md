---
import: ../../../../crowbook/common.book
title: "L'inquisition ordinaire"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2014/02/08 10:34:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/linquisition-ordinaire.pdf
output.epub: ../../../../../dist/download/linquisition-ordinaire.epub
download: linquisition-ordinaire
categories:
  - Poèmes
---
Une marche paisible dans la rue.  

Une simple envie de prendre l'air.  

De profiter d'un beau temps.  

Une chose simple.  

Qui soudain nous angoisse.

&nbsp;

Une foule d'autres.  

Ombres anonymes.

&nbsp;

Un simple détail qui nous démarque.

&nbsp;

Les remarques et regards qui fusent.  

Ou du moins les entend-on.  

Ou du moins les voit-on.  

( Ou du moins le croit-on ? )

&nbsp;

Nous sommes trop ceci.  

Nous ne sommes pas assez cela.

&nbsp;

Un seul désir subsiste encore.  

Devenir invisible.  

Échapper à jamais à ce regard.


&nbsp;

Le regard de l'inquisition ordinaire.
