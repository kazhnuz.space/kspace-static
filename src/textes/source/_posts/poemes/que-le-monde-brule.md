---
import: ../../../../crowbook/common.book
title: "Que le monde brûle"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2012/08/31 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/que-le-monde-brule.pdf
output.epub: ../../../../../dist/download/que-le-monde-brule.epub
download: que-le-monde-brule
categories:
  - Poèmes
---
Dans la nuit aux milles bruits,

Dans un monde aux cruautés incessante,

Mais bien à l'abri sous mon toit,

Je laisse aller mon imagination,

A un souhait sauvage, rêvasserie barbare.

&nbsp;

Je veux voir le monde brûler,

Les villes partir en cendre.

Mairies, symboles d'un pouvoir,

Églises, tout ce qui est sacré,

Je veux que tout cela soit happé par les flammes.

&nbsp;

Je ne suis pourtant pas un grand rebelle,

Surtout pas un robin des bois modernes,

Je connais les injustices mais m'en préoccupe peu,

Je vis ma vie sans voir celle des autres,

Autres qui me le rendent bien.

&nbsp;

Je veux voir la foule devenir braises,

Chaque personne devenir flambeau

La graisse ne serait plus qu'un inflammatoire

Les vies plus qu'un moyen de nourrir le feu

Qui chaque jour ravagerait un peu plus ce monde.

&nbsp;

Je ne suis pas fou, ne suis pas pyromane.

Je suis même ce que l'on appellerais un homme rationnel.

Même si je veux l'apocalypse sur la Cité :

Les gens ont toujours voulus me refuser la gloire que je mérite,

S'ils ont gagné, ils n'auront qu'une victoire à la Pyrusse.

&nbsp;

Après-tout, pourquoi est-ce que j'aimerais ce monde ?

Il ne m'offre pas ce que je mériterais,

Il ne m'offre pas amour et réussite.

Juste un bon salaire et une vie confortable,

Mais comment s'en contenter quand on mériterait plus ?

&nbsp;

Catharsiques calcinations,

L'humanité n'est que tas de carbone...

Imaginations salvatrices.

Représentations synaptiques de pulsions meurtrière,

Qui n'auront court que dans mon psyché.

&nbsp;

Un monde de flamme, un monde de braise.

Une civilisation qui pensait voir son apogée

Désormais plongée dans la déchéances.

Verrais-je un jour cela ?

Verrais-je le sublime de l'incendie qui emportent vies et foyer ?

&nbsp;

Je regarde avec envie mon briquet.

D'habitude instrument d'un plaisir au gré d'une vie,

Est ce que ma mort lente deviendra pour eux mort rapide ?

Sombrerais-je finalement dans cette folie que j'ai toujours voulu contenir ?

Laisserais-je sortir les pulsions qui sommeille en moi depuis longtemps ?

&nbsp;

Un si bel incendie...

Beauté éphémère

D'une flamme fatale.

&nbsp;

Je ne dois pas sombrer,

Je ne dois pas sombrer.

&nbsp;

Je veux voir le monde bruler,

Les villes partir en cendre.

Mairies, symboles d'un pouvoir,

Églises, tout ce qui est sacré,

Je veux que tout cela soit happé par les flammes.

&nbsp;

Ne pas sombrer...

Ne pas sombrer...

&nbsp;

Je veux voir la foule devenir braises,

Chaque personnes devenir flambeau

La graisse ne serait plus qu'un inflammatoire

Chaque âme de ce monde un moyen de nourrir le feu

Qui chaque jour ravagerait un peu plus ce monde.

&nbsp;

... Ne pas...

... Ne pas...

&nbsp;

Un jour, un homme me tendis la main.

Il me dit les mots que je voulais entendre.

« C'est vrai que c'est injuste, tu pourrais être bien plus »

Il me proposa le pouvoir, il me proposa la puissance.

Pourquoi ne l'aurais-je pas suivi ?
