---
import: ../../../../crowbook/common.book
title: "Le silence"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2013/03/11 10:40:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/le-silence.pdf
output.epub: ../../../../../dist/download/le-silence.epub
download: le-silence
categories:
  - Poèmes
---
Savoir quoi dire n'est pas toujours « difficile ».

Parfois ce n'est pas l'absence de message qui isole.

Parfois, le silencieux n'est pas celui qui ne sait pas.

Parfois, mettre en ordre les mots est difficile.

&nbsp;

Ces jours-là, rien ne sonne juste.

Ces jours-là, les mots perdent leur sens.

Ces jours-là, tout est dissonant.

Comment alors encore parler ?

Comment alors encore rire ?

Comment alors encore même pleurer ?

&nbsp;

Dire à une personne que l'on tient à elle.

*Mais seul le silence règne.*

Dire à une personne qu'elle nous fait souffrir.

*Mais seul le silence règne.*

Vouloir se révolter contre tout les microtyrans du quotidiens

*Mais seul le silence règne.*

&nbsp;

Enfermé dans un mutisme, enfermé dans sa propre conscience.

Les seuls mots dit sont ceux du quotidiens.

Mots tellement répétés et utilisés qu'ils en sont usés.

Est-ce la complexité de nouveau mot qui effraie ?

&nbsp;

Les phrases difficiles sont pourtant simple.

Parfois est-ce juste « oui » ou « non »

Parfois est-ce un « je suis là ».

Mais le mutisme reste. Encore et toujours.

Le mur est invisible mais présent.

La peur est impalpable mais présente

Dans ces mots devenus morts de n'être usés.

&nbsp;

Est-ce l'impression de devoir dire quelque chose d'exceptionnel ?

Est-ce la peur que le message soit mal compris, mal prit ?

Est-ce l'envie que le message soit beau par lui même,

Comme le serait une simple forme vide ?

&nbsp;

Le silencieux lui même ne le sais pas.

Le silencieux n'a que quelques mots résonnants inlassablement.

Le silencieux ne sait que dire, le silencieux est perdu.

&nbsp;

*« Je ne peux pas »*
