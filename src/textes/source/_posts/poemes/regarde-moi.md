---
import: ../../../../crowbook/common.book
title: "Regarde-moi"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2014/02/10 10:37:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/regarde-moi.pdf
output.epub: ../../../../../dist/download/regarde-moi.epub
download: regarde-moi
categories:
  - Poèmes
---
Regarde-moi

&nbsp;

Tu le fais

Je suis là

Tu le sais

Tu m'écoute

Tu regarde

Et tu vois

Tous ces autres

&nbsp;

Je suis là

Tu as peur

Je suis toi

Ton cauchemar

&nbsp;

Et toujours

Murmurant

Devant toi

Tout les mots

Qui te suivent

Te poursuivent

Sans arrêt

Pour détruire

Qui tu es

Effacer

Toute ta vie

&nbsp;

Regarde-moi

Regarde-moi

&nbsp;

Toute ta vie

Regarde-moi

Tu as peur

Murmurant

Tous les mots

« Je suis toi »

Sans arrêt

Tu regardes

Devant toi

Ton cauchemar.

&nbsp;

Tout ces autres

Te poursuivent

Toute ta vie

Regarde-moi

&nbsp;

Regarde-moi

Sans arrêt

Qui es-tu ?

Tout ces autres

Tu le sais

Ton cauchemar

Tu as peur

Je suis toi

&nbsp;

Tu m'écoute

Tu regarde

Tu m'écoute

Tu regarde

Tu m'écoute

Tu regarde

&nbsp;

Murmurant

Tout les mots

Tu le sais

&nbsp;

Murmurant

Ton cauchemar

Tu as peur

&nbsp;

Regarde-moi

Regarde-moi

Regarde-moi

Regarde-moi

&nbsp;

Je suis là

Pour détruire

Tous ces mots

&nbsp;

Le silence

Devant toi

Tu as peur

&nbsp;

Regarde-moi

Regarde-moi

&nbsp;

Toute ta vie

Murmurant

Le silence

Tu as peur

Tu poursuis

Tu le fais

&nbsp;

Effacer

Tous les autres

Pour détruire

Ton cauchemar

&nbsp;

Tu le fais

Tu le fais

Tu le fais

&nbsp;

Regarde-moi

Le silence

&nbsp;

C'est fini

Tu l'as fait
