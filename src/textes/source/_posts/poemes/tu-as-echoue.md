---
import: ../../../../crowbook/common.book
title: "Tu as échoué"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2015/12/07 10:27:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/tu-as-echoue.pdf
output.epub: ../../../../../dist/download/tu-as-echoue.epub
download: tu-as-echoue
categories:
  - Poèmes
---
Tu cours, tu cours, jamais tu ne peux te cacher.

Partout autour de toi, tout n’est que destruction,

Des fragments de ton monde se font arracher,

La réalité est vouée à la damnation.

&nbsp;

La créature s’est réveillée, tout est perdu.

Est-ce Dieu ou le Diable ? Il fut ton ennemi.

C’est le nombre manquant, celui qui t’a vaincu.

Tu étais le héros, brisé par le banni.

&nbsp;

Partout, où que tu sois, un même son qui résonne,

Quelques notes, sans mélodie, sans harmonie,

Le chant d’un monde déchu, où ne reste personne.

&nbsp;

Partout, ne flottent que des spectres, fragment du passé.

Ils hurlent, ils sanglotent, et par tous tu es honni.

Toujours le même reproche, trois mots : « Tu as échoué ».
