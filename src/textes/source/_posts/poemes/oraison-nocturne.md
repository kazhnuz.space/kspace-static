---
import: ../../../../crowbook/common.book
title: "Oraison Nocturne"
lang: fr
version: 1.1.0
license: CC-BY-SA 4.0
date: '2012/08/31 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/oraison-nocturne.pdf
output.epub: ../../../../../dist/download/oraison-nocturne.epub
download: oraison-nocturne
categories:
  - Poèmes
---
Bienvenue dans le monde de la lune

Monde d’étoile et de noir infini,

Monde de la nuit obscure.

Tout est sombre, tout est mystère,

Monde duquel les gens ont peur,

Univers de Croque-Mitaines

Et de monstres sous les lits.

&nbsp;

Mais si la nuit est la période de l’obscure,

C’est paradoxalement le moment de regarder,

Elle découvre le voile du ciel,

Voile cachant les étoiles et les planètes,

Comme si la lumière ne nous permettait qu’une vision du proche

Et le noir offrait à notre œil l’infini.

&nbsp;

Bienvenue dans le monde de la lune,

Monde des rêves et des cauchemars.

Monde plongé dans la brume des songes,

Ces moments que l’on adore ou que l’on exècre,

Nos espoirs les plus fous et nos pires tourments.

Le noir nous fait oublier l’autour un moment,

Pour nous plonger dans notre propre psyché.

&nbsp;

Pour connaître les autres, il nous faut nous connaître,

Autrui est soi et nous sommes les autres.

Si nous ne pouvons nous juger,

Si seul l’œil de l’autre peut nous percer,

A travers la brumes des songes se trouve notre subconscient,

Univers intangibles d’angoisse et de désirs.

&nbsp;

Bienvenue dans le monde de la lune,

Monde des rues vides et sombres,

Monde des balades solitaires.

Un petit vent frais me fait frisonner,

Mais au final, je me sens bien,

Un moment parfait pour réfléchir sur l’infini,

Un moment parfait pour rêver tranquillement.

&nbsp;

Je ne suis finalement pas si déçu d’avoir loupé mon bus

Personne ne m’attend, je suis seul à l’appartement

Je n’ai pas à me presser, aucune obligation.

Pas de fatigue, juste le son des pas.

Je marche à travers tant de lieux connus,

Mais que je redécouvre dans le noir…

&nbsp;

Bienvenue dans le monde de la lune,

Monde des erreurs, des petits malheurs,

Monde des non-dits, des oublis et des retards.

&nbsp;

Ce monde est le miens, j'y ai grandis et vécu.

Les démons du quotidiens avaient gagnés et ont perdu.

C'est un cycle, cela recommencera-t-il ?

Je ne sais pas, mais aujourd'hui, je suis bien.

&nbsp;

Le monde n’est pas une horloge parfaite

Un emploi du temps, de comptable,

ou un et un font tout le temps deux.

&nbsp;

Le monde de la nuit n'est que l'image de celui du jour,

Une image sans le soleil pour nous aveugler.

&nbsp;

Il est enfin temps de parler.
