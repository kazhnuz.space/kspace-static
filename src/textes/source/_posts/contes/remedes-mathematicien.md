---
import: ../../../../crowbook/common.book
title: "Les remèdes du mathématicien"
date: '2016/12/16 17:23:00'
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/remedes-mathematicien.pdf
output.epub: ../../../../../dist/download/remedes-mathematicien.epub
download: remedes-mathematicien
categories:
  - Contes
---

Il y a bien longtemps, vivait dans les contrées de mes ancêtres un jeune mathématicien. Il était vu et considéré malgré son jeune âge et sa trop forte fierté comme un puits de sagesse. Sa science expliquait la pluie et le beau temps. Il prédisait la course des astres, calculait le temps qui passait, et son savoir semblait inépuisables à ses pairs. Des occultes profondeurs de la démonologie aux secrets célestes de la métaphysique, nulle connaissance ne semblait résister à ses discours désinvoltes.

Un jour, on dit que ce mathématicien tomba sur une jeune personne, qui était atteint d’un mal de l’âme qui semblait incurable. Il fut content de pouvoir se rendre utile, parce que cela lui semblait être un mal qu’il avait bien connu. En effet, non seulement il avait beaucoup lu sur le sujet, mais il avait eut une affliction qui lui semblait bien proche. Alors, il lui parla. Rapidement, ils entretinrent une correspondance régulière.

Au début, tout semblait bien se passer. Les belles paroles du savant offrait réconfort et soutient à la personne atteinte du mal. Il semblait être expert, maîtriser totalement la situation, et le calme qu’il offrait face à toute situation semblait contagieux. Mais quelques crises de colères par-ci, quelques disputes par-là, ce fut les deux confiances qui s’ébranlèrent. Le magicien des sciences avait peur de perdre le contrôle, et la personne qui lui avait donné sa confiance commençait à douter de l’efficacité des actions du savant – actions qui tardaient à arriver.

Peut-être fut-ce à ce moment-là que le mathématicien aurait-dû se rendre compte de son erreur et de sa faute. L’erreur d’avoir cru qu’il saurait guérir avec la même facilité qu’il avait à jouer avec les arguments et les mots un mal profond et incrusté. L’âme de son prochain est un sujet bien moins léger que le nombre d’ange qui pouvait tenir sur une tête d’épingle.

Mais le mathématicien avait trop de fierté pour admettre qu’il avait tors. Petit à petit, il perdait le contrôle, et sa confiance en lui. Ses actions étaient erratiques. Un coup il semblait accepté les conseils que lui prodiguait son patient, et un coup il refusait tout en bloc. La science était avec lui et son entêtement, non ? Il ne savait plus quoi faire, mais ne voulait pas l’admettre… ne le pouvait pas ?

Un jour, l’événement inévitable arriva. Il avait perdu le contrôle. Las de son manque de contrôle, et remarquant l’aggravation de sa situation, son patient lui fit signifier que tout était fini. Qu’il n’accepterait plus ses traitements. Le mathématicien accepta cela pourtant bien facilement : cette situation de manque de contrôle lui avait été douloureuse, et il lui était plus simple d’abandonner tout en bloc, et de se dire que c’était juste son patient qui avait refusé son traitement pourtant si efficace.

S’il était triste de voir s’éloigner un être qu’il avait appris à chérir, le mathématicien était au fond de lui soulagé. Il se résolut à ne plus perdre le contrôle : Les sciences de la nature et des anges étaient bien plus simples que les âmes de ses semblables. Il avait peur d’échouer à nouveau, et se refusait de prendre le risque. N’aurait-ce pas été plus simple d’accepter d’écouter les autres ? Mais la solution ne lui vint même pas. Il s’éloigna alors petit à petit de tous ces semblables, et ne prodiguait que quelques banalités quand on lui demandait de l’aide dans les affaires de l’âme et du cœur. Ce n'était plus un domaine dans lequel il manquait de connaissance : c'était devenu un domaine indigne d'intérêt.

Et on dit qu’il ne resta au mathématicien que son orgueil pour seule compagnie.
