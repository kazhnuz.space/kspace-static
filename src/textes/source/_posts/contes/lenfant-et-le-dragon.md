---
import: ../../../../crowbook/common.book
title: "L'enfant et le dragon"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2018/06/12 23:38:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/lenfant-et-le-dragon.pdf
output.epub: ../../../../../dist/download/lenfant-et-le-dragon.epub
download: lenfant-et-le-dragon
categories:
  - Contes
---

*Quelque part dans les contrées où les fées et les lutins continuent de se montrer à la vue de tous se trouvent une ville, Carillon. Avec ses murs d’une blancheur éclatante et ses toits d’un bleu profond, cette cité perchée sur les falaises tient son nom des cloches qui sonnent perpétuellement, faisant de chaque journée une mélodie nouvelle et envoûtante. Cependant, un jour par semaine, elle se taisent, faisant le silence.*

*Bien souvent les voyageurs étonnés s’enquissent de l’origine de ce troublant silence – est-ce le souvenir d’une tragédie, est-ce un deuil qui force le respect d’un mystérieux mutisme à cette métropole de mélodie ? À cela les habitants leur répondent qu’il s’agit d’un jour de fête, le jour de l’enfant. Ce jour est le jour où le questionnement enfantin sauva toute la ville d'un dragon. Ce jour est le jour qui rappelle aux puissants de la ville que bien des choses les dépassent.*

*Et après cela, ils racontent cette intrigante histoire :*

---

« Jadis, les cloche de notre cité sonnaient sans cesse, et jamais un seul jour ne s’arrêtaient. Elles sonnaient la paisibilité de notre vie, le faste de nos récoltes et la clémence de notre climat. Notre puissance effrayait nos ennemis, et nos murs immenses montraient que nous étions imprenable.

Cependant, un jour, une menace arriva dans notre ville, une menace venu du ciel. Un dragon, au regard de cendre et au cri de fureur tomba du ciel et nous attaqua à l’intérieur même de nos murs. Il terrorisait les passants. Son souffle était brulant et son cri terrifiant, et nous perdimes bien des combattants, ce jour là. Mais nous n’arrêtames pas nos cloches, nous ne voulions pas céder à la peur, abandonner notre façon de vivre. Nous voulions montrer notre force. Il sembla disparaitre le soir tombé, nous laissant dans une nuit d’incertitude.

Le lendemain hélas, il attaqua de nouveau. Il n’était pas parti, mais se déplaçait dans le dédale des ruelles. Alors nous montâmes une équipe pour aller le traquer. Nul de revint. Un nouveau quotidien de peur et de mort commença alors. Les soldats ne réussissaient à vaincre le dragon, et celui-ci semblait ne pas vouloir sortir. Mais toujours les cloches sonnaient, donnant courage et volonté aux habitants et aux chevaliers. Nous gardions foi grâce à leur mélodie.

L’enfant d’un magistrat, alors émit une hypothèse sur le dragon :

— Et si le dragon au souffle brulant et au cri terrifiant était celui qui avait le plus peur ? Nos cloches sonnent fort et nos murailles sont grandes, peut-être se croit-il piégé dans notre cité ?

Mais personne ne voulait croire en l’idée d’un dragon qui avait peur.

Alors le roi décida que de simple guerrier ne suffirait à occir la bête. Les uns après les autres, il appella les cavaliers les plus nobles et courageux du royaumes. De pieux et preux chevaliers, qui sauveraient Carillon du souffle du dragon.

Le chevalier Alembert, à la monture resplendissante, répondit à l’appel et s’en vint s’agenouiller devant le roi. « Je viens, monseigneur, sauver cette ville et votre royaume. Je m’en vais occir ce reptile et offrir sûreté aux habitants de votre cité ». Dans un grand galop, il s’attaqua au dragon. Mais celui-ci le broya immédiatement de ses puissantes pattes.

Le baron Hadrien, au courage incroyable, répondit à l’appel et s’en vint s’agenouiller devant le roi. « Je viens, monseigneur, sauver cette ville et votre royaume. Je m’en vais occir ce reptile et offrir sûreté aux habitants de votre cité ». Sans la moindre peur, il s’attaqua au dragon. Mais celui-ci le déchiqueta immédiatement de sa terrible mâchoire.

Le comte Rembert, aux pouvoirs anciens, répondit à l’appel et s’en vint s’agenouiller devant le roi. « Je viens, monseigneur, sauver cette ville et votre royaume. Je m’en vais occir cet animal et offrir sûreté aux habitants de votre cité ». Psalmodiant d’étranges incantations, il s’attaqua au dragon. Mais celui-ci l’écrasa immédiatement avec sa queue aux écailles dures comme le diamant.

Le duc Eusèbe, à l’épée étincelante, répondit à l’appel et s’en vint s’agenouiller devant le roi. « Je viens, monseigneur, sauver cette ville et votre royaume. Je m’en vais occir cette bête et offrir sûreté aux habitants de votre cité ». Brandissant sa lâme, il s’attaqua au dragon. Mais celui-ci le découpa immédiatement avec ses griffes acérées.

Le prince Amaury, à l’armure indestructible, répondit à l’appel et s’en vint s’agenouiller devant le roi. « Je viens, monseigneur, sauver cette ville et votre royaume. Je m’en vais occir ce monstre et offrir sûreté aux habitants de votre cité ». Sûr de son invulnérabilité, il s’attaqua au dragon. Mais celui-ci le carbonisa de son souffle flamboyant.

Pendant que les plus grand guerriers affrontaient le dragon, l’enfant avait petit à petit tenté de parler aux moines des églises. Après le décès du prince, ceux-ci acquiescèrent à l’idée d’arrêter les cloches.

Et un beau jour, un mardi, il n’y eut plus aucun son dans la ville.

Tous, hommes, femmes, enfants, bêtes, tous découvraient parfois pour la première fois un monde en silence. Le dragon releva la tête. Son regard semblait moins fou, et il ne soufflait ni ne criait plus. Les derniers guerriers se préparèrent à l’achever, quand l’enfant passa devant eux. Des habitants poussèrent des cri d’effroi, à l’idée du sort que la bête allait faire à l’enfant. Le dragon allait-il broyer, déchiqueter, écraser, découper, carboniser l’unique personne qui avait pensé qu’il n’était pas une bête sauvage ?

L’enfant s’arrêta, loin du dragon, et resta debout, droit comme un piquet.

— Les cloches ne font plus de bruit, tu peux partir. Je ne tenterais pas de t’en empêcher ou de te faire de mal.

Le dragon regarda autour de lui. Son regard rougeoyant se posa sur l’être fragile qui se tenait devant lui. Le temps semblait comme arrêté, tous retenaient leur souffle. Brutalement, le dragon s’envola, et disparu dans le ciel.

La ville était sauvé.

L’enfant reçu les honneurs, mais ne vécu qu’une vie simple, en dehors des murailles de la ville. On raconte que dans son age avancé, le dragon vint chercher l’être qui l’avait épargné, avec la proposition de venir vivre de très longues dernières années avec eux. »

*La légende qu’encore aujourd’hui, celui ou celle qui restera doux et silencieux pourra traverser la colonie du dragon, pour trouver une mystérieuse personne, à l’âge très avancé mais à la santé de fer, qui vous expliquera que même les nobles et fiers dragons peuvent avoir autant peur de vous que vous avez peur d'eux, et que parfois il faut savoir ne plus être puissant et effrayant pour pouvoir sauver ceux à qui nous tenons.*
