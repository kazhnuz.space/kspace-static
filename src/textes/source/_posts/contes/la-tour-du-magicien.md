---
import: ../../../../crowbook/common.book
title: "La tour du magicien"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/08/31 12:15:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/la-tour-du-magicien.pdf
output.epub: ../../../../../dist/download/la-tour-du-magicien.epub
download: la-tour-du-magicien
categories:
  - Contes
---
_Il était cette histoire. On la nommait celle de la Tour du Magicien. Elle se racontait dans les plus haut cercles de la société très fermé de ceux qui connaissait l’existence de l’autre monde. Elle était supposé y cacher la plus grande des armes. Le plus grand des secrets. On disait qu’elle contenait le but ultime de la vie de ceux qui connaissaient l’existence des forces magiques._

_On disait aussi qu’il fallait faire le plus grand des sacrifices pour y l’obtenir._

Depuis longtemps existent dans nos contrées des monastères cachés. Ils étaient là pour offrir une retraite loin du monde à ceux qui étaient initiés à l’existence de l’autre monde et des forces qui en provenait. Les jeunes moines y apprenaient à devenir les gardiens du Grand Mystère, à protéger la Terre du surnaturel, et le surnaturel de la Terre.

Un jeune de ces moines, bercé dans les secrets et les apprentissages, désirait connaître. Il voulait savoir toujours plus. Et des connaissances que son insatiable curiosité convoitait, la plus grande était le contenu de la Tour du Magicien. Il désirait savoir qui était le magicien. Quel était ce secret. Pourquoi il était aussi grand, pourquoi était il retiré du monde. Était-il dangereux ? Rien n’était plus attrayant qu’un secret dangereux.

Entraîné par le torrent de la curiosité, il s’était enfuit une nuit de son monastère, vers la porte qui menait à la retraite de cette tour. N’écoutant que son désir de savoir, et nullement les recommandations à la prudence, il répétait l’erreur que bien des jeunes gens, avide de pouvoir, avait fait avant lui.

À travers le monastère, il était arrivé dans la plus grande forêt cachée. Jamais nul des peuples magiques n’avait réussi à la dompter. Les âmes s’y perdaient, les vies se brisaient face aux monstres qui la hantait.

Le jeune moine entra dans la forêt, et marcha pendant des heures. Devant lui se présenta une araignée géante, reine des arachnide. Elle s’avança et lui parla de sa voie froide et rêche.

— Que viens tu faire dans ce bois, petit homme. Ne sais-tu pas le sort qui t’y attends ?

— Je viens découvrir le plus grand des secrets, je viens découvrir ce que recèle la tour du magicien.

Et alors l’araignée s’éloigna. Elle semblait avoir peur. Comme si elle ne voulait interférer avec une telle mission.

— Je ne veux pas partager ne serait-ce qu’une seconde du destin de celui qui vivra les deux morts. Fuis ses bois, si jamais tu tiens à tout ce qui est plus cher que la vie.

Et le jeune moine repris sa route. Il croisa d’autre créatures monstrueuses, mais toutes étaient effrayé par le destin du jeune homme, comme si la fatalité de ses actions pouvait se reporter sur elles.

— La tour du magicien n’est pas un endroit interdit, lui expliqua une liche. Tout le monde peut y aller. Seulement, quand on y entre, il faut accepter le chemin sur lequel on s’est avancé. Sais-tu au moins ce que tu cherches, en ce secret ?

Le jeune garçon s’arrêtait un moment. Il regarda la créature qui se tenait devant lui. Il attendit un moment, et lui donna la réponse

— Je veux juste savoir quel secret peut être assez terrible pour qu’on estime que l'interdire où le détruire n'est pas suffisant pour décourager ceux qui veulent le découvrir.

La liche s’éloigna du jeune garçon.

— Se poser la bonne question est une bonne chose. Mais encore faut-il la comprendre, pour savoir si on veut vraiment trouver la réponse. Bien des gens, alors qu’ils comprenaient même les Arcanes, les cinq grandes sources de la magie, n’ont pu survivre à cet entre du démon

À la fin de son périple, il voyait l’entrée de la tour. Cependant, à se pied, un crâne. Et juste à côté, les ossements du bras. Une personne avait tenté de se traîner jusqu’ici, et était mort. Le jeune garçon regarda alors autour de lui, et vit ce qui entourait la tour. Des centaines de squelettes, de cadavres. Le jeune garçon dégluti, et s’avança. Il entra dans la tour.

Son envie de savoir le poussait à aller affronter cette épreuve.

Cependant, quand il entra dans la tour, rien. Il n’y avait même plus de corps. Avait-ils tous eut le droit de ressortir, ou était-ce une puissance qui détruisait tout ceux qui entraient sans être méritant ? Mais aucun piège ne sembla s’enclencher, aucun monstre ne vint l’arrêter alors qu’il gravit l’escalier en colimaçon de la tour. Et tout en haut, en son sommet, se trouvait au cœur de la pièce, ce qu’il cherchait : un simple cahier à la couverture noire et unie, un simple cahier abîmé par le temps. Ses pages étaient jaunies, couvertes d'inscriptions dans une langue que personne ne semblait comprendre.

Le jeune moine tourna les pages. Il essayait de comprendre. Rien d’autre ne se trouvait dans cette pièce. Était-ce inscrit dans ce cahier les plus terribles secrets magiques existant ? Une pierre philosophale ? Une formule pour dépasser les quatres arcanes, sources de la magie ? Les inscriptions du cahier semblait comprendre de nombreux alphabets, mélangés dans des mêmes mots incompréhensibles. Certains mots n’étaient en fait que le même son, inscrit dans le plus d’alphabet possible. Était-ce alors un véritable texte ?

Était-ce une vaste farce macabre ? Une mise en scène, pour punir celui qui croiraient en un secret si puissant qu’il permettrait de vaincre toute les armes ? En effet, combien d’expression ne disaient-elles pas que les livres avaient un pouvoir plus grand que les armes ? Que l’érudit à la force de vaincre toutes les armées. Tout cela était malheureusement faux. En maniant les phrases, en pensant que ce qui est plus beau est plus grand, peut-être qu'on peut penser que c'est vrai… Mais malheureusement, bien des grands esprits, bien des âmes pures furent réduits au silence par le feu des armes et des batailles. Ce serait alors une moquerie, une vengeance de tous les brillants esprits détruit par la guerre. Un livre qu’on ne pouvait comprendre, parce qu’il n’y avait plus ceux qui pouvaient l’expliquer.

Cependant, la puissance magique se sentait dans se livre. Peut-être était-ce seulement un leurre, pour cacher la véritable nature du livre. Le secret n’était pas dans le livre, mais avait la forme du livre. Un livre incompréhensible. Une épreuve. Fallait-il comprendre le livre, où comprendre ce qu’était le livre pour le trouver ? Le jeune moine se demanda combien de guerrier venu chercher une arme capable de vaincre tous leurs ennemis était parti en voyant le livre, et qu’il était impossible à comprendre.

Où peut-être était-ce un avertissement. Ce secret n’entraînera que la confusion. Le jeune homme se concentra. S’il avait raison, le livre devait être un simple sceau. Et ce qu’il fallait faire pour découvrir le secret, c’était ouvrir le sceau.

— Très bien, livre. Je suis prêt à découvrir le secret.

Il brisa le sceau.

En apparence, rien semblait se passer. Cependant, le jeune moine se sentit tomber dans un abysse. Le monde venait de cesser de faire sens. Un tas d’idée lui entrait dans la tête en même temps, des vérités sur le monde. Des informations qui semblent surgir de nulle part, une fois le sceau qu’étais le livre rompu. Cependant, il problème l’empêchait de comprendre toutes ces informations. Elles étaient toutes contradictoires. Toutes ses croyances se révélaient vrai, même celles auxquelles il n’avait jamais cru. Cependant, elles était aussi fausse. Toute, sans exception. Même l’idée que le vrai et le faux était contradictoire devenait à la fois vrai et faux. Toutes les lois de la magie, de la morale, s’étiraient dans une sensation incompréhensible pour le jeune homme. Tout faisait sens, mais un sens qui n’était que le chaos des informations et des entités. Tout n’était plus que paradoxe. Il était submergé de visions d’objets qui étaient présent et absent à la fois, il se voyait à la fois jeune, bébé, vieux, homme, femme, neutre, riche, pauvre, fort, faible, petit, grand, existant, inexistant, être physique, être d’esprit, sexué, assexué… Il ne voyait plus rien, où plutôt ne pouvait retrouver la véritable image dans l’infini d’image vrai et fausses à la fois. Il voulait partir, il voulait fuir. Mais une petite voix en son sein lui disait de rester. Il ne devait pas partir.

Il ne se souvint jamais combien de temps il resta dans cet état. Obligé d’utiliser toute sa volonté pour ne pas s’enfuir et retourner vers un univers cohérent. Obligé de rester à une envie de fuir qui face à cette douleur qui lui brûlait l’esprit, comme si son âme même allait être détruite par le feu du chaos. Cependant, après un moment, il comprit. Il comprit ce qu’était ce livre, pourquoi il vivait cela. Et pourquoi les gens en mourrait. Et pourquoi les gens étaient tués pour cela. Il avait déjà lu cette histoire.

_« Il y a cette histoire, avec cette caverne. Des hommes y sont enchaînés. »_

Il tenta de marcher, il devait retrouver son chemin.

_« Ils n’ont jamais vu la lumière du jour. Jamais directement. Tout ce qu’ils voient, depuis le fond de leur grotte, ce sont des ombres d’eux-même et des choses, projeté par les quelques rayons du soleils qui parviennent jusqu’à eux. Des sons, ils ne connaissent que quelques échos, de temps en temps. »_

Il chercha le piédestal du livre de ses doigts, toujours aveuglé. Il voulait se remettre debout.

_« Qu’est-ce qu’il se passe, si l’un d’entre eux est libéré, et qu’on l’amène jusqu’à la sortie ? »_

Il ne réussi pas à se remettre debout. Il resta alors assis, dos contre le piédestal.

_« Si on l’amène jusqu’à la sortie, alors il sera ébloui, ses yeux seront brûlés par une lumière bien plus forte que tout ce qu’il aura jamais supporté auparavant. Il souffrira de ces changements. Il voudra résister, et ne pourra percevoir ce qu’il y a devant lui. Il voudra retourner à son état initial. »_

Il tenta de reprendre son souffle.

Ce qu’il voyait, c’était le _vrai monde_.

_Le paradoxe_.

Le monde n’était pas cohérent, mais rempli de moments qui n’obéissait pas aux lois de la logique. De moments impossibles. Et de cela naissait la magie. _Ex contradictione sequitur quodlibet_. D’une contradiction, on peut déduire ce qu’on veut. Il avait appris ça. Une loi de logique classique qui disait que quand une série de proposition contenait une contradiction, on pouvait déduire n’importe quel énoncé de cette contradiction.

Et des contradiction de la réalité, permanente et omniprésente, pouvait naître n’importe quoi. Mais comment pouvait-elles exister ? Cette question lui restait encore sans réponse. La magie. La puissance de transformer le monde en outrepassant les lois visible de la nature.

_« S'il persiste, il s'accoutumera. Il pourra voir le monde dans sa réalité. »_

On lui avait appris qu’il y avait cinq arcanes de la magie. Le mouvement, la matière, l’esprit, la vie et le pouvoir. On lui avait dit que c’était les sources de la magie. C’était faux. Les arcanes n’étaient que la manière dont on avait réussi à comprendre cette puissance de transformation et à plus où moins la maîtriser. Elles n’étaient qu’une métaphore. Les êtres magiques puissants puisaient directement dans l’énergie du Paradoxe. Cependant, s’il en comprenait le principe du Paradoxe, sa nature profonde lui échappait.

_« S’il retourne auprès de ces semblables, ceux-ci seront incapable d’imaginer sa transformation, accomplie ou non. Ils le recevront très mal. Ils refuseront de le croire. »_

Le jeune moine se relevait difficilement. Il avait toujours le souffle court, mais il commençait à retrouver la vue. Il voyait maintenant que la pièce était rempli de bibliothèques, pleines de livres dont des feuilles dépassaient des pages. Il comprenait l’idée de base, mais une grande partie du reste des informations qu’il avait reçu le dépassait encore. Quelques uns avaient fuit avant de comprendre, d’autres étaient sorti pour partager leur savoir au monde entier.

Il comprenait pourquoi il avait vu tant de corps qui sortaient de la tour.

_« Ceux-ci, ayant peur d’une vérité qui changerait tout leur monde, incapable d’imaginer ce qu’il a vu… Ne le tueront-ils pas ? »_

Les monstres avaient eut peur d’empêcher une personne d’aller vers la tour, de peur que ce qu’il y avait dedans les veuillent eux à la place. Cependant, une fois que la personne sortait, ils avaient encore plus peur qu’elle corrompe le monde avec ce qu’elle avait vu. Il était donc condamner à rester ici. Il était enfermé ici. Le retour lui était impossible.

Désormais, le jeune moine savait qui était le magicien. Il n’était pas un individu en particulier. Il est le chercheur, il est celui qui veut découvrir la vérité sur les forces du monde. Il avait eut accès à des tas d’information dont il n’avait compris que les bases , et tout ce qu’il pouvait faire, c’était de tenter de comprendre.

Il avait désormais l’éternité pour essayer de mettre du sens sur ce qu’il avait vu. Sur le monde. Sa récompense était sa punition. Sa récompense était la possibilité du savoir, le début d’une nouvelle quête, et l’éternité pour y accéder. Sa punition était de vivre l’éternité enfermé ici, jusqu’à ce qu’une autre personne arrive pour le rejoindre. Il ne pourrait revoir ceux à qui il tenait.

Il s’assit.

Il prit un des ouvrages de note.

Il aurait besoin de s’occuper.
