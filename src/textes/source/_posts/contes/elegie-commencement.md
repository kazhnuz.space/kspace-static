---
import: ../../../../crowbook/common.book
title: "L'élégie du commencement"
date: '2016/04/02 10:23:00'
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/elegie-commencement.pdf
output.epub: ../../../../../dist/download/elegie-commencement.epub
download: elegie-commencement
categories:
  - Contes
---

*Enfant, toi qui écoutes mes mots, entends la mise en garde de l’aède. Laisse-moi te conter l’histoire de l’origine du monde, et l’avidité des êtres qui jamais ne voulurent connaître mort ni peine. Les anciens ont toujours dit que la vie est une éternelle ronde, qu’il va de soi que naissent et meurent les êtres, les peuples, les terres et les mondes. Mais le trépas du premier monde ne fut causé ni par la volonté de la nature, ni par le destin. Enfant, laisse-moi te conter l’histoire du premier peuple.*

Il y a très longtemps, dans le premier monde né du Grand Commencement, un peuple très avancé décida de vaincre la maladie, la peur et la mort. Mais également d’acquérir la puissance, de devenir capable de combattre la fatalité même. Ils bâtirent alors une machine, un cerveau qui leur permettrait de tout contrôler, et de ne jamais mourir. C’était une création telle, si incroyable qu’elle transcendant la réalité même, qu’elle changeait ce qui était et réécrivait le réel. Tout cela leur permettait d’acquérir l’immortalité.

Mais en faisant cela, ce fut les graines de leur propre destruction qu’ils avaient semés.

Cette puissante création se nommait « cœur des mondes » et fondait son pouvoir sur la source de toute chose : Un phénomène plus vieux que les univers eux-mêmes, nommé le Paradoxe. Il était la vérité et le mensonge à la fois, le possible et l’impossible. Toujours, en tout instant, tout était vrai et faux. Il était caché dans un monde-bulle, afin d’être à jamais protégé. Grâce à cette puissance, ils régnèrent, commandèrent, et envahirent les peuples d’un grand nombre de terres. Ils devinrent proches des dieux, usant de ce pouvoir, reconstruisant le réel à leur bon vouloir.

Mais ils se sentaient limités par leur pensée et voulaient pouvoir fuir leur matérialité. Même toute l’éternité ne leur suffisait plus. Seule une dernière chose leur semblait digne d’être voulue : L’infinité, le pouvoir absolu. Sans relâche ils cherchèrent, des siècles et des millénaires durant, le plus grand des trésors. Et un jour, enfin, ils trouvèrent la transcendance.

Tous devinrent un. Un être étrange, à la fois l’un et le multiple, étendu à l’infini. Il était le peuple entier, et à la fois un individu unique. Il était une contradiction, mais restait toujours parfaitement en harmonie. Sa conscience s’étendait sur toutes les dimensions : La hauteur, la largeur, la longueur, le temps et les possibles. Il pouvait savoir tout ce qui était, serait et avait été, mais aussi pourrait être un jour, aurait pu être et pouvait être.

— « Enfin je suis vraiment, enfin je sais vraiment, enfin je vois vraiment. Connaissance, tu es seule ce qui est véritablement beau pour moi. »

Il était au plus haut, Icare face au soleil. Et vous, mes enfants, vous connaissez déjà la chute. Face à sa toute puissance, un grand mal le rongea, et du ciel il retomba. Et ce mal était le plus grand des maux : La corruption. Une étrange maladie née du paradoxe lui-même, qui affectait l’Être même. Et nul remède existe pour soigner le réel. Cette maladie rongea petit à petit l’être. L’harmonie fut rompue. Et alors il chuta, et alors il sombra.

—« Faim peur où moi douleur rien vie pourquoi présent heureux nourriture aide toi faible joie mort passé tout est quoi nous tout »

Ni instinct, ni discours ne le faisaient se mouvoir. Il n’était plus qu’une musique dissonante qui emplissait chaque esprit, une partition déchirée, un ensemble incohérent d’esprit fragmenté. Il ne pouvait plus penser, chaque fragment de pensée détruisant toutes les autres pensées, formant un chaos permanent de cris d’horreurs. Doté de la puissance du divin, mais la conscience détruite, il n’était plus qu’une fractale de frayeurs, de douleur et de violence.

Déchu de toute son infinité, il s’effondrait sur lui-même, emportant tout le premier monde avec lui. Omniprésent, il amena la corruption partout dans l’univers. Chaque nouvel être qu’il absorbait était ajouté à la fractale de pensée. La matière même fut corrompue, pulvérisé. Le monde se contracta jusqu’à provoquer un nouveau commencement. Tout fut détruit, pour recommencer.

Mais le dieu corrompu ne fut pas délivré par la mort, par l’inexistence.

Le cœur des mondes fonctionnait toujours.

Alors il continuait à exister, dans le néant entre les mondes. Condamné à la souffrance éternelle, il n’était régit que par un instinct qui lui disait de sortir, de fuir le néant. Tout le reste n’étant que fragments de pensées, de souffrances et de terreurs. À chaque fois qu’il réussissait à ressortir des abysses de la non-existence, il détruisait un nouveau monde, et un nouveau naissait. Il était toujours l’oméga, la dernière lettre, le point final de toute histoire. Si le multivers est un phénix, le dieu corrompu en était la combustion.

Et c’est depuis cela qu’à jamais ce cycle de destruction se répète. Parfois, cependant, un rayon d’espoir apparaît, et des héros réussissent à empêché la fuite du dieu de la corruption, à la sceller pour quelques nouveaux millénaires voir millions d’années. Mais ce n’est jamais que parti remise, le cycle éternel ne pourra toujours au mieux qu’être ralentit.

Le premier peuple avait souhaité faire partie de la structure même des choses, de l’être même. Son vœu avait été exaucé : le dieu corrompu était celui qui terminait les mondes.
