---
import: ../../../../crowbook/common.book
title: "La solitude du roi immortel"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/07/13 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/la-solitude-du-roi-immortel.pdf
output.epub: ../../../../../dist/download/la-solitude-du-roi-immortel.epub
download: la-solitude-du-roi-immortel
categories:
  - Contes
---
_On dit qu'entre notre monde et celui de mages résident un nombre incroyable de mondes-bulles, de réalités fragmentaires caché dans les interstices du monde de la logique et de celui des paradoxes. Des espaces, coincés entre nos règles rationnelles et celles étranges des arcanes magiques, qui se retrouvent à vivre selon leur propres lois._

_On dit que dans un de ces mondes, la mort n'existe pas, mais est remplacé par un mal bien pire. Un monde où les habitants doivent sans cesses lutter contre des hordes de monstres, nécessitant à chaque fois qu'ils chutent face à leurs ennemis, coincé dans une guerre éternelle où nulle mort ne peut les délivrer. Un monde où un roi éternelle regarde, coincé dans une solitude dont nul ne pourrait le sortir._

_Aujourd'hui, je vais vous conter l'histoire de ce monde._

&nbsp;

Un jour, il y a bien longtemps, un frère et une sœur découvrir un des univers étrange qui se trouvent coincé entre les deux mondes. Chaque idée pouvait y être matérialisée, chaque rêve pouvait y être concrétisé. Ce monde était modifiable, jusque dans ses règles même, a volonté. De cette terre sans vie, ils pourraient faire un paradis. Un monde qui ne connaîtrait pas la mort, la guerre et les tourments. Cependant, pour y entrer, un prix terrible était à payer. Il fallait abandonner son corps, le voir être dispersé dans la structure même de cette nouvelle réalité, le voir faire qu’un avec ce nouveau monde. Seul le cœur de la personne résiderait alors dans ce monde, quelque part.

La sœur fut la première à y entrer, sacrifiant son ancienne vie pour son nouvel idéal. Elle y serait la Créatrice. Elle y fit naître l’océan, le ciel et le soleil. Et au centre de tout cela, une Île.. Elle fabriqua de ses mains les plages, les forêts et les montagnes de cette île. Et partout sur cette île, de grands cristaux, qui en permettait le fonctionnement, et aux gens d’y vivre. Après des mois, son frère revint. Il était aller chercher un peuple. Des gens malheureux, qui ne voulait qu’une chose : changer de vie. Parmi eux, cinq orphelin, qu’il adopta comme ses propres fils. Il organisa les villes, offrit de nouvelles identité à ceux qui avaient fuit l’ancien monde. On lui proposa au début d’être le roi, mais il refusa, nommant un de ses vieux amis à la place. Il se contenta d’être le Conseiller du roi, vivant pour servir au mieux son vieil ami, et pour éduquer les cinq orphelin.

Au début, l’Île fonctionna comme cela. Le Roi, son Conseiller et la Créatrice, travaillant main dans la main pour construire le plus beau des mondes.

&nbsp;

Dans cette terre, les corps n’était rien qu’une création des cristaux, une illusion, une fusion d’apparence et de souvenir. Pour chacun des êtres qui y vivait, manger, boire dormir et respirer n’étaient plus des obligations, mais des plaisirs. La mort n’y était pas supprimé, mais remplacé. Les blessures graves pouvaient être soignée par un repos dans les grands cristaux qui parsemaient le monde, et lorsqu’un corps était trop endommagé, la personne pouvait abandonner son corps et ses souvenirs et se réincarner dans un nouveau corps, démarrant une nouvelle vie.

Personne ne savaient exactement ou se trouvait le cœur de chaque personne, mais il était quelque part, dans ce monde. Peut-être était-il partout ?

&nbsp;

Malheureusement, un jour, la tempête vint sur l’Île. Un royaume puissant du monde originel découvrit ce monde caché, après avoir enquêté sur les disparitions mystérieuses de ceux qui avaient rejoint ce dernier. Au début, ils ne voulurent perturber ce paradis. Mais la peur commença à les gagner quand une terrible épidémie commença à décimer la population. Ils voulurent alors utiliser ce monde, non pour sauver tout le monde, mais les plus grands. Les esprits brillant, les grands artiste. Il voulait éviter la perte irrémédiable de ceux qu’ils voyaient comme pouvant apporter le plus à l’humanité.

Ce projet était de recréer le jardin d’Eden, le projet Elysium. Le but était de créer un paradis artificiel pour les grands esprits. Le cœur deviendrait alors leur nouveau lieu de vie, une terre de culture où les plus grands esprits se retrouveraient, dans une vie vouée à philosopher et faire avancer la science et l’espèce humaine. Ils seraient de plus en plus nombreux, et leurs publications éclaireraient de plus en plus la planète, jusqu'à permettre un age d'or où l’humanité serait éternellement éclairé par des savants immortels. Mais pour cela, ils devaient prendre le contrôle du Cœur, afin de reprogrammer le monde pour qu’il agisse comme ils le voulaient.

&nbsp;

Le vieux roi regarda avec peur les armées qui commençaient à s’attaquer dans le royaume, mais tentant de toujours plus rassurer la population. Le Conseiller aidait les ravitaillement, à déplacer les blessés. La Créatrice utilisait de toute sa puissance pour faire surgir de nul part murs, armes, défenses. Mais les combattant de l’armée d’Elysium commencèrent à apprendre à utiliser les pouvoirs du monde. Une jeune femme rejoint les trois hautes personnes du royaume. Une ancienne générale, venue ici pour fuir les horreurs qu’elle avait vu pendant la guerre, se proposait pour combattre. Elle devint la Gardienne.

La guerre fut terrible et sans relâche, les soldats de la Terre et du Cœur revenaient tous sans cesse une fois qu’ils étaient tombé, ramené à la vie par les cristaux. Comment faire pour ne vaincre que les soldats de la terre ? Des armes expérimentales furent créé, dans le but de briser les corps cristallin, afin de forcer une réincarnation sans le moindre souvenir, ce qui permettrait au soldat terrestre de devenir de simples habitant du Cœur. Cependant, cela ne se passa pas comme prévu : sur les sujets de test, la destruction du corps cristallin ne fut pas complète mais se contenta de casser brisant toute cohérence entre le corps et le souvenirs, détruisant l'esprit de l'adversaire.

Ces armes furent quand même utilisées. L'armée terrestre fut exterminée, réduit à l'état de ces créatures incohérentes, les derniers souffles, qui furent bannis hors des mondes habités, dans des parties de l’île qui furent bloqués dans des petites bulles de réalités fermés parce que rendu trop brisés par la guerre, la structure même de la logique y était brisée. Les soldats se disaient qu'avec assez de temps, le corps des derniers souffles serait trop usé et ils se réincarneraient, sans aucun souvenirs de ce qui leur est arrivé. Cela n'arriva jamais.

À la fin de la guerre, traumatisée par ce qui s'était passé, une partie de la population décida de se réincarner. Il était plus simple de connaître dans les livres d'histoire une telle horreur que de l'avoir vécu soi-même. Parmi ceux qui se réincarnèrent, il y eut le vieux roi. Ce fut la fin de la première ère.

&nbsp;

La Créatrice et le Conseiller décidèrent qu’il était temps de passer le flambeau à la nouvelle génération. Ils continueraient avec la Gardienne leur travail, tandis que leurs enfants pourraient organiser le monde comme ils le souhaiteraient. L’aîné devint le nouveau Roi, la suivante devint Générale des forces armées, les deux jumelles devinrent une Mage et une Prépresse. Seul le petit dernier refusa d’avoir un rôle important, déclarant qu’être l’Arlequin lui allait très bien. Malheureusement, le jeune Roi n’arrivait pas à dormir la nuit, effrayé de voir un jour le retour des esprits. Nuit après nuit, il faisait le même cauchemar. Le retour de l’armée d’Elysium. Si les premiers mois de son règne furent heureux, ils sombra rapidement dans la peur.

Pour se rassura, il ordonna de continuer la production des terribles armes qui leur avait assuré la victoire. Il avait au début voulu l’arrêter, mais avait changé d’avis, ses actes dictés par la peur. Le lendemain de sa décision, ces deux sœurs cadettes, la Mage et la Prêtresse virent le voir.

&nbsp;

— « Ô mon roi, mon bon frère, réfléchit un peu à tes actes, » commença la mage. « Ces armes sont trop terrible pour être utilisés, notre monde même pourrait être en danger. »

&nbsp;

La prêtresse, comme toujours, restait silencieuse.

&nbsp;

Mais ils n’écouta pas ses conseils. Il était effrayé par une nouvelle possibilité. Et si les soldats étaient trop nombreux pour utiliser l’arme sur eux ? Et si une armée trop grande venait, des hordes permanentes qui continueraient à venir jusqu’à ce qu’ils les aient écrasés ? Et si tous les mondes de l’extérieur se mettaient à croire au projet Elysium ? Il devait avoir des troupes nombreuses, sans sacrifier des habitants qui n’avaient rien demandé. Alors il inventa à l’aide des cristaux un nouveau moyen de défense. Des monstres générés et contrôlés par les cristaux, servant à défendre le territoire du Cœur. Ces monstres seraient des sortes d'intelligences artificielles basiques dédiées au combat. Ils pourraient être déployées un peu partout en cas de besoin.

Les deux sœurs revinrent l’avertir, la Mage disant qu’il jouait avec le feu, et qu’il mettait le monde entier en danger.

&nbsp;

Mais ils n’écouta pas ses conseils. Il était effrayé par une nouvelle possibilité. Et si il existait des soldats terrestre rescapée, sans avoir la moindre possibilité de connaître leur position exacte ? Une force mystérieuse et invisible, qui n’attendait que leur heure. Et si des gens de ce monde adhéraient aux idée du projet Elysium ? En effet, pour certain, ne pouvait-ce être attirant de créer un monde de savants et philosophes éternels ? Il prit alors une terrible décision : Il allait changer la mémoire de tous les habitants, et faire effacer tous souvenirs de ce qu'était véritablement l’Île. Ce ne serait alors qu'un monde, ayant toujours été comme ça. L’ancien monde serait oublié à jamais. Au cas où, les monstres seraient déployés dans les zones hors des villes.

Un programme serait installé en eux pour qu'ils attaquent toutes personne connaissant la terre : En effet, ils ne pourraient alors être que des Terriens, non ?

&nbsp;

Quelques jours après son annonce, ses trois sœurs et son frère virent le voir, avec un ultimatum : Il devait abandonner son projet, sinon ils le forceraient à l'abandonner, et iraient même jusqu'à le détruire lui pour protéger l’Île. Faire tout oublier à tous le monde, les monstres et les armes, ce ne seraient que faire sombrer l’Île dans la catastrophe, celle d’oublier les erreurs et les horreurs du passé. Il répondit tristement qu'il n'avait pas le choix. Dans ses yeux, il y avait la peur. Il n'avait jamais oublié la guerre. Il appela ensuite la garde.

&nbsp;

Alors qu’ils étaient accompagnés dehors, la jeune Prêtresse se retourna, et le regarda dans les yeux :

&nbsp;

— Mon frère, je sais que tu as peur. Mais écouter sa peur, c’est arrêter d’écouter sa raison, c’est arrêter d’écouter son cœur. La raison et le cœur sont ce qui font de nos de vrais humains. C’est leur combat perpétuel qui nous fait avancé. La peur obscurcit les deux, et tu as perdu toutes tes forces. Abandonne ce projet qui n’est né que des ombres qui te hantent, et te murmurent à l’oreille des conseils bien malavisés. Mon frère, je t’aime, mais si tu veux déclarer la guerre à la sanité et à la vie, alors la sanité et la vie te déclareront la guerre. Je suis désolée.

&nbsp;

Une nouvelle guerre éclata, à l’intérieur de la famille royal. La Créatrice, le Conseiller et la Gardienne, aussi effrayé par les souvenirs de l’ancienne guerre, furent dans le camp du Roi. Ils voulaient à tout prit assurer la protection de leur monde contre ceux qui étaient venu de nul part et de partout pour l’attaquer. Le reste de la fratrie royale devint les chefs de la Rébellion. Au début, la guerre restait une guerre froide. Nul combat n’éclata.

Mais un jour, la Gardienne, décida d’aller discuter avec la Mage pour tenter de la raisonner. Un combat éclata. On vit la Mage revenir avec le corps inerte de sa mère adoptive. Elle ne semblait ni pouvoir se réincarner, ni retourner aux cristaux. Elle avait trouvé comment bloquer le cœur d’une personne. La guerre fut déclaré. Le père des enfants, l’ancien Conseiller du Roi, sombra dans le chagrin et le regret, et parti pour ne jamais revenir. La Créatrice aussi se retira. L’ancienne génération s’était retiré, et la nouvelle se faisait la guerre.

&nbsp;

Le jeune Roi se retrouvait alors seul, contre sa fratrie qui voulait le détrôner. Toute l’armée ne semblait suffire à les arrêter. Il eut un jour une explication : la maître des secrets avait découvert qu'il était possible d’utiliser les pouvoirs de son propre cœur. Cela permettait d'utiliser des pouvoirs incroyables, mais cela faisait risquer sa vie. Le roi ne chercha pas à faire de même, et entra dans un cycle perpétuel de blessure et de soins par les cristaux, et envoyant les monstres attaquer ses frères.

Après des mois de bataille, la rébellion fut capturée. Le royaume était grandement endommagé. Certains partisans de la terre grondaient. Le roi avait énormément perdu en popularité, considéré comme un faible. Il n'avait que 20 ans, et n'aurait sans doute même plus longtemps à vivre, tellement son corps était abîmé. Il lui fallait prendre une grande décision. S'il détruisait ses sœurs et son frère, il serait considéré comme un roi fort et puissant.

Mais il ne pouvait pas s'y résoudre.

&nbsp;

Il décida de baisser la puissance d’une des armes, et de se contenter de bloquer avec le fruit de ses recherches la forme physique de ses frères et sœurs, les rendant bien moins dangereux. Après, il les fit enfermer dans des tours conçu pour les garder prisonnier à jamais.

La révolution grondait alors, le peuple voulait se débarrasser du roi trop faible. Les quelques partisans de la Terre en avait profité pour semer l’idée qu’il serait mieux d’être un paradis pour les grands esprits que d’être un royaume en ruine. Le roi se dirigea alors vers les cristaux. Il mit alors directement son premier plan à exécution : la Grande Réincarnation. Il commença par désincarner tous les corps physique, sauf le siens, et de ses derniers fidèles. Ils détruisirent ce qu'ils purent trouver comme trace de la terre, ignorant l’existence de certains mondes cachés construit par la créatrice.

Ils lancèrent après cela la réincarnation, et le roi dit adieu à ses derniers fidèles.

&nbsp;

Il n'allait pas se réincarner, mais il fit entrer sa forme physique dans les cristal. Il n'osait ni essayer de mourir définitivement, ni se réincarner, ni rester.

Il voulait juste un peu de sommeil.

&nbsp;

Un seul élément ne se passa pas comme prévu. Les monstres se mirent à mal fonctionner : Ils en se contentaient pas d’attaquer les terriens, mais tout ce qui bougeait, à cause de leur intelligence artificielle qui fonctionnait mal. Le monde était désormais l’Île, terre ou les humains devaient vivre malgré les monstres, plongé dans un combat éternel entre deux armées immortelles. L'ancienne armée royale fut désormais destinée à protéger les habitants des monstres. Certains essayaient même d'apprendre à contrôler les monstres.

La nouvelle histoire de le Cœur se fonda sur cette nouvelle situation, celle des héros et des monstres. Le vagues souvenirs de l'avant-réincarnation resta sous forme de légendes vagues, de mythes complexes à comprendre. Et, dans un demi sommeil, le roi éternel ne pouvait que contempler à jamais les dégâts de ce qu’il avait fait.
