---
import: ../../../../crowbook/common.book
title: "La chute"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/28 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/la-chute.pdf
output.epub: ../../../../../dist/download/la-chute.epub
download: la-chute
categories:
  - Contes
---
Il avait été puissant. Il avait été grand. Mais désormais, il n’était plus rien.

&nbsp;

Il se souvenait d’avant. De ce temps où encore on L’adorait, ce temps où encore on Le vénérait, ce temps où encore on Le craignait. Cette époque où Sa puissance était telle que Sa simple colère pouvait faire tomber des royaumes, des empires, faire s’effriter les montagnes et carboniser les forets.

Il avait été un titan, il avait été le plus roi incontesté d’un panthéon. Les autres divinité tremblaient face à Sa puissance. Il se souvenait de ce temps bénit de Lui-même, où d’un simple mot Il avait ses désirs satisfaits. Il avait protégé son peuple élu avec une dextérité qui L’impressionnait Lui-même, qui L’avait conforté dans son rôle de chef de tout les titans.

Qu’il avait fait croire qu’en fin de compte, il était un Dieu, au dessus des titans : Il commençait à s’identifier au Grand Créateur, l’unique divinité des divinité, celui que devait vénéré les titans.

&nbsp;

Il avait acquis le pouvoir après une dure lutte. Son prédécesseur était devenu un tyran, obnubilé par la crainte qu’il serait remplacé par un nouveau roi des titans. Son prédécesseur écrasait d’une violence également seulement à sa terreur toute les nouvelles divinité.

Ce despote divin les détruisait physiquement, leur brisait le corps et l'âme pour leur empêcher d’atteindre une force nécessaire pour l’arrêter. Presque immortels, ils ne décédaient jamais à leur blessures mais devaient conduire une vie éternelle d’obéissance aux titans puissants, une vie ou jamais ils ne pourraient prendre la main.

Certains fuyaient loin du plus grand royaume des titans pour échapper à ce sort funeste, devenant des petites divinités n'ayant qu'un petit nombre de croyant.

&nbsp;

Alors, Lui, encore jeune divinité qui cherchait à faire Ses preuve était intervenu, et avait montré Sa puissance. Il avait avec patience réuni d’autres titans exilés, cachés dans les royaumes lointains. Il avait avec patience conduit un peuple dans les royaumes humains à la gloire pendant un temps. Fier de Sa grande armée, il était parti à l’assaut du tyran, dans la cité céleste.

Armée d’une grande faux, Il avait terminé dans la souffrance le règne de Son prédécesseur, et avait accompli le cycle :

Il avait été couronné nouveau roi des titans et avait condamné à l'oubli l'ancienne cour.

&nbsp;

Ensuite était venu un long règne, pendant lequel Il avait prit goût à sa puissance. Il avait contrôler Ses nations, il avait défendu son honneur pendant de longues guerres.

Mais avec le temps, Il était devenu de plus en plus certain de sa toute puissance. Avec le temps, Il n’avait même plus prit le soin de s’assurer du soutiens de ses hommes.

Il était désormais le nouveau Grand Créateur, n’avait-Il donc rien à craindre de simple titan ?

Il était une divinité supérieure.

&nbsp;

Après des siècles de règne, un nouveau jeune titan était venu devant lui, et voulait son trône. Une petite divinité mineure, protecteur d'un groupe de mortel sous le contrôle du royaume divin, comme il en existait tant. Le genre de titans rebelles que personne ne prenait véritablement au sérieux : être protecteur d'un petit clan n'apporte aucun pouvoir face à la puissance du roi des titans.

Chaque jour, il revenait le défier, mais n'avait pas la puissance et devait repartir. Les mois passèrent, rythmée par des défis quotidien. Cependant, un jour, un piège Lui fut tendu. Son ambroisie quotidienne fut empoisonné.

Il ne sut jamais comment le jeune titane rusé avait réussi à tendre le piège. Il avait été plus intelligent que lui. Celui qui s’était prit pour plus qu’un titan, pour un Dieu, pour l’incarnation du Grand Créateur subit le sort d’une grande partie des rois titans qui se rendaient coupable d’hybris.

Alors qu'il était affaibli, il l'attaqua, vainquant ses garde, et le supplanta, armée d'une épée.

D'un coup, il lui signifia qu'il était désormais détrôné.

&nbsp;

Humilié, vaincu, il était désormais obligé de vivre dans l’ombre. Mais la revanche d’un ancien roi des titans venait toujours.

Elle se produisait lorsque le règne de son successeur s’achevait dans la souffrance.

&nbsp;

Il était patient.

&nbsp;

Il avait tout son temps.
