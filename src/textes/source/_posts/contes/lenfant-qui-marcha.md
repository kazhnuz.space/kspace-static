---
import: ../../../../crowbook/common.book
title: "L'enfant qui marcha"
date: '2016/07/21 10:13:00'
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/lenfant-qui-marcha.pdf
output.epub: ../../../../../dist/download/lenfant-qui-marcha.epub
download: lenfant-qui-marcha
categories:
  - Contes
---

Dans un pays, à une époque qui pouvait aussi bien être il y a très longtemps que demain, vivait une petite fille. C’était une enfant qui vivait comme les autres. Elle avait ses joies et ses peurs, elle était curieuse et aimait découvrir de nouvelles histoires. Parfois il y avait des héros, parfois des gens ordinaires. Parfois elles faisaient peur, parfois elles faisaient rire.

Tous les soirs, ses parents lui racontaient une nouvelle histoire. Tous les jours, elle en découvrait une nouvelle par elle-même. Et elle adorait en inventer.

&nbsp;

La vie semblait belle.

&nbsp;

Mais un jour, la petite fille tomba malade. Et personne ne put y faire quoi que ce soit. Elle s’affaiblissait de jour en jour, pouvait de moins en moins manger. Et elle n’eut le droit à nul miracle. Ses parents la savaient condamné, mais pour atténuer sa souffrance, tous les jours ils allaient la voir. Lui faire penser à autre chose. Pendant un instant, s’évader. Sortir de sa vie. Masquer leur propre douleur pour apporter du réconfort. Elle aussi, pendant ces moments, faisait en sorte de sourire le plus possible. Masquer ses propres peur pour apporter du réconfort.

Et un soir, pendant son sommeil, elle s’éteignit. C’était la fin d’une histoire. Ses parents restèrent longtemps malheureux, mais se relevèrent et continuèrent à avancer dans leur vie. Avec un manque qui resterait.

Mais ils ne s’arrêtèrent pas de marcher.

&nbsp;

Quant à la petite fille, sa première histoire était finie, mais une autre allait commencer. Elle se réveilla. Dans un monde entièrement vide. Enfin non. Une grande plaine désertique s’étendait jusqu’à l’horizon. Un ciel, bleu, sans nuage, mais il n’y avait pas le moindre soleil. Elle était toute seule. Il faisait froid. Elle avait peur.

Elle commença à courir, pour savoir où elle était. Où étaient ses parents. Elle ne se demandait pas pourquoi elle avait d’un seul coup la force de marcher. Elle ne se demandait pas pourquoi elle n’était plus faible. Elle était trop perdue pour se poser de telles questions, elle voulait retrouver ses parents. Elle courut jusqu’à être épuisé. Seule ses traces de pas lui prouvait qu’elle n’avait pas fait du surplace. L’horizon était toujours aussi désespérément vide. Rien ne pouvait lui servir de repère.

Elle était perdue dans un monde infiniment plat et vide.

&nbsp;

Jamais la petite fille ne sut combien de temps elle avait couru. Des heures, des jours, des semaines ? Le ciel était constamment clair, sans astre pour repérer le temps. Tout ce qu’elle savait, c’est qu’elle n’avait plus d’énergie. Elle avait faim. Elle avait soif. Elle se laissa retomber sur le sol, et souhaita juste pouvoir manger et boire.

Elle s’endormit.

&nbsp;

À son réveil, un arbre se trouvait juste à côté d’elle. Des fruits dodus et juteux y avaient poussé. Était-ce arrivé pendant qu’elle avait dormi ? Oubliant toute prudence, elle prit un des fruits et mordit dedans. Il était bon. Elle en mangea jusqu’à ne plus rien pouvoir avaler, se laissant tomber. Elle regarda le ciel. Il était toujours aussi vide. Tout autant que son esprit. Elle n’arrivait même plus à se poser de questions. Elle n’était pas sûre de la réalité de tout ce qui se passait.

Elle ne pouvait plus faire qu’une chose face à tout cela. Se lever. Et marcher. Marcher jusqu’à ce qu’elle trouve des réponses ou de la vie. Marcher jusqu’à ce que ce monde ne soit plus la plaine déserte et vaste qu’elle voyait jusqu’à l’horizon. Marcher jusqu’à ce qu’elle ne puisse plus le faire. Elle ne savait ni ou elle était, ni pourquoi elle y était.

&nbsp;

Mais déjà quelques idées venaient dans sa tête. Elle se souvenait de ses derniers instants, avant de s’endormir. Des pleurs. Des bruits de l’hôpital.

Et si elle ne s’était pas simplement endormi ? Et si l’expression « sommeil éternelle » était exact, et qu’elle vivrait désormais dans un rêve définitif ? Elle était effrayée comme elle ne l’avait jamais été. Elle comprenait qu’elle ne reverrait jamais ses parents. Elle était seule. Et comme ce monde était vide, ne risquait-elle pas de l’être à tout jamais ?

&nbsp;

Les larmes aux yeux, elle regarda ses mains, puis l’arbre devant elle. Un fol espoir s’alluma.

&nbsp;

Et si l’arbre n’était pas apparu par miracle ? Et si elle était celle qui l’avait fait apparaître ? Peut-être alors qu’elle aurait la capacité de ne pas rester toute seule. Alors elle se concentra.

Elle regardait devant elle. Elle savait ce qu’elle voulait créer : Un portail vers là ou se trouvait ses parents. Vers son monde originel. Elle se concentra. Elle l’imaginait comme d’immense portes, entièrement en or et en joyaux. Une sorte de membrane liquide. Quand elle traverserait cette membrane, elle se retrouverait dans son monde d’origine, et pourrait parler à ses parents. Lorsqu’elle ouvrit les yeux, la porte était devant elle. Majestueuse et monumentale, elle surplombait toute la pleine. La petite fille s’élança, fondit dans le portail pour rejoindre ses parents.

Mais rien ne se passa.

&nbsp;

Il était impossible de sortir du monde dans lequel elle se trouvait, elle était désormais enfermée dans sa monade, son monde intérieur.

&nbsp;

Effondrée par le chagrin, elle songea un instant à créer des copies conformes de ses parents, pour pouvoir plonger dans leur bras et pleurer… Mais malgré la douleur que ça lui causait, elle s’y refusa : Ses parents étaient irremplaçables, elle ne pouvait en créer des copies pour tenter de les retrouver. Parce qu’elle ne les retrouverait pas.

Et elle avait presque l’impression que faire cela serait les trahir, trahir ses vrai parents qui, dans son monde d’origine, étaient en train de la pleurer. Après tout ce qu’ils avaient fait pour elle, elle n’avait pas le droit de les trahir, pensait-elle.

&nbsp;

Elle ne sut jamais combien de temps elle resta seule, à vaguement manger les fruits des arbres quand elle avait trop fin, et à tester mollement ses nouveaux pouvoirs. Parfois elle créait des statues à l’effigie de ses parents. Mais généralement les détruisait à cause de la colère et du chagrin.

Elle restait seule parce qu’elle ne voulait voir personne d’autre que ses vrais parents, et se refusa pendant longtemps à créer le moindre autre être vivant.

&nbsp;

Ce n’est qu’au bout d’un moment que la solitude la pesa trop, qu’elle se mis à vouloir vraiment voir des gens. N’importe qui. D’autres êtres humains.. Elle ferma les yeux et laissa son esprit s’emplir de toute sa volonté. C’était comme si elle savait au fond d’elle qu’elle devait utiliser tout ce qu’elle pouvait pour voir ce qu’elle voyait. Elle se souvenait de ce rêve étrange qu’elle avait fait, il y a longtemps. Celui ou elle savait qu’elle rêvait, et qu’elle avait pu prendre contrôle des événements.

Elle voulait être dans une ville, comme cette ville médiévale qu’elle avait visitée avec ses parents, il y a quelques années. Une ville pleine d’activité et de festivité. Elle voulait revoir la vie.

&nbsp;

Elle ouvrit des yeux émerveillés en voyant la ville peuplée et pleine de vie qui s’étendait devant ses yeux. Les gens s’esclaffaient et s’envoyait des grandes claques dans le dos. Elle était dans une de ces villes idéalisés des livres de sa petite enfance. Quelque chose de vaguement médiéval et utopique à la fois. Ou les rois étaient bons, et ou le mal n’était pas le fruit de systèmes complexes et de personnes malveillantes, mais de grands méchants flamboyants vêtu d’un grand manteau sombre. Le souvenir rassurant d’anciennes histoires de son enfance, souvenir qui lui réchauffait le cœur et consolait un peu son chagrin.

Elle courrait dans la ville, elle mangea des parts d’énormes gâteaux qui étaient présentés pour la fête, elle regarda les spectacles des saltimbanques. Elle s’amusait comme une petite folle, elle n’avait pas vu le moindre autre être humain depuis des temps si longtemps.. C’était plein d’une vie qu’elle pensait ne jamais revoir. Les gens étaient heureux et s’amusaient. Elle était chez les gentils.

C’était un monde simpliste, mais qui la rassurait. Elle se mit alors à parcourir la grande plaine. Elle la sectionna en un nombre incroyable d’îles flottante. Elle n’avait pas à craindre les questions scientifiques de gravité, tous ce genre de chose : Ces règles n’étaient pas celles de son univers. Elle fit naître une nature chatoyante, des animaux fantastiques. Certains venaient de ce qu’elle avait lu, et d’autres étaient reconstruits à partir de bout d’animaux de son monde – à partir de ses souvenirs et de ses connaissances naissait un nouveau monde.

Dans le ciel, des centaines de petites étoiles éclairait les îles flottantes. D’immense boules de feu en suspensions, qui cessait étrangement de briller fort pour la nuit, ne lassant qu’une luisance rougeoyante permanente, telle une sorte de veilleuse pour l’enfant qui avait peur du noir. Elles offraient la vie, telles les fruits de l’arbre qu’avait fait pousser en premier la jeune enfant. Elle créa des peuples gouvernés par des reines et rois sages et justes, aux chevalières et chevaliers dotés de courages et de compassions, prêt à aider tous. Ils étaient bien sûrs dotés de défauts, mais c’était comme si la sensation que de véritables personnes allaient vivre dans ce monde lui donnait envie de créer une utopie.

&nbsp;

Elle ne savait pas qu’elle ne jouait que d’une certaine manière son rôle dans le cycle de perpétuation des univers. Elle ne savait pas que toute sa vie avait préparé par inadvertance ce moment, que chacun des souvenirs, de ces expériences en avait fait une personne unique, qui serait à l’origine d’un monde unique. Ça n’avait été écrit nulle part. Ce n’était aucun destin qui avait décidé ça. Ce n’était pas dans les règles du multivers que les êtres dotés d’intelligences étaient à l’origine des mondes suivant. Ce n’était qu’un épiphénomène qui faisait office de cosmogonie. Une sorte de coup de bol transcendant. Qu’un effet secondaire de la capacité d’imaginé, d’avoir des mondes à l’intérieur de sa tête.

Tout le monde est démiurge ; ce n’est qu’un hasard. Un jour, un être capable de penser et de se tromper, de raisonner et de déraisonner, d’aimer et de détester était mort. Ce jour-là, l’œuf qu’il était avait éclos dans un nouveau monde, qui prenait sa place dans l’infinité du multivers. Son monde intérieur était devenu monde extérieur.

Rapidement, elle remarqua que son monde n’était pas infini : Elle retourna au point de départ.

&nbsp;

L’arbre.

&nbsp;

Au bout de quelques années, de quelques siècles à observer tous son univers, elle commença à se lasser. Elle avait l’impression que tout se répéter, et une langueur envahissait son âme. Que pouvait-elle trouver pour s’amuser dans ce monde parfait ? Elle s’ennuyait de plus en plus, et avec l’ennui, le chagrin et la colère revint. Elle avait tout fait pour oublier pendant des années qu’elle avait perdues ses parents, et à quel point ils lui manquaient. Mais maintenant, elle ne pouvait plus fuir en avant. Et ce monde stupide lui refusait de les revoir !

Elle se mit en colère. Ce monde était sa création, elle y avait des pouvoirs fantastiques, et un simple petit portail lui était refusé ? Elle voulait revoir ses parents, c’était si compliqué pour ce fichu monde ?

&nbsp;

Le mal à toujours des origines diverses suivants les mondes. Parfois il s’agit de quelque chose de complexe, et de pas vraiment explicable, qui peut venir de l’ambition, ou de la sensation de puissance qu’éprouvent certaines personnes à écraser les autres. Parfois, il s’agit d’une force pernicieuse de la nature. Parfois c’est inscrit profondément dans la nature humaine. Parfois c’est l’exception, parfois c’est la règle.

Ici, il s’agissait de l’ennui et de la douleur d’une enfant qui n’avait jamais demandé d’avoir le rôle de divinité, qui n'avait jamais mérité de subir une telle responsabilité. D’une enfant qui tentait de compenser le fait que ses parents lui manquait par le fait de créer, toujours plus créer. Il s’agissait du chagrin d’une petite fille qui ne pourrait jamais revoir ses parents. De sa colère face à sa propre impuissance à changer ce qui la rendait malheureuse.

Comme si elle s’était amusé à détruire un château de sable qu’elle avait construit, elle introduisit le mal et les ténèbres dans son monde.

Des milliers d’esprits maléfiques, issues de ses siècles d’idées noirs et d’ennui.

&nbsp;

La petite fille eut un moment de culpabilité. Mais face à l’excitation qu’il se passe enfin quelque chose, et encore sous le coup de sa colère, de son sentiment d’injustice, elle la mit de côté : ce fichu monde lui apporterait au moins un peu d’intérêt ! Alors naquit l’Ordre des Exorcistes, qu’elle participa elle-même à fonder en secret, qui devait se battre contre ces spectres. La guerre fut longue et rude, mais les spectres furent vaincus. De nombreuses familles d’exorcistes devinrent puissante dans les royaumes. Ils renversèrent parfois des rois. Était-ce pour mieux protéger le monde ou simplement par ambition ? Les exactions de nombreuses familles semblaient montrer qu’il s’agissait de la seconde chose.

Alors des rébellions se formèrent. Alors, lassés des abus des exorcistes, les peuples se révoltèrent contre leur règne. La guerre à nouveau gronda. Cette fois, elle fut pour la liberté. La jeune fille qui courrait à travers les batailles, invisible et intangible aux combattants, s’amusait comme jamais elle ne s’était amusé. Des héros, des méchants. Parfois des combats tragiques de héros contre d’autres héros.

Les batailles pouvaient durer des jours. La pluie s’abattait sur les corps vide de vie après que les forces s’étaient affronté. Certaines familles profitaient de la situation pour gagner encore plus de pouvoir en jouant un peu dans les deux camps, faisant courbettes face aux exorcistes avant d’exalter d’autres royaumes à prendre les armes contre eux. Les conspirations et les coups d’états étaient pour la jeune fille passionnant à suivre. Est-ce que ce double jeu allait se retourner contre tel noble ? Est-ce qu’il allait subir les conséquences de ses actes ? La guerre se termina après un dernier duel, et un arrangement donna aux exorcistes des titres important, mais leur retira en grande partie le pouvoir politique.

La petite déesse malgré elle fut contente : Une fin heureuse pour une histoire épique de guerre, de trahisons et de complots.

&nbsp;

Mais le lendemain, en marchant à travers les villes, elle comprit que les flammes qu’elle voyait et qui brûlaient encore des batailles de la veille étaient bien réelles. Que l’action qui l’amusait tant était la mort de centaines de milliers de personnes. Que les souffrances n’étaient pas celles qu’elle aurait vu dans un conte. Elle vit les ruines. Elle vit les pleurs, elle vit les villages qui brûlaient.

Avant, elle n’avait regardé que les batailles de haut. Parfois, elle avait plongé au cœur de l’action, s’enivrant d’adrénaline. Mais maintenant qu’elle était dans le calme, dans les villes, elle ne voyait que les conséquences de cette guerre. Les conspirations lui semblait moins passionnante maintenant qu’elle voyait ce que cela avait produit sur le monde qu’elle avait créé… Et qu’elle avait participé à détruire. Et que la fin heureuse qu’elle avait imaginée n’était que pour bien des gens qu’heureuse dans ce qu’elle n’apporterait pas encore plus de malheur.

Alors la jeune fille s’arrêta de marcher. Peut-être avait-elle trop agi sur ce monde ? Elle ne pouvait pas détacher les yeux de ce malheur. Tout ce qu’elle avait pu imaginer dans ses histoires, elle se rendait compte qu’elle ne pouvait pas le souhaiter pour le monde réel. Que les grandes guerres épiques n’étaient pas si appréciables quand elle se passait dans un vrai monde. Que les fins heureuses n’étaient pas si heureuses quand on changeait de point de vue.

Un écrivain ne peut pas être maître du monde, de la réalité.

&nbsp;

Elle songea à effacer tous ses actes. Mais étrangement, alors qu’elle avait pu tout créer par avant, cette fois, son contrôle lui fut retiré. Était-ce une punition face à ce qu’elle avait fait, ou était-ce son propre sentiment de culpabilité qui lui interdisait d’effacer ses erreurs d’un geste de la main ? Comme si elle n’avait pas le droit de tenter de faire croire que rien de ceci n’était arrivé. Son monde idyllique n’était plus. Il avait été remplacé dans un monde ou la méfiance régnait et le conflit menaçait d’éclater à nouveau.

Elle prit alors la décision de se retirer du monde. Pour aller réfléchir. Pour méditer, et pour retourner à une vie normale. Elle créa un bâtiment, une simple maison. La copie exacte de celle qu’elle avait eut à l’époque ou elle était encore en vie. Elle protégea sa maison, pour éviter que des personnes mal intentionnée entrent chez elles.

Sur le pas de sa porte, elle regardait les différentes îles flottantes. Elle soupira. C’était son monde, mais elle se sentait coupable de tout ce qui était arrivé. Et coupable elle était. Elle ne se sentait pas le droit de réparer ses erreurs. Où peut-être avait-elle peur ? Peut-être était-ce impossible de bien réagir à une telle culpabilité, la culpabilité d’avoir semé conflits et morts, encore plus quand on était encore enfant ? Elle se retourna, entra dans la maison.

Et ainsi la créatrice du monde s’enferma, décidant de tourner le dos à tout ce qu’elle avait construit.

&nbsp;

Tel est le mythe de l’enfant qui marcha.

&nbsp;

Tel est le mythe de la création de notre monde, du Grand Archipel.

&nbsp;

Telle est la tragédie qui fit naître le monde dans lequel on vit : Celle d’être né d’une créatrice trop jeune pour endosser une telle responsabilité.
