---
import: ../../../../crowbook/common.book
title: "Sourire"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/02/07 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/sourire.pdf
output.epub: ../../../../../dist/download/sourire.epub
download: sourire
categories:
  - Nouvelles
---

Le ciel était orangé comme son habitude dans les cieux de Manco Capac, et l’étoile Cuzco était visible dans le ciel, un immense disque rouge. L’air était comme toujours frais sur la planète. Même en étant la plus proche des planètes de la zone habitable de l’étoile, Manco Capac était la plus fraîche.

Mais l’air frais était aussi ce qui faisait son charme. Ainsi que ses grands espaces à explorer, à découvrir, à habiter. Cette planète, à l’origine vierge de vie, était un nouveau terrain d’aventure, d’exploration.

&nbsp;

Et cette fois, se disait Tiso, ce ne serait pas aux dépens d’autres peuples. L'envie d'aventure ne se ferait pas sur une montagne de corps et d'âmes brisées par la haine ou la négligeance des puissants. Ils ne seraient pas des colons de terres habitées, ils ne seraient pas les destructeurs de peuples, civilisations, espèces.

Aucune des planètes de Cuzco n’était habité ni même totalement habitable, à l'origine, et le système solaire avait été choisi spécifiquement pour cela. Pour pouvoir créer un nouveau lieu à partir de rien. Pour donner une nouvelle chance.

Cependant, à une année lumière se trouvait une étoile autour de laquelle il y avait des traces de vie. De futurs voisins.

&nbsp;

Tiso vivait dans un village fermier dans les plaines qui entouraient la capitale Quri Kancha, sur le continent de Vilcabamba, plus grand des deux continents de la planète. Avec ses compagnons, il avait participer à construire le village, avec l'aide des nombreuses machines servant à aider à cette lourde tâche. Ils étaient la première génération arrivée sur la planète, il y a de cela six ans, quand la planète avait été déclaré "habitable".

Le jeune homme se souvenait encore du jour de l’atterissage, après quelques siècles d’orbite durant la terraformation.

Ça avait été une grande fête improvisée sur le train d’atterissage, et qui avait durée plusieurs jours. À la fin de la fête, un discours des différents chefs religieux, politiques et culturels présent dans le voyage avait annoncé ce qui était le nouvel espoir du projet Dandelion : Ils étaient sur une nouvelle terre, qu’ils allaient devoir construire ensemble.

Ils avaient le droit à un nouveau départ, à une nouvelle possibilité de faire les choses d’une nouvelle manière.

&nbsp;

Même si tout n’était pas parfait, ce projet de construire ensemble une planète, une civilisation, était ce qui faisait avancer les habitants du vaisseau Manco Capac.

Le vaisseau avait été lancé alors que dans la Confédération Solaire, la guerre faisait rage et les humains s'entredéchirait. Ici, des gens de toutes origines avaient décidé de fuir ce conflit, où comme toujours les plus fragiles étaient les premières victimes. Les ancêtres des habitants avaient connu et consigné les horeurs de la guerre, de l'oppression et de la haine.

Les nouveaux habitants n’étaient pas bien nombreux : à peine quelques millions, sans compter les enfants, qu’ils soient ou non en stases dans le vaisseau, en sommeil avant de pouvoir être reveillé et vivre leur nouvelle vie.

Était-ce assez ? Ils étaient sûrs que oui.

&nbsp;

La petite planète avait été relativement rapide à terraformer - quelques siècles à peines - et était l’une des premières communautés extra-terrestres qui avaient été formées grâce au projet Dandelion.

Des tas de réactions chimiques, physiques complexes, qui avaient pour but de rendre vivable la planète glacée et stérile qu'était Manco Capac. Ce n'était pas toujours facile : la planète avait une terre grandement stérile toujours, et cela demandé beaucoup de travail de créer des endroits fertiles pour créer les forêts, terres agricoles où les parcs naturels.

Depuis, l’espace s’étendait. Ils construisaient de nouveaux villages autour de la ville, et déjà des bateaux étaient lancés pour découvrir les nouveaux continents, et commencer à y vivre. Les animaux s’acclimataient généralement bien, dans les espaces créés pour eux. Petit à petit, ce nouveau monde se construisait. Il n’était pas comme l’ancien :

De nouvelles expérimentations de sociétés s’y faisaient, des cultures pouvaient y prospérer plus facilement que sur Terre, et les premiers éléments d’une culture commune continuaient à se former.

&nbsp;

Depuis Manco Capac, les scientifiques commençaient à réfléchir à comment ils allaient observer, voir entrer en communication avec les formes de vie sur les planètes du soleil voisin.

Ils ne voulaient pas répéter les horreurs du passé – surtout quand bien des habitants de la planète étaient descendants de victimes de la colonisation.

Ils ne voulaient pas non plus être les « méchants aliens envahisseurs » des films de science-fiction.

&nbsp;

Il y avait parfois des conflits.

Certains se demandaient comment les choses se passaient, sur la lointaine terre-mère.

Les récoltes n’étaient pas toujours faciles, et il fallait apprivoiser cette nouvelle terre : Quelle plante pouvaient pousser dans ces terres peu fertiles, comment allait être le climat ? Parfois, des pluies catastrophiques détruisaient tout. Ils découvraient à leur dépend que certaines zones étaient sismique.

Cependant, ils reconstruisaient. Ils avançaient et tentaient d’améliorer les choses.

&nbsp;

Parce qu’ils avaient un but. Parce qu’ils avaient un avenir.

&nbsp;

Et à travers l’espace, se disait Tiso, des centaines de petites graines d’humanités avaient été plantés.

Chacune permettant à des cultures différentes de revivre, chacune permettant à de nouveaux modèles de se former. La diversité de l’espèce humaine se diffusait à travers le cosmos. Des planètes inhabitées se retrouvaient habités. Des premières rencontres se faisaient quand une planète non-habitée était voisine d’une autre habitée – il fallait espérer désormais qu’elles se passent bien, et que les fautes du passé ne soient pas répétées.

On était dans le début d'une nouvelle ère. Comme tout changement de cette envergure, il était à la fois effrayant et excitant. Tout était possible. Les meilleurs futurs comme les pires dystopies.

Cependant, les habitants de Manco Capac avaient décidé de sourire. Parce qu'ils espéraient un futur meilleur.

&nbsp;

On avait soufflé sur le pissenlit, et ses graines s’en allaient se disperser à travers l’espace.
