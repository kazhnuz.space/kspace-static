---
import: ../../../../crowbook/common.book
title: "Sans âme"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2019/10/07 16:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/sans-ame.pdf
output.epub: ../../../../../dist/download/sans-ame.epub
download: sans-ame
categories:
  - Nouvelles
---

On racontait que dans les couloirs les plus sombres de la base-ville de Pôle Sud existait un homme surnommé Sans-Âme. Cette personne au visage cachée par un masque blanc qui était presque une légende urbaine dans les sous-sols.

Les êtres quasi-entièrement mécanisés existaient depuis des années déjà, depuis le milieu des années 2050, mais cet personne était allée encore plus loin. C’était son esprit même qui aurait été transférée dans un cerveau cristallin, cerveau universel des IA.

&nbsp;

Il était considéré comme le premier être à avoir abandonné entièrement son humanité, à être devenu entièrement machine.

***

Une erreur dans la mise d’une sécurité. Une déflagration. Une douleur intense. L’impression de sombrer.

&nbsp;

Ovide avait du mal à se rendre compte de ce qui se passait autour de lui. Il avait mal comme jamais il avait souffert. Il paniquait. Est-ce que c’était la fin ? Il lui était impossible de bouger, de voir. Chacun de ses membres était brulé. Il lui fallait toute sa volonté pour rester conscient. Ses vêtements avaient aussi brulés à même le corps. Un long moment avec des voix qui tentaient de cacher leur panique. Une voix proche de lui disait que tout allait bien se passer, qu’ils allaient le soigner.

Il sentait qu’il était transporté. Il arrivait à vaguement entendre les voix qui parlaient de sujet qu’il n’arrivait pas à comprendre.

Puis le noir.

&nbsp;

Il était ailleurs. Il avait perdu connaissance. Il était sur un support un peu mou… Etait-il à l’hôpital ? Il entendait des cris et des pleurs… ses parents. Ils devaient être horrifiés par ce qui se passait. Il entendit la voix d’un médecin qui lui parlait.

— Monsieur, votre corps est gravement brûlé. Certains de vos organes internes ont surement été gravement endommagés par l’explosion. Votre colonne vertébrale est sectionnée. Nous pouvons faire quelque chose pour vous sauver, mais il nous faut votre accord. La société Stahl Corporation vous demande si vous acceptez un traitement expérimental, qui consisterait à tenter une robotisation complète. Votre esprit serait numérisé dans un cerveau à cristaux reconfigurables, et serait intégré à un corps androïdique.

&nbsp;

Le jeune homme avait toujours été contre cela. Ses parents et lui-même avaient toujours été des proches de l’Homo Novus. Ils étaient contre toutes les formes d’implant cybernétique, estimant que c’était une manière de fuir sa réalité et de dénaturer l’humain. Et ce qu’on lui demandait était encore plus qu’une mécanisation complète, qui gardait un cerveau biologique, et la colonne vertébrale – c’était sans doute l’absence de cette dernière qui la rendait impossible.

Cependant, il avait mal comme jamais, et était au bord de la mort. Il avait peur.

&nbsp;

— Tou-tout ce que vous voudrez, bredouilla-t-il avec difficulté.

&nbsp;

Il commença à être mis sous anesthésie, entendant alors qu’il s’enfonçait dans le néant les cris horrifiés et furieux de ses parents.

***

Ovide se réveilla. Il ouvrit les yeux. Il voyait. Il regardait autour de lui. Il tenta de descendre du lit, mais tomba, ayant mal anticipé son saut. En se relevant, il comprit rapidement pourquoi : il était plus petit qu’avant. Il n’était pas d’une taille minuscule – une dizaine de centimètre de moins – mais fait de voir le monde de plus bas était suffisant pour le déséquilibrer.

Ses premiers pas furent difficiles : il allait lui falloir un peu de temps pour s’adapter au corps. Il était partagé entre une forme d’allégresse d’être sauvé et une honte de s’être dénaturé pour sa survie. Il se dirigea vers le miroir, pour voir son corps robotique. Cependant, il était rassuré comme jamais : il se sentait toujours être lui-même, et ressentait vraiment des émotions et des sentiments.

&nbsp;

La première chose qu’il remarqua était que le corps était un corps de très haute qualité. Il était incapable de dire la différence entre ce corps et un « vrai corps ». Il n’en avait encore jamais vu des comme ça. Le corps était androgyne, et avait des traits peu marqué. Les cheveux étaient plutôt longs. Il portait une grande tunique blanche, le strict minimum. Le corps était également asexué. C'était le corps générique des androïde utilitaire devant avoir l'air le plus humain possible.

&nbsp;

— Désolé, fit une voix derrière lui. Nous n’avions pas prévu que nous aurions à faire une opération si tôt, et nous n’avions donc qu’un corps très « générique » de base, afin de pouvoir convenir dans la plupart des cas de personnes, et nous n’avons placé dessus que le strict minimum pour vivre : vous pourrez le modifier à votre convenance plus tard en commandant ce qu’ils vous faut – cela ne nous regarde pas du coup on vous laisse le choix – et nous voulions vous réveillez le plus tôt possible, afin de permettre votre adaptation.

&nbsp;

Ovide sursauta – donc un corps robotique était toujours capable de sursauter – et se retourna, voyant un homme seul qui tenait un carnet.

&nbsp;

— Je me présente, continua-t-il. Joseph Cromwell, directeur de recherche en robotique et cyberisation du groupe Stahl. J’ai supervisé votre numérisation dans le cerveau cristallin, et lorsqu’on m’a dit que vous alliez vous réveillé, j’ai préféré être là. Je m’excuse de ne pas avoir montré ma présence plus tôt.

&nbsp;

Ovide hocha la tête, murmura que ce n’était pas grave. Il tenta d’assimiler tout ce qu’on venait de lui dire. Tout ceci n'était pas vraiment ce qui l'embarrassait le plus. Il avait bien compris qu'une grande partie de son nouveau corps était modifiable, et une autre question le taraudait quand a son apparence, bien plus importante que des questions « d’accessoires » ou de cheveux :

— C’est possible de récupérer mon visage ? Autant être plus petit, ça me dérange pas trop, autant j’aimerais bien pouvoir me reconnaître moi dans la glace, quoi…

— C’est tout à fait possible, mais pas encore prêt. Nous devons encore attendre la fin de l’analyse 3D de vos photos afin de vous fournir votre visage, ça malheureusement on ne peut pas en prévoir d’avance.

&nbsp;

Le jeune nouvel androïde comprenait. L’idée de rester un temps sans son visage le gênait, mais il comprenait.

&nbsp;

— Et pour mes parents ?

— Ils sont deux pièce à côté, dans la salle d’attente, à discuter avec votre médecin, mais à votre place j’attendrais pour-

&nbsp;

Ovide n’avait pas entendu. Il sortit dans le couloir et se dirigea vers la salle d’accueil pour aller rassurer ses parents, leurs dire qu’il allait bien. Il espérait qu’ils comprendraient son acte de peur, et qu’ils ne le verraient pas trop comme un lâche après ce qu’il avait fait. Cependant, en entendant les éclats de voix de ses parents derrière la porte fermée, il n’osa pas aller au bout, et écouta à la porte

&nbsp;

— Nous avons dit que nous ne voulons que le corps pour pouvoir l’enterrer ! Vous avez tué notre fils, ne croyez pas que nous voulons voir votre pantin !

&nbsp;

Ovide se prépara à passer la porte, pour aller les rassurer, pour leur dire que tout allait bien, et qu’il était bien lui, et qu’il ressentait les choses.

&nbsp;

— Je vous dis qu’il va très bien, le transfert s’est fait parfai-

— Nous avons déjà dit que votre transfert, nous n’y croyons pas. Vous avez charcuté le cerveau de notre fils, encore vivant, pour tenter de copier ce qu’il pensait, le rendant en mort cérébrale. Vous avez ensuite laissé mourir son corps, profitant de sa douleur et de sa peur pour avoir l’occasion de vous faire de la pub. Vous voulez jeter son corps comme un vulgaire déchet.

— Je vous dis que tous les tests montrent que-

— Stop avec vos « tests » ! éructa son père. Nous savons bien ce qu’il en retourne. Votre robot sans âme n’est pas notre fils. Notre fils, vous auriez pu faire pleins de chose pour le sauver, même si sa vie n’aurait pas été parfaite, et même si son corps n’aurait été plus pur. Mais vous avez choisi de le tuer, et de le copier pour créer un pantin sans esprit, qui ne fait qu’imiter l’apparence d’un esprit humain.

&nbsp;

Ovide se retourna, et marcha vers sa chambre. Il découvrait sur le chemin qu’il était capable d’avoir des larmes. Il comprenait exactement ce qu’ils pensaient. Il avait pensé la même chose. Chez l’Homo Novus, ils pensaient qu’une simulation réussie ne constituait pas le fait de « ressentir ». Qu’un robot ne pouvait avoir de vrai ressenti, de véritable « qualia ». Qu’ils ne faisaient qu’émuler des sensations en les stockant, imitant les comportements de manière à donner l’impression qu’elles sont présentes, également à lui-même. Qu’il n’avait pas peur, pas de plaisir, pas d’amour : il ne faisait que croire au monde et à lui-même qu’il en avait.

Que derrière, on était proche du zombie philosophique, un programme sans esprit qui imite l’apparence d’un esprit humain.

&nbsp;

Sur le chemin, il croisa Mr. Cromwell

&nbsp;

— Vous pouvez annuler la production de mon visage, lui dit-il avec amertume. Je crois que je n’en aurais plus besoin.

&nbsp;

Il ne laissa pas le temps à Cromwell de répondre, et retourna dans son lit. Tout ce qu'il voulait, c'était qu'on le laisse tranquille. Qu'on lui laisse le temps d'avaler ce qui venait de se passer, ce qu'il venait d'entendre. Il n’avait pas envie d’interagir avec qui que ce soit. Après tout, qui pouvait gagner à discuter avec un être sans esprit ?

Il avait besoin de temps.

***

Les années avaient passé. Aujourd'hui, Ovide était devenu une légende. Suite au scandale provoqué par ses parents, sont expérience n'avait à sa connaissance jamais été reproduite. Il s'était habitué aux insultes de l'Homo-Novus à son encontre. « Zombie », « Altéré ».

Pour beaucoup, il était un monstre.

Au début, Ovide avait tenté de montrer ses émotions, de montrer qu’il avait vraiment la capacité de ressentir. Il avait raconté des blagues, avait ri, pleuré, aimé, haït.

Comment prouver ce qui était invisible, ce qu’on ressentait au fond de nous, ce qui par définition était caché à autrui ? Que son programme n’était pas une version numérique du Malin Génie, avec pour unique objectif de tromper les pauvres hères qui seraient trop naïf pour croire à cette fable que serait son âme ? Le doute carthésien devenait alors une arme pour refuser l’humanité d’autrui. Et ils avaient réussi. Leur cible doutait parfois doutait que ses sentiments et ses émotions étaient réels, et pensait être dépourvu d’esprit, uniquement habité par le fantasme artificiel d’être réellement humain de processeurs.

Et ça, c’était quand ce n’étaient pas les théories conspirationnistes, prophétisant que « Sans-Âme » serait à l’origine de la tant crainte « révolte des machines ».

&nbsp;

Ovide se laissa tomber sur le canapé de son appartement. En bazar comme toujours, il ne prenait plus vraiment le temps de ranger. Il avait encore été insulté aujourd'hui, et n'avait pas trop l'humeur à quoi que ce soit. Les mots résonnaient encore dans sa tête.

— Sale machine, à cause de toi Ovide est morts. Ne crois pas que tu as le droit de le remplacer. C’était un garçon si gentil et serviable, il ne méritait pas que ses derniers instants soit de se faire charcuter le cerveau pour rendre célèbre un connard de milliardaire pour un robot qui ne pense pas.

— Et si je pense pas, connard, est-ce que vous savez que cela ne sert à rien de m’insulter, avait-il songé.

Mais le danger était trop fort pour se permettre de faire des taunts. Ovide avait réussi à fuir les hommes qui l’attaquait, mais pas la violence de leurs mots.

Il repensait à tout son vécu. Les différents corps que Judith, sa meilleure amie lui avait fait essayer, après son refus de retourner chez ses parents. Son appréciation croissante des différentes possibilités que lui apportait cet aspect-là. Le fait de pouvoir changer de corps suivant ses besoins et de ses ressentis. Ses différentes connexions à des corps tout sauf humains, des machines.

***

Cependant, aujourd'hui, quelque chose se produisit. Il entendit sonner à sa porte. Méfiant, il s'approcha de la porte. Était-ce une des personnes qui l'avait suivi ? Il fut surpris de voir que non. C'était une jeune femme qu'il ne connaissait pas.

&nbsp;

— Mon-monsieur Ovide ? hésita-t-elle. Je me nomme Élysa. J'aurais besoin de vos conseils… Vous êtes le seul à pouvoir aider mon ami ici-présent, c'est très important.

— C'est bien moi, répondit simplement Ovide, peu habitué à ce genre de politesse. Qu'est-ce que je peux faire pour v…

Il s'arrêta. Il comprit.

&nbsp;

Derrière la jeune femme se tenait un petit robot, en forme de chat anthropomorphique noir, ne faisait pas plus d'un mètre vingt. Cependant… quelque chose n'était pas habituel dans ses expressions. Il se cachait, regardait avec une anxiété visible l'androïde, accroché à la jeune femme. Il tentait de ne pas montrer son visage, comme s'il en avait honte. Des émotions qui n'étaient que rarement reproduit chez les robots. Dans un corps totalement inadapté à avoir une base humaine.

Ovide resta bouche-bée. Cet être était un robot visiblement entièrement technologique. Mais avait des émotions complètes.

&nbsp;

Après des années, Ovide n'était plus seul.
