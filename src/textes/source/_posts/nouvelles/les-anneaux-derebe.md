---
import: ../../../../crowbook/common.book
title: "Les anneaux d'Érèbes"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2019/10/01 22:27:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/les-anneaux-derebes.pdf
output.epub: ../../../../../dist/download/les-anneaux-derebes.epub
download: les-anneaux-derebes
categories:
  - Nouvelles
---
Rose se souviendrait toute sa vie de son arrivée dans les Anneaux d’Érèbe.

&nbsp;

Elle avait huit ans, et était à moitié assoupie dans la navette Char d’Hermès qui l’amenait jusqu’aux Anneaux, bercée par le doux vrombissement des moteurs du véhicule spatiale. Avec juste entre ses bras le dernier souvenir qu’il lui restait de chez elle, sa fidèle peluche d’un animal dont elle ne connaissait pas le nom, elle avait du mal à totalement s’endormir. C’était comme un cauchemar qui se terminait.

Elle avait du mal à croire qu’elle allait enfin quitter la guerre entre sa planète et celle voisine. Qu’elle allait avoir dans les Anneaux enfin l’abondance et une vie paisible.

&nbsp;

Le visage collé au hublot, elle commençait à voir se former les Anneaux. De gigantesques stations spatiales en forme de beignet, flottant majestueusement dans l’immensité du vide. La lumière en éclairait totalement un côté, révélant des structures tournantes complexes imbriquées ensemble pour former l’anneau. Dans les parties obscures, seules les lumières extérieures de la colonie se voyaient, formant des points mouvant lentement, comme avec solennité. Cet endroit était celui de la paix. Cet endroit était celui où elle pourrait vivre autrement.

Elle sera fort Guide, sa peluche, tandis que la fatigue finissait par avoir raison de sa volonté à rester éveillée pour voir toujours plus des anneaux.

&nbsp;

C’était le début d’une nouvelle vie.

&nbsp;

Après quelques dizaines de minutes, Rose fut réveillée par l’annonce qu’ils allaient descendre. Elle attrapa par habitude la main de son voisin et meilleurs ami, un autre des orphelins de sa planète qui avaient été recueillis pour aller vivre dans les Anneaux. Celui-ci sursauta, visiblement aussi a moitié endormi. Les enfants furent amenés jusqu’à la porte de sortie par les adultes. Tout le monde restait silencieux, assez impressionné – et un peu intimidé – par leur arrivée. Mais Rose remarqua rapidement ce qui intimidait tout le monde. Une être proche des humains, de grande taille, avec un teint gris mat, des oreilles un peu pointues et un air presque elfique.

Une phosphorienne.

&nbsp;

Ce nom avait été donné à une espèce qui était en train de naître chez les humains, au contact de certains phénomènes.

Rose connaissait les histoires qui narraient qu’ils avaient obtenue depuis quelques siècles le pouvoir de voyager à travers le temps et les mondes. Elle-même se sentait un peu intimidé, n’en ayant jamais vu hors des histoires et de quelques fois où elle avait pu avoir accès au réseau numérique interplanétaire.

&nbsp;

— Bonjour, les enfants. Je suis Lyra, et je suis contente de vous accueillir dans les Anneaux d’Erebes. J’espère que vous serez heureux dans ces nouveaux foyers. Nous espérons de tout cœur que si nous ne pourrons vous faire oublié les horreurs de la guerre que vous avez connu, nous pourrons panser les blessures qu’elles ont laissé en vous.

&nbsp;

Elle regardait la phosophorienne, qui lui tendit un bonbon, à elle comme a son ami, alors qu’elle passait, et commençait à entrer dans de long couloir blanc et neutre qui allait l’amener jusqu’à l’intérieur de l’anneau. Pour les enfants, ce trajet était interminable. Ils avaient hâte de voir leur nouveau chez-eux, et avait envie de courir après les heures a être resté assis. Certains mangeait leur friandise en silence, d’autres commençait à s’agiter un peu.

Cela fut après ce qui leur sembla être une éternité qu’ils arrivèrent à l’intérieur des Anneaux, et firent face à un spectacle tel qu’ils n’avaient jamais vu : un monde sans ciel et nuage, tout ce qu’ils voyaient en regardant en l’air était un paysage lointain vu de haut, comme s’ils regardaient depuis un hublot d’avion ou de nevette. Ils pouvaient voir une forêt vu de haut, et pleins d’habitations. C’était comme si la terre se refermait dans le ciel. Tout l’intérieur de l’anneau était un cylindre dans lequel on pouvait voir un paysage qui remontait jusqu’à haut dans le « ciel ». Ils étaient trop jeunes pour comprendre que c’était la rotation des anneaux qui formaient la gravité artificielle de chacun de ces cylindres qui le composait.

Rose regardait avec attention ce nouveau ciel qui serait le sien.

&nbsp;

Elle se souviendrait toute sa vie de son arrivée dans les Anneaux d’Érèbe.

&nbsp;

Les années passèrent et Rose avait grandit dans les Anneaux. Elle était entrée dans l’armée de l’Alliance Humaine afin d’aider l’Alliance à protéger tous les habitants. Cependant, comme toujours, petit à petit la perfection qu’elle voyait durant son enfance s’était estompée. Elle voyait que les phosphoriens n’étaient pas les elfes magiques qu’elle voyait en eu quand elle était petite et que leur jugement des humains « non évolués » était parfois plein de mépris et de dégouts. Que l’Alliance Humaine était également un groupement politique complexe, avec son lot de corruption, jugement hâtif, haines refoulées et guerres ouvertes. Elle avait avec le temps également appris les circonstances de la guerre qui avait opposé son peuple avec une petite planète Centaurienne. Une histoire de rivalité financière qui avaient dégénérés en une guerre totale. Et que les phosophoriens et l’alliance n’avait pas sauvé par bonté de coeur, mais par envie d’avoir un front pour être assez puissant face aux autres espèces de l’univers, et des autres univers qui commençait à être connu. Tout ceci donnait un goût presque amer à ses souvenirs merveilleux. Rose ne pourrait-elle jamais retrouver la magie de son enfance dans le monde ? Le fait d’avoir l’impression que des gens n’étaient là que pour la sauver de son malheur. Cela semblait impossible.

Sauf un jour.

&nbsp;

Devenue âgée et éloignée de tout cela, elle attendait dans un véhicule spatial. Le vrombissement des moteurs faisaient du bien à son dos, et elle était heureuse. Oh, la magie de ce trajet n’était plus pour elle, combien de fois avait-elle fait ce genre de voyage ! Non, il était pour les personnes à côté d’elle. Un jeune zoomorphe tigre, d’à peine dix ans. Il portait un petit tee-shirt avec un gros poney à l’air un peu stupide dessus – le premier qu’il avait voulu après avoir pu retirer sa tenue stérile blanche des Centres. Et une jeune petite zoomorphe chauve-souris, qui portait fièrement ses couettes et sa jupe – tenues qu’on lui refusait dans les Centres d’Entrainement, sous prétexte des caractéristiques qu’on lui avait attribué à la naissance – tout en cherchant s’il ne restait pas quelques bonbons dans le sac qu’elle avait rapidement englouti. Les deux enfants regardaient d’un air émerveillé les Anneaux d’Érèbe, toujours aussi majestueux malgré les années, loin des considérations politiques et des découvertes sombres.

Et Rose était heureuse. Ces enfants n’auraient jamais à apprendre un jour que leur arrivée ici n’était que le fruit de volonté politiques et économique de renforcement d'un empire. Ils auraient le droit de garder la magie de cet instant.
