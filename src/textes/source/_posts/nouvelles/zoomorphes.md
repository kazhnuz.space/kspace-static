---
import: ../../../../crowbook/common.book
title: "Zoomorphes"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2016/03/26 10:25:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/zoomorphes.pdf
output.epub: ../../../../../dist/download/zoomorphes.epub
download: zoomorphes
categories:
  - Nouvelles
---

*La philosophie, les grandes doctrines ne sont-elles pas parfois que le résultat d’une bonne communication, dirigée à des fins économiques ? C’est ce qu’on pourrait se demander parfois. Combien de grandes avancées sont dues à des changements économiques plus qu’aux grandes idées ?*

*Sur cet aspect-là, certains diront sans doute, un peu cyniquement, que c’est le résultat qui compte. Que c’était au final bien pratique si l’ensemble des volontés égoïstes, des ambitions personnelles pouvaient faire avancer la société vers un mieux, poussée vers le progrès par une main invisible. Mais un jour, nous avons accepté de ne pas seulement aller à l’encontre du progrès pour des raisons économiques : Nous avons accepté de faire une terrible infamie, dans l’espoir de rendre l’économie plus « prospère ».*

*Nous avons rendu produit ce qui n’aurait jamais du l’être. L’éthique a été mise sous le tapis de la relance économique.*

*Nous avons trouver une nouvelle excuse pour déshumaniser et asservir.*

&nbsp;

Dans Sélénite, capitale de la Confédération Solaire, ce début d’année 2233 avait été très chargé. Richard Neyes, président pour encore deux ans, avait reçu de nombreuses visites. Cela ne faisait que 32 ans que la Confédération existait, et commençait à tenter d'organiser le système solaire. Cependant, les soucis ne manquait pas. Toujours des restes de nostalgie de l'époque ou tout l'espace n'était que des colonies terriennes, la construction de biosphères artificielles illégalement sur Mercure et Cérès - qui n'étaient pas encore organisé en territoire, des soucis avec les Versatiles - la nouvelle génération d'androides composé de nano-robots - et une crise économique majeure lié à l'augmentation des prix de la robotiques.

Heureusement, cette journée, ce n’était qu’un rendez-vous avec Juan Mobes, directeur de la société SymbioSys, une société travaillant sur le transhumanisme et sur l’ingénierie biologique. Ils avaient notamment réalisé une puce cérébrale intelligente aux fonctionnalités allant du simple gommage de musique-récurrente-agaçante-dans-la-tête à la recherche internet instantanée, la capacité de stimuler les sens, très utilisés que ce soit pour des jeux vidéo encore plus réaliste, pour l’apprentissage néo-sensoriel ou des utilisations plus porté vers l’érotisme ou la pornographie. Si le dernier n’était jamais indiqué dans les publicités, c’était un des marchés les plus porteurs. Ils avaient aussi réalisé des traitements contre des maladies graves, des anti-vieillissement très efficaces et des petits animaux de compagnies conçu pour contenir ce qu’il y avait de plus mignon dans au moins dix espèces d’animaux. Ainsi que beaucoup de critiques pour faire passer leur marché avant les considérations éthiques, la tentative de masquer derrière diverses stratégie de management les soucis d'exploitations qui y existe, ainsi que leur lobbying au niveau du président.

Le président était malgré les fortes critiques assez content de le voir. L'entreprise avait créé de nombreuses invention qui avait eu un fort succès dans une grande partie de la confédération, et l'entreprise était bien vue pour son fort engagement il y a longtemps lors des guerres d'indépendance. Pour lui, cette entrevue allait être sans doute soit l'avant première d'une "grande découverte" de SymbioSys à laquelle il ne comprendrait rien, soit une invention totalement gadget qu'ils aimeraient bien voir être utilisée en public. Neyes avait eu quelque scrupules a être utilisé comme ça en homme-sandwich, mais c'était normal pour le président d'aider l'entreprise qui avait aidé à la victoire. Ils étaient des héros.

Le whisky était déjà choisi – une bonne marque – et le président attendait dans son bureau. Au bout de quelques minutes, et exactement à l’heure prévue, il entendit frapper à la porte. Mobes se tenait devant la porte, accompagné d’une personne, visiblement de petite taille, entièrement emmitouflée dans un manteau. Ses gardes firent les gros yeux au PDG de NewSymbiosys : Laisser entrer quelqu’un qui se couvrait comme ça ? Pas question. Cependant, Neyes leur fit signe de les laisser entrer. Il activa juste – au cas où et pour rassurer ses gardes – son champ de force personnel. En cas d’attaque, il ne pourrait pas être atteint.

&nbsp;

Juan Mobes fit un signe de la main, et dit à l'être qui l'accompagnait de s’asseoir, avant de faire de même. Le président remarqua qu’il lui parlait avec un ton paternel mais autoritaire… Il lui demanda le motif de sa visite, tout en lui servant un verre de Wisky. Un bon verre, puisque cette marque spéciale contenait de l’alcool à seuil, qui autorisait à être un peu pompette mais qui n’allait jamais trop loin, ce qui permettait de mieux profiter du breuvage.

— Ah, monsieur le président, je viens apporter une réponse à la crise des robot-travailleurs.

Cette « crise » était un problème. Depuis 65 ans, l’exploitation des planètes et planètes naines jusqu’à la ceinture d’astéroïdes, le début du puisage d’hydrogène dans Jupiter, et l’explosion de la production industrielle (qui était devenu exponentielle avec les besoins de croiseurs et de bases spatiales) avaient provoqué une demande en travailleurs mécanique, moins couteux que les humains. Cependant, les besoins étaient devenus tels que ces travailleurs étaient considérés comme trop coûteux, étant assez complexes à construire et contenant beaucoup de matériaux rares. De même, leur utilisation dans le contexte humain avait été extrêmement critiqué, par un besoin d’interlocuteur vivants. Ces deux éléments étaient ce qu’on appelait le crise des robots-travailleurs. Cependant, la situation était également bloquée sur le second aspect par le refus de recourir au salariat, trop coûteux.

— Quelle est cette réponse ? S’enquit le président.

Avant de répondre, le PDG retira d’un grand geste la cape de l’être qui l’accompagnait. En dessous, à la stupeur du président, se trouvait un chat anthropomorphique, ayant un corps en grande partie humanoïde, à l’exception d’une tête féline et d’un corps entièrement recouvert d’une fourrure tigrée. Il regardait le président d’un air docile, et ne dit qu’un petit « Bonjour monsieur », poli. Le président ne savait pas quoi dire

— Voici Answer, la solution à notre problème, annonça fièrement Mobes. Nous avons écouté tous les avis pour chercher la meilleur solution, et avons décidé de ne pas répondre au problème uniquement par la technologie. Il était évident que les robots commençaient à être une solution non convainquante. De même, il était impossible de revenir au trop couteux salariat, pour de simple ouvrier ce serait une catastrophe. Donc voici notre nouveau produit, les zoomorphes ! La marque est en cours de dépots. Il est capable de faire des calculs, et maîtrise les savoirs nécessaires à être un ouvrier semi-qualifié. Un exemple tout bête, il connaît ses tables : Answer, quelle est la racine carré de soixante-quatre ?

— Huit, répondit calmement le jeune zoomorphe.

Le président restait bouche bée. SymbioSys avait créé des *chimères humaines* sans qu’il le sache ?

— Les zoomorphes sont basé en grande partie sur du génome d’espèce animales. Nous n’avons utilisé aucun gène humains qui n'existent pas chez d'autres espèces, nous ne nous sommes inspirés que de nos gènes brevetés pour l’amélioration des capacités mentales, déjà utilisé dans notre espèce de chats qui parlent. Évidemment grâce à des gènes tirés de grands primates, l’intelligence d’un zoomorphe est plus grande que celles de ces petits animaux de compagnies. Ils sont capables de réalisés des tâches simples, et sont d’une extrême obéissance. Ils ne peuvent ni se révolter, ni agresser des humains, grâce à l’utilisation des technologies de notre filiale d’hypnose combiné à l’énonciation améliorée des lois robotiques d’Asimov faite par notre filiale spécialisé dans l’ingénierie philosophique. Et grâce à nos méthodes d’accélération de croissance, un zoomorphe met 1 an à être conçu, pour un prix moindre que le moins cher de nos robots, et ce même sans compter les trois-cent-quatre-vingt-neuf brevets qui majorent le prix d’un robot et l’abonnement nécessaire pour que nous entretenions le robot régulièrement. Et ce ne sont que les prototypes, nous visons des modèles commerciaux environs 2 fois moins chers.

&nbsp;

Le président tapa du poing sur la table, furieux.

— Mon très cher monsieur Mobes, commença-t-il avec une voix froide. Je peux vous dire que c’est une honte, ce que vous montrez là. Vous avez fait de grandes choses par le passé, mais vous rendez-vous compte de ce que vous faites ? Vous tentez de tricher avec la constitution de notre pays, et avec les droits de l’homme !

Le PDG déposa son verre de whisky sur la table et s’enfonça confortablement dans son fauteuil. Il était beaucoup moins souriant. Légèrement nerveux, même. C’était étrange, il semblait ne s'être pas attendu à cette réaction. Du moins, pas aussi rapidement ni directe.

— Je vois que nous allons avoir un problème, monsieur le Président. Et que vous comprenez mal la situation. Ce n’est pas un homme, que vous voyez là, mais un zoomorphe. Je ne fais que créer une nouvelle sorte d’animal, qui sera plus pratique pour l’homme pour faire toutes les taches ingrates qu’il ne peut lui-même faire et qu’il ne peut déléguer aux robots. Voyez ça comme les bœufs utilisés jadis dans les champs.

— Vous savez parfaitement que non, rétorqua brutalement le président. Ce que vous faite, ce sont plutôt des êtres humains qui aurait des apparences bestiales, pour pouvoir en faire des esclaves. Vous vous êtes dit que la seule chose qui pourrait convenir pour remplacer les machines, ce serait l’homme, ironiquement, et vous avez donc décidé d’en créer artificiellement avec une autre apparence, pour que ça passe.

Le PDG reprit son verre de whisky, et le porta à ses lèvres. Il y eut un petit temps de silence. Answer restait calme, bien qu’un peu gêné, comme si le fait que son maître se fasse critiquer lui était difficile à accepter.

— Monsieur le président, soyez raisonnable, essaya de calmer Mobes. Leur intelligence n’est au niveau que d’animaux comme le dauphin ou les grands singes, et la parole était déjà présente chez d’autres créatures. L’apparence bipède de nos créatures est due qu’à des besoins pratiques, et se base sur celles des grands singes. D’ailleurs, les pieds d’Answer sont munis de pouces préhensiles. Ce seront que des animaux. Vous n’avez pas vu d’inconvénient pour mes animaux de compagnies antiallergique, non ? Ni pour mes animaux à viande amélioré. Il n’y a ici pas vraiment de différence : il ne s'agit que d'un animal, adapté aux besoin que nous lui donnons.

— Il y a une différence. Vous précédentes créations n’étaient que de simples manipulation génétique, qui avait été testée avant, affirma le président. La, ce que vous avez créé est une nouvelle créature *intelligente* et *consciente*, que vous voulez qu'on puisse exploiter. C’est inacceptable.

— Exploitation, exploitation… répéta le directeur d’un air fatigué. Vous me semblez trop fixé sur cette idée. Nous ne retournons pas à l'esclavage. Quand vous vous rendez dans une ferme, parlez-vous d’esclavage des animaux ? D’ailleurs, vous pourriez aussi bien parler de sacrifice, de condamnation à mort ou de cannibalisme à propos des abattoirs ! Mes zoomorphes seront sûrement bien mieux traité que cela, vous savez. Je suppose qu'il faudra bien évidemment interdire leur viande d’être consommée. Question d’éthique !

— La question justement est là, répliqua le président. Vous dites toujours « animaux » pour indiquer qu'ils seraient radicalement contraires à nous et aux aspirations qu'on peut avoir… mais qu’est-ce qui différencie véritablement vos zoomorphes de nous ? C'est le même scénario qui se répète constamment. Face à d'autres êtres, on décrète qu'une différence est suffisante pour les voir comme étant "moindre". L’horreur de l'esclavage de jadis n’était pas que de faire travailler des humains sans les payer. Ces humains étaient déshumanisé à cause de leur origine éthnique. On a fait de même pour des raisons de religions, sexe, genre, orientation sexuelles.  On a toujours chercher des excuses pour justifier un système injuste. Et même aujourd'hui, les ombres de tout cela continue à planer et alimenter mépris et haine.

Le président fit quelques pas en avant, se rapprochant de Mobes.

– Ici et aujourd'hui se trouve devant moi un être qui pense et qui a conscience de lui-même. Il est donc pour moi tout aussi digne d'être humain que vous et moi, d'avoir exactement les mêmes droits. Vous n'avez fait qu'inventer une nouvelle de ses excuses, inventer de toute pièce des êtres fait pour être attaqués, pour tenter de faire croire que c'est bon, vous aviez abandonné les précédentes.

L'entrepreneur eut un rictus.

— Qu’est-ce qui les différencie de nous ? La génétique, répondit simplement. Ils sont plus éloignés de nous que le sont les grands singes. Hors, je ne crois pas que vous ayez donné à Cheeta la citoyenneté solaire, si ? Si vous accepter l’idée que mes zoomorphes sont « humains », ou « digne d’être considéré comme humain », si c’est par l’intelligence, j’attends vos excuses officielles pour le génocide des chimpanzés, conduits à la porte de l’extinction. Si c’est pour leur intelligence, leur « conscience », j’attends que vous accordiez la citoyenneté aux derniers grands singes, aux dauphins, à certaines espèces de perroquets – notamment nos Gris de la Lune améliorés – à une certaine espèce de poulpe et aussi à toutes les intelligences artificielles utilisant le moteur ALI depuis les versions 4.x, notamment les Versatiles.

Le président était furieux. Il savait que les dires de Mobes pourraient parfaitement convaincre au moins une partie du parlement solaire, et peut-être même au moins une petite partie de la population interplanétaire. Oh, il y aurait des refus, des critiques, mais beaucoup étaient fixé sur les dégâts de la crises des robots-travailleurs, surtout dans le domaine des services. Il était vrai que sur ce point de vue, les zoomorphes apporteraient une solution, d’autant plus que les biotechnologies de Mobes semblaient vraiment avoir permit de rendre la production de ces zoomorphes peu coûteuses. Mais à quel prix ? Allait-il devoir fermer les yeux sur cette horreur à cause de la crise économique ? Devait-il accepter l’esclavage pour la reprise ?

Juan Mobes semblait à nouveau sourire, mais cette fois de l’absence de réponse de son adversaire. Il leva son verre, comme pour célébrer sa victoire à la joute verbale. Le président devinait que par la, Mobes voulait estimer que le débat était terminé. Et ça, il n’en était pas question.

— Je rajouterais que les expérimentations comme cela demande une autorisation officielle du président, rappela Neyes. Sans cela, la création de chimère est un acte illégal, passible d’une condamnation grave pour votre société.

Le président n’avait pas envie de jouer à ce jeu-là, mais il n’avait pas le choix. Il regardait le zoomorphe devant lui. Il en était à son troisième mandat de sept ans, entamé de 5 ans, soit 19 ans de présidence. Il avait vu des tas de projets d’animaux, mais jamais rien de semblable. Mais le PDG sortit de son manteau un papier. Une autorisation d’expérience pour un projet nommé « zoomorphisme », visant officiellement à « expérimenter sur l’intelligence animale de sorte à les permettre de réaliser des résolutions de problèmes lié aux travaux ». Signée il y a de cela 25 ans par son prédécesseur. Le président fut surpris. Cela datait des premiers signes avant-coureur de la crise des robots-travailleurs.

— C’est un très vieux projet, que nous avons préparé depuis des années, répondit Mobes, l’air cette fois un peu amusé. Nous avons très vite compris en fait ce dont nous avions besoin.

Il y eut un temps. Le président regardait, furieux, le PDG, qui soutenait son regard avec un air mélangeant une sorte de compassion paternelle, presque rabaissant et du regret. Comme une sorte de « c’est dommage d’en arriver là ». Le jeune zoomorphe lui semblait moins calme, s’agitant nerveusement, mais ne disant rien, toujours autant en retrait. Bien dans le rôle que lui donnait SymbioSys.

— Nous ne faisons pas que ça pour nos revenus, Richard. Nous faisons cela parce que c’est la meilleure chose possible pour l’humanité. Cette crise doit être résolu. Et nous savons que vos collègues du parlement seront bien plus réceptifs à notre message… Je vais prendre congé de vous.

Juan Mobes se leva, et sorti de la salle, accompagné du jeune zoomorphe, sans que le président puisse faire quoi que ce soit. Même s’il y avait beaucoup d’agressivité dans le regard d’Answer, le président fut certain d’apercevoir une lueur de peur, et d’espoir. Se rendait-il compte de ce qu’il risquait de lui arriver, lui et ceux comme lui ? Où alors avait-il peur de ne finir par être qu’une invention non autorisée, qui serait alors piquée ? Le président ne savait plus quoi dire, plus quoi faire. Il devait commencer à faire campagne contre ce projet. C’était la dernière chose qu’il pouvait faire.

&nbsp;

Quelques mois plus tard, l’annonce fut faite, les semaines, les mois de débats ensuite furent extrêmement violents. L’exploit technique fut célébré par certains. D’autres calculèrent les économies que ça ferait. Certains les remettaient en doute.

Et surtout, une grande opposition eut lieu… mais pas celle qu’espérait le président. De nombreux partis et personnes qualifiaient les zoomorphes de « monstre de Frankenstein conçu uniquement pour détruire encore plus les travailleurs ». Ce mot revenait souvent : « des monstres ». Pour eux, il ne fallait pas éviter de faire vivre à des êtres le sort de la déshumanisation : Il fallait les détruire, comme des créatures impies. Il y eut aussi ceux qui se méfiaient de ces créatures. Et s’ils étaient agressifs ? Et s’ils étaient dangereux ? Et les maladies ? Et les pulsions sexuelles ? Ils savaient qu’ils pouvaient se reproduire, même si une grande partie des mâles et femelles sur le marché seraient stérilisés ou castrés pour des raisons de sécurité et de comportement – notamment pour ceux dédié à servir dans des milieux avec des enfants – est-ce qu’il n’y avait pas alors quand même des risques ?

Mais au soulagement de Neyes, il y eut quand même des militants contre la création d’êtres « prêt-à-soumettre ». Notamment des groupes qui militèrent pour la libération immédiate et l’obtention des droits citoyen pour la nouvelle espèce.

Et finalement, le vote eut lieu, après des semaines de débat houleux. Les membres des principaux partis pour se sont mobilisé comme jamais pour voter. Cela leur permis de gagner face à l’opposition farouche, mais qui était divisée sur plusieurs fronts. D’autant plus que le principal parti opposé fini par être en grande partie minée par l’abstention, après une guerre interne lié au sujet.

Et cette nouvelle situation fini par devenir la norme, au bout de quelques mois. Avoir un ou une zoomorphe chez soi devenait presque un signe de richesse. Des serviteurs considérés comme « modernes » et « hype ». Les « modèles » basés sur les félidés et les canidés, ainsi que toutes les autres créatures considérées comme « mignonnes » eurent un grand succès, des serviteurs « dociles et adorable ». Notamment certains créés pour être jeune, pour les enfants et adolescent. Un mois après ce vote, le président Neyes décida de démissionner en protestation. Sans effet.

&nbsp;

Un nouveau peuple avait été créé, une nouvelle excuse avait été trouvée.
