---
import: ../../../../crowbook/common.book
title: "Le Prisonnier"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2013/06/02 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/prisonnier.pdf
output.epub: ../../../../../dist/download/prisonnier.epub
download: prisonnier
categories:
  - Nouvelles
---

« *Laissez-moi partir ! » hurlais-je contre la porte désespérément close. « Je peux appeler la police ! J'ai mon téléphone ! Ça ne sert à rien, je ne peux rien vous rapporter !* »

&nbsp;

Aucune réponse. Je donne un coup de pied dans la porte à cause de la colère. Toujours pas de réponse et maintenant j'ai mal. Je m'assois par terre, partagé entre la fureur et la terreur. Que se passe-t-il, qu'est ce que je peux bien foutre ici ?

J'essayai de rassembler mes souvenirs. Je m'étais endormi comme d'habitude, seul à mon appartement. Puis plus rien. Et puis, je m'étais réveillé dans cette salle étrange, au planché terne et abimé, et aux étagères pleines à craquer d'objets en tout genre et aux murs sales et délaissés. Cela ne m'aidait pas beaucoup à comprendre ce qui se passait. La salle sentait le moisi et l'abandon, et la poussière me faisait tousser. Personne n'y était sans doute entré depuis un moment… Si l'on exceptais la personne qui m'y avait amené pendant mon sommeil. Parce que j'étais certain de ne jamais être entré ici. L'endroit ne me disait rien, et je me serais sûrement souvenu d'un lieu aussi bizarre. Et je ne voyais toujours pas ce que je fichais ici. Et j'avais peur de ne jamais pouvoir quitter cet endroit.

&nbsp;

— LAISSEZ-MOI SORTIR !! hurlais-je à pleins poumon. Je, je ferais ce que vous voulez ! Vous savez, je ne suis pas n’importe qui. Mes parents sont puissant, une ancienne famille de l’empire ! Je sais pas si vous connaissez la théorie des métaux, mais ma famille est considéré comme étant faites d’or. Donc on peut régler cela à l’amiable, tandis que si vous me faites quoi que ce soit, les répercutions seront bien plus dure. On a un deal ?

&nbsp;

Aucune réponse. Toujours que le silence.

&nbsp;

— Il y a bien une raison pourquoi je suis ici, hein ? On n'enlève pas les gens comme cela, pour rire. Ce serait une attitude hautement illogique. Et vous n'êtes sûrement pas un tueur en série. En effet, vous auriez pu me tuer bien plus tôt… A moins que vous vouliez vous amuser avant… Mais… ce n’est pas le cas. N’est-ce pas ?

&nbsp;

Je sentais ma peur grimper au fur et à mesure de ce que je disais. Et si c'était bien cela ? Et si j'avais juste été une victime capturée au hasard par un sadique pervers qui ne cherchait qu'une victime à torturer ou tuer – voir les deux à la fois ? Et ça se trouve, en lui disait que j’étais fait d’or, il allait encore plus en vouloir à ma personne… Je me mis à marcher en rond, mes mains agrippant mon propre tee-shirt.

Merde, merde, merde ! Qu'allait-il m'arriver ? Je n'arrivais même pas à réunir mon esprit pour essayer de comprendre ce qui se passait. J’accélérai le pas, de plus en plus stressé. Peut-être que le tueur allait entrer à l'instant. Peut-être que je vivais mes dernières secondes. Je ne veux pas mourir. Du moins, ni maintenant, ni comme ça. Une mort rapide et non douloureuse dans environ une soixantaine d'année ça m'arrangerait. Avoir le temps de réaliser quelque chose digne de moi et de ma famille, au lieu de subir une mort inutile, enfermé dans cette salle. Tout seul.

&nbsp;

— Ah ! résonna une voix. Je crois que tu commence à comprendre ou est le problème. C'est moyen, disons que c'est ni la meilleur réaction, ni la pire. Je te met 11/20, peut mieux faire.

&nbsp;

Je me retournai. Qui a dit ça ? Et depuis quand c'était possible de lire dans les pensées ? *Et qui oses me noter, moi ?* Malgré la situation et ma frousse, je ne put m'empêcher de penser à toute les fois ou je m'étais dit qu'heureusement que la télépathie n'existait pas. Non, c'était impossible, elle n'existait pas, si elle existait, ce serait trop gênant… Et c’était le genre de superstition que pouvait se permettre les familles de basse naissance, qui n’étaient pas écrasés sous le poids des responsabilités. Ce type avait sûrement dit ça juste pour me troubler, ou en regardant mes expressions faciale. Il devait y avoir des caméras de surveillance, des hauts parleurs, et le type jouait à me déstabiliser. C'était donc bien un sadique. Je m'assis sur le sol. Je devais rester concentré, rester concentré…

&nbsp;

— Tu refroidis pour le premier, mais tu es brûlant pour le deuxième ! retentit à nouveau le voix mystérieuse.

&nbsp;

Non, non, non, non, non… C'était impossible, impossible, la télépathie n'existait pas, je le sais, c'est absurde qu'elle existe, elle ne peut pas exister, le surnaturel n'existe pas, tout est rationnel…

&nbsp;

— Je ne saurais pas dire si tu es amusant ou tu es chiant, soupira mon geolier. Amusant parce que tu joue au rationnel et tout, mais plutôt qu'être certain de tes explications rationnelle, tu panique au moindre truc qui pourrait paraître irrationnel, sans même trouver l'explication la plus plausible. Chiant parce que tu es borné.

&nbsp;

Mon cœur bat de plus en plus. Cette fois, il ne pouvait qu'avoir lu dans mes pensées, j'en était certains, c'était impossible de faire de la psychologie aussi bien et de deviner aussi facilement mes hésitation.

&nbsp;

— Mais ça je le savais déjà, que tu étais borné… Sinon : Bonne réponse, mais par le mauvais moyen. 8/20.

&nbsp;

Par le mauvais moyen ? Mon explication se tenait pourtant… Non ! Je ne doit pas l'écouter. Je repris ma marche, essayant à tout prit de chasser de mon esprit ce télépathe. Et je devais trouver un moyen  de sortir, les télépathes pouvaient être un danger. Il ne devait pas rester dans mon psyché, si cela continuait, je deviendrais fou. Et surtout, mon esprit ne devait pas rester une porte ouverte comme cela.

Ouvrir son esprit, son « ame », comme le disent certains, c'était la porte ouverte à toute les faiblesses.

N'importe qui pouvait alors briser simplement la poutre branlante. Et nous détruire, partir en nous laissant à l'état de ruine. Je n'avais pas passez des années à batir un bunker pour qu'un clown psychique pervers et sadique viennent foutre le souk dans mes pensées. Je devais respirer. Il ne s'était pas manifesté face à ce que je disais. C'était un bon point. Peut-être que quand je reprend mes esprits et que je pense de manière claire et rationnelle, il ne peut pas apparaître. D'ailleurs, peut-être n'a-t-il jamais existé, et que c'est juste le stress qui me fait halluciner additivement. Je me sentais reprendre confiance à moi, j'en avait presque oublié que j'étais enfermé sans pour l'instant espoir de sortie.

&nbsp;

— *Peut-être que quand je reprend mes esprits et que je pense de manière claire et rationnelle, il ne peut pas apparaître*, m'imita alors la voix d'une manière particulièrement aiguë et nasillarde.

&nbsp;

Mais quand cela allait-il s'arrêter ! En tout cas, maintenant, là, c'était sûr…

Mais non, j'étais con ! Je ricanai dans mon coin, me sentant stupide. Il y avait une explication à tout cela. Une explication logique. Déjà, j’étais fatigué. Hors, je sais que quand je suis fatigué, j'ai tendance à penser à voix haute, parfois sans m'en rendre compte. Ensuite, peut-être qu'hier j'avais un peu bu, ou une connerie du genre, et que j'avais décidé de faire un somme dans cette salle, et que j'avais tout oublié ! Et cette voix, c'était juste un gamin qui m'entendait, et qui tentait de me faire flipper.

Mais je suis un esprit cartésien, moi. Les fantômes et autre connerie du genre, on pouvait pas m'avoir avec cela.

&nbsp;

— Je crois que cette explication est encore plus tiré par les cheveux que ta situation… Et quand tu flippais quelques secondes auparavant parce que tu pensais qu'on lisait dans tes pensées, c'est un peu pitoyable de te qualifier d'esprit cartésien…

&nbsp;

Haha gamin, tu ne me prend pas au piège ! Mon explication tiens la route, quoi que tu en dises, tu vas donc tranquillement me laisser sortir de cette salle ! Je me dirigeais après ces mots vers la porte. Je me levai avec difficulté, ayant très sûrement passé la nuit sur ce plancher, mon dos et mes articulations me faisaient mal comme à chaque fois que je passait une nuit sur une surface trop solide. En tout cas, douleur ou pas, une chose était sûre. Je n'allais pas rester dans cette endroit lugubre. Et surtout pas dans ce froid, dans ce froid qui me glaçait jusqu'au sang. L'endroit ne devait sûrement pas être chauffé.

Je me dirigeai donc vers la sortit d'un pas rapide, n'ayant pas envie de m'attarder une seconde de plus. La porte de sortie était une grande porte en bois sombre, imposante et presque effrayante tellement je me sentait tout petit par rapport à elle. Mais ce qui me perturba le plus était l'inscription gravée dessus. Inscription qui n'y était pas auparavant :

&nbsp;

« Tu ne vas quand même pas me quitter ? »

&nbsp;

Je ne l'avais sûrement pas remarqué la première fois. C'était la seule explication logique. Bon, je devais trouver un moyen d'enfoncer la porte. Sans grande conviction, j’eus le réflexe de quand même vérifier si la porte était ouverte.

Elle l'était.

Je restais méfiant. Il y avait quelque chose qui clochait. Le texte pouvait bien avoir été écrit par mon « ravisseur ». Peut-être était-ce un piège qui m'attendais de l'autre coté. Ici, au moins, je savais ce qu'il y avait. C'était bizarre, mais j'étais en terre connue. Par contre, derrière… Peut-être que la personne qui m'avait enfermé s'y trouvait ? Peut-être qu'il allait m'attaquer ? Et qu'est-ce qu'il y avait derrière ? Un début d'escalier en colimaçon. Ma curiosité était encore plus attisé, et finalement arriva le moment ou j'ouvris en grand la porte et sortit de la salle.

En descendant l'escalier en colimaçon, dont la traversé me sembla longue de plusieurs heures, je réfléchissait… J'était visiblement enfermé par une sorte de type bizarre qui jouait au télépathe et qui avait le goût des mauvaises histoires d'horreur. Je me demandait s'il allait faire un truc genre sortir des infos que personne ne sait sur moi… Les murs étaient des murs de pierres fissurées, qui semblait proche de l'effondrement.

Après une longue descente, j'atteint la fin de l'escalier, pour arriver dans une nouvelle salle. Cette fois ci, elle était entièrement blanche, immaculée. La saleté avait laissé place à une propreté des plus surprenante. Okay, donc avait j'étais dans le vieux grenier tout sale d'une maison tenue par un maniaque de la propreté ? Mais au final, je compris ce qui me dérangeait. Ce n'était pas la propreté. Cette salle était vide. Il n'y avait aucun objet, aucun meuble. Que du blanc. Au plafond, au mur, et au sol. Et sur la porte à l'autre bout.

&nbsp;

« Bien, tu es arrivé au niveau 2 ! Il est maintenant tant que l'on se rencontre, n'est-ce pas ? »

&nbsp;

Je vis la porte à l'autre bout s'ouvrir, mais sans vraiment pouvoir distinguer ce qu'il y avait comme salle – ou comme extérieur – derrière, et je vis entrer une personne habillée toute en noir, ne laissant rien voir d'elle-même. Cela me semblait trop cliché pour être une mise en scène. Il devait venir de l'extérieur, parce que la température avait chuté de quelque degré lorsqu'il était entré. Mais que me voulait-il, bon sang !

&nbsp;

« Te faire comprendre deux-trois trucs. » chantonna l'être encapuchonné. « Déjà, si tu pouvais comprendre ou tu es, cela m'amuserait beaucoup ! »

&nbsp;

Comment ça, me faire comprendre deux-trois trucs ? J'essayais de cacher le mieux possible ma frousse. Parce que même si la situation me semblait cliché au possible, le simple fait d'être dans cette situation cliché, et de courir un risque qui me semblait imminent.

&nbsp;

— Le risque que tu as, ce n'est pas le risque du type de celui qui va se faire poignarder, ne t'inquiète pas… Non, le risque que tu as, c'est celui de l'homme assiéger, mais qui refuse de l'admettre. Tu es dans la dernière forteresse que tu possède. Ton dernier bastion. Celui ou personne ne peut t'atteindre – à part moi. Tu es dans l'oeuf cosmique, la dernière forteresse que possède l’esprit d’une personne. La source de tout ton monde, la source de tout ton être. Ta conscience. C'est la seule terre ou tu peux-être en sécurité. Pourquoi ? Parce que c'est la seule qui t'appartient ! On n'est jamais mieux que chez soi, home sweet home, comme le disent les expressions, non ? »

&nbsp;

Je regardais. Ce pouvoir de création de monde. L'imagination était mon arme, ma retraite. Mon pouvoir.

Je comprenais tout. Pourquoi il lisait dans mes pensées et tout. J'étais seulement en train d'imaginer un nouveau monde ou je pourrais me retirer. J'étais dans cet œuf cosmique, qui venait d'être réalisé par mon imagination. La retraite de l'artiste. Son atelier ultime, qui est sa propre création. Et derrière cette porte, qu'était-ce ? Ce monde ? Celui d'où je venais ? Je tentais de rationaliser.

Il existait trois types de personnes, au dessus du commun. L'or – les créatifs et les sages, l'argent – les dirigeant et les charismatiques, et le bronze – les forts et les combattants. J’étais une personne d’or, et on m’avait toujours qualifié comme étant un grand créatif, futur penseur et écrivain. Je devais donc avoir la capacité de modéliser quelque chose de véritablement « physique » en moi.

&nbsp;

— C'est la porte de l'enfer. Tu y retrouveras les autres, comme pourrait-on dire si on mélangeais Huis Clos et La Divine Comédie… Cela ne donne pas envie, hein, les autres, la foule… Mais ils sont encore là. D'ailleurs, je suis eux… Leur regard, leur esprit… Je suis le gardien de l'enfer, le regard des autres. Je suis Minos, Juge de l'Enfer. Tu es éternellement soumis à mon regard, mortel. Je suis celui qui t’empêche de protéger de te diriger vers ce monde trop dangereux. Les ors, les sages, voudraient bien être à ta place. Les argents, les soldats, sont colériques et bornés. Les bronzes sont peu enclin à l’intelligence et aux discussions intéressante. Ici au moins, en toi, tu es en bonne compagnie, non ? Avec moi, jusqu’à la fin des temps ! Minos avait fini cette phrase, en me susurrant les derniers mots à l’oreille. J'eus un frisson. Derrière cette capuche, qui ne me montrait rien de celui me regardait, une certitude était en effet présente.

&nbsp;

Il me regardait. Et me jugeait sûrement. Il jugeait ces bégaiement que je pouvais avoir, il jugeais mes maladresses… Il me pensait sûrement être un menteur à chaque fois que je disais des trucs paradoxaux.

Il se souvenait sûrement de chaque connerie que j'avais faite, et m'en voulait peut-être pour cela. Ou alors il ne prenait même pas la peine de me détester, j'étais trop ridicule et pathétique pour cela… M'appréciait-il ? Je ne pouvais le savoir…

Je m'étais trompé dans son rôle initial. Il ne me retenait pas captif. Il était venu troubler ma retraite.

Tout ces doutes quand je n'entendais plus personne parler après que moi j'ai parlé. Toute cette honte quand je n'étais pas capable de me souvenir de ce que je disais. Toutes mes conneries. Tous les instants ou j'avais été ridicule ou ridiculisé. Toutes les fois où les règles de vie de ma classe m’étaient retombée dessus.

La porte. Je pouvais prendre la porte. Oui, je pouvais le fuir, je pouvais partir.

&nbsp;

— Je ne peux en effet pas te retenir… soupira-t-il. Mais n'oublie pas… Je viens du regard des autres, qui sont derrières cette porte. C’est ça, que tu dois fuir. Don’t shoot the messenger, comme ils disent.

&nbsp;

Tant pis, au moins je devais essayer. Je me dirigea vers cette porte de sorti en courant, au cas ou il me tendait un piège. Mais il ne fit rien. En quelque pas, j'étais devant la porte, qui était tout simple, comme une porte de cuisine. Avec un post-it dessus avec un mot.

&nbsp;

« Vous qui sortez, abandonnez toute espérance ».

&nbsp;

Par delà cette porte était la foule. Par delà cette porte était le regard constant des autres. Me devais-je de fuir dans le royaume de l'enfer si son gardien s'invitait chez moi ? J'avais presque la certitude que je ne pouvais pas le virer… Il était trop fort. Je me sentit mal, comme si ma respiration se coupait. Je pensais à la foule. Je les voyais, ces silhouettes informes, qui m'entouraient, qui formaient une véritable prisons. J'entendais des voix. Est-ce que ces gens allaient m’attaquer ? Non, calme toi, calme toi, tout vas bien… J'étouffais, je me noyais, cet océan d'humain était impossible à traverser… Je les voyais, toutes ces personnes, qui était comme moi, mais qui étaient certainement amplises de haine et de jalousie pour une place que je n’avais pas demander…. Ne pas se faire remarquer, ne pas se faire remarquer. J'étais compressé, j'étais écrasé…

Mais je devais fuir.

Je devais quitter ce monstre qui me manipulait. Je ne peux pas rester une seconde de plus. Il a perverti mon œuf cosmique. Ma seule cachette n'en est plus une, seule la vie de fugitif me reste, a présent. Je déglutis, toujours en lutte contre le froid qui s'insinuait en moi. Mais j'ouvris quand même la porte, pour me retrouver dans un chemin forestier, de nuit. Je soupirais de soulagement : Il était vide. Personne.

La seule lumière que j'avais était celle de lanternes accrochées sur les arbres, qui me permettaient de pouvoir avancer. Je fis quelque mètre, avant d'hésiter. Il faisait quand même sacrément noir. Et c'était bien ici le lieu de la foule. Ils pouvaient être partout. Mieux valait peut-être supporter Minos que d'avoir la vrai foule face à soit. Mais à peine eus-je fait un seul pas en arrière qu'un corbeau vint se poser sur une branche devant moi, me dévisageant de ses deux petits yeux brillants. Je continuai d'avancer, décidant de ne pas avoir peur d'un simple corbeau.

&nbsp;

Mais je me figeai au moment où il se mit à me parler.

&nbsp;

— Stupide. Lâche

&nbsp;

Pardon ? Cette journée est déjà assez pourrie, je dois quitter mon propre refuge pour retourner vers les autres, et je me fais maintenant insulter par un piaf ?

&nbsp;

— Stupide, répéta-t-il. Tu es stupide. Lâche. Tu fuis.

&nbsp;

Oh, sale piaf, je fuirais moins si c’était pas rempli de connards ici-bas.

&nbsp;

— Lâche. Et méchant. Tu fais l’hypothèse que tous les autres sont méchant et stupide, mais tu es méchant et et stupide.

&nbsp;

Méchant, moi ? Ce n’est pas moi qui suis allé agressé des gens dans la rue, par jalousie de leur situation. Bon, je savais bien que la situation n’était pas totalement juste, mais quand même !

&nbsp;

— Méchant. Tu les agresses à chaque fois que tu les dis comme le commun. Tu les insultes à chaque fois que tu crois qu’ils méritent leur place en dessous de la tienne.

&nbsp;

Je n’ai jamais dit qu’il la mérité, mais qu’ils étaient jaloux. C’était normal, mais c’était pas bien.

&nbsp;

— Mesquin. Tu te cherche des excuses pour te justifier. T’es tu dis que si tu étais gentil avec eux, que si tu les écoutais et ne les méprisais pas, ça irai mieux ?

&nbsp;

Son regard était particulièrement pénétrant… Était-ce un troisième round ? Était-ce encore ce Minos qui venait se la ramener sous une apparence différente. Il m'avait d'abord fait peur. Puis s'était joué de mot. Et maintenant m'attaquait directement en m'engueulant. Et pourquoi étais-je lâche, en plus ? Oui, je ne fais pas l'hypothèse la plus agréable du « tout le monde est gentil », monsieur le corbeau.

Mais j’ai vu le monde.

Okay, j’ai pas tout vu, mais soyons un peu logique, okay ? Je sais que c’est dur pour un animal qui n’a donc pas l’intelligence de mon espace, mais faisons un peu de calcul.

Quand tu subis une douleur attendu, celle-ci est plus faible que quand elle est attendu. C'est la même chose pour les joies : Une bonne surprise c'est toujours mieux qu'un truc que tu attendais. Donc, si tu fais l'hypothèse la plus joyeuse : Si elle s'avère vrai, tu te prend un truc un tout petit peu agréable, et si elle est fausse, tu te mange un camion de déception. Si tu fais l'hypothèse pessimiste, tu peux amortir la douleur ou recevoir une joie plus grande ! Tu es donc gagnant dans les deux cas. Ce n'est même pas avoir du -1 ou +1 dans un cas et -2 ou +2 dans l'autre, non ! L'optimiste à -2 et +1, et le pessimiste -1 et +2 !

C’est pour ça que les gens intelligents sont toujours pessimistes. Parce que c’est lo-gi-que.

&nbsp;

— Lâche. Tu te réfugie derrière des mathématiques, en mettant des valeurs au hasard. La plupars du temps, il se passe rien. On vit sa vie sans grande surprise. Pendant tout ce temps, l’optimiste à ton +1, ton pessimiste -1. Pareil en attendant quelque chose. Pas logique.

&nbsp;

Mais si, c’est logique, parce que c’est bien plus fort quand se prend la surprise, tu met +1 mais moi je mettrais plus une quantité négligeable.

&nbsp;

— Prétentieux. Tu pense toujours avoir raison. Et lâche. Tu as tellement peur d'être déçu que tu ignores chaque possibilité qu'il y ait le moindre événement cool, juste parce qu'il y a des arbres qui font peur avant…

&nbsp;

Je me retournai, légèrement en colère. Il se prenait pour qui, ce corbeau, pour me faire la morale ? Et se foutre de moi par la même occasion ? Énervé, je lui rétorquai que je ne voyais pas de quoi il parlait, et que j'avais mes raisons d'être comme j'étais, de ne pas prendre de risque, et qu’il ne pouvait pas les comprendre.

Que la vis d'être d'or, ce n'était pas facile. On avait pas la force des bronzes, ou le charisme des argents. Notre rôle était purement consultatifs, on avait pas la chance des autres.

Il se mit à rire, avec un croassement qui m'irrita encore plus.

&nbsp;

— Égocentrique. Tu dis avoir souffert, et que ta situation est injuste. Mais tu rejette la souffrance de tous ceux qui sont en dessous de toi pour grandir la tienne. Le classique « Tu ne sais pas ce que j’ai vécu ».

Je sais ce que tu as vécu. Ta petite vie de personne d’or, de la haute société. Riche. Puissant. Mais d’autre était jaloux. Des disputes. Des claques quand tu n’as pas été aussi intelligent que tu te dis être. Tout les tiens l’ont connu. Oui ça fait mal. Ta « terrible souffrance », c’est celle que tous rencontre un jour. Un gros foirage qui te fais honte. Une rupture difficile. Quelques personnes qui t’emmerde. Et pour cela, ensuite, tu accuses le monde entier d’être contre toi, chaque personne que tu vois peut être un méchant, alors que t’es plutôt du bon côté du fossé de ceux qui sont les puissant et ceux qui sont leurs victimes.

&nbsp;

Il se rapproche de moi. Je comprend son but : Il ne veut que me faire culpabiliser. Mais tu n’es pas le premier, tu sais ?

&nbsp;

— Mais combien de fois tu as ignoré avec un certain dégoût une personne dans le besoin, dont le métal n’était pas « précieux » ? Combien de fois tu as dit que tu n’avais « pas le temps » quand quelqu’un avait besoin de ton aide, avant d’aller glander devant l’ordinateur ?

Combien de fois, tu as vu une personne dans la rue se faire harceler, et tu t’es dit que ce n’était pas tes affaires ?

Je te dis tout ça parce que tu sais que c’est injuste, mais ne veux pas l’admettre parce que tu ne veux pas accepter avoir commis des fautes. C’est toi qui ne veut pas admettre la vérité avec sérénité. Tu sais que tu fais parti de ceux qui sont puissant, mais tu refuses de le regarder en fasse, pour ne pas être "le méchant"

&nbsp;

Il vole autour de moi.

&nbsp;

—La seule source de ton mal, c'est toi qui l'a fait rentrer, tu sais qui elle est, et pourquoi elle est là. Aller, pschitt, ne retourne pas vers ce bâtiment. Le monde qui t’attend par delà, c’est le monde à construire. Tu dois construire un monde avec les autres.

&nbsp;

Je tapai du pied. Il commençait vraiment à me courir sur le haricot. En plus, ce qu'il disait me faisait mal. Je serrais les dents avant de lui répondre que je savais ce que je faisais, que je n'étais plus un enfant. Et puis qu'en plus je ne faisais pas vraiment chier les autres quand j'étais tranquillement dans ma tête, et que je gardais ce genre de truc pour moi et puis après c'était bon, basta et je pouvais rire et m'amuser. Je lui déclarai aussi qu'il n'était pas dans ma tête, et qu'un corbeau qui parlait était déjà assez bizarre comme ça.

&nbsp;

— Si, répondit-il. Je suis dans ta tête.

&nbsp;

Je tenta de déloger le corbeau d'une pierre mais celui-ci l'évita sans problème. Écoute, le piaf, j'ai eut mes histoires, j'ai le droit de prendre un peu de repos et de ne plus penser à ce genre de chose, non ? Il me rétorqua que j'y pensais tout le temps, avant de me faire la remarque qu'il allait bien falloir réfléchir un peu. Je bougonnais. Je n'avais pas envie de m'arrêter, je préférais continuer en avant, puis voir au fur et à mesure de ce qui se passait. J'eus le droit encore au reproche d'être un gamin.

Je ressentais une envie de piaf rôti à la broche…

&nbsp;

—Tu as le choix, fit l'oiseau, ouvrant grand ses ailes tout en ignorant mes menaces. Tu peux retourner là bas, ou avancer pour pénétrer plus profondément dans la forêt pour rejoindre le monde, pour sortir de la monade.

&nbsp;

Je me retournai et décidai de reprendre la route vers le bâtiment lugubre. C'était sans doute là-bas que je pourrais retourner en arrière pour que tout soit comme avant. J'entendis derrière moi que le piaf semblait presque paniquer. Tout me semblait logique à présent. Ce piaf avait juste envie de me faire avancer plus loin dans cette foret, et pour cela il m'avait provoqué en essayant de jouer sur une fierté qu'il s'imaginait en moi. Il voulait m'envoyer dans la foule. Et qui avait interet à ce que je sois dans la foule ?

Minos, tout simplement.

Il devait avoir un pouvoir réduit quand j'étais dans l'oeuf. D'ailleurs, c'était que au moment ou j'étais en train de sortir qu'il a put me faire subir une hallucination. J'entrai dans le bâtiment, traversant la salle vide – ou le bourreau n'étais plus.

Je reconstituait tout en marchant le plan de Minos. Il m'avait fait sortir de la salle principale de l'oeuf – le grenier, qui représentait le bazar que c'était dans mes idées, avec des trucs partout… La poussière, c'était peut être pour faire grenier abandonné, un genre d'endroit que j'avais toujours révé de visité – dans l'entrée pour commencer à avoir un pouvoir plus fort sur moi, dans le but de me faire fuir vers la foule. Dans cette entrée, il avait été jusqu'au traumatisme, pour que je le fuis en prenant mes jambes à mon coups. Ensuite, voyant que j’hésitai, il avait envoyé le corbeau… Mais avait utilisé la mauvaise technique. Je retournerais dans l’œuf cosmique, à l'abri. Au cœur de ma monade. Je continuerais de vivre ma vie comme je l'entend, essayant d'avoir le moins d'histoire possible. L'escalier en colimaçon ne fut pas long à remonter – à ma grande surprise – et je me retrouvai enfin dans la salle ou tout avant commencé.

Mais elle était vide. Surpris, je me mis à regarder dans tout les sens. Plus aucun meuble ni rien. Je regardais au plafond, ou était suspendu un grand chandelier magnifique, avec d'étranges flammes violettes, qui projetaient une lumière surnaturelle dans toute la salle.

Et il était là.

&nbsp;

— Je t'attendais.

&nbsp;

Il n'y avait quelque chose que je n'avais compris ? Pourtant, tout tenait… J'essayais de fuir à nouveau, vers la porte. Elle était fermée. Je me retournai.

Il était juste devant moi.

&nbsp;

— Alors, tu as compris, maintenant ?

&nbsp;

Je tombai. Que ce passait-il, que ce passait-il ? Est-ce que je m'étais trompé ? Est-ce qu'en fait le corbeau avait eut raison ? Mais c'était forcément un piège ! Il me provoquait pour que j'ai envie de continuer ma route…

&nbsp;

— C'était bien ce qu'il faisait. Il voulait te renvoyer vers la foule. Moi, je ne voulais pas, donc j'avoue que j'ai été très content de ton choix. Même s'il m'a surpris. Je pensais que tu aurais compris que certains de ces conseils était « bons », dans sa vision des choses. Mais cela veut dire qu'au fond tu penses comme moi, ça me rassure ! Tu ne restes pas sous le danger… Les autres, surtout ceux de bronzes, ceux qui t’on attaqué. C’est eux le vrai danger que tu subis. Tous jaloux, tous à vouloir avoir ta place. Alors, tu la leur laisse, qu’ils s’entre-tuent tandis que toi ici, tu pourra régner sur ta monade. Être tout puissant et créer un monde où toi seul sera le roi. Toi et moi. Tous deux seuls.

&nbsp;

Je vis une porte à l'opposée de la porte aux gravures. Je m'y dirigea et sortit immédiatement dans la salle. Mais tout ce qui m'attendais était un précipice ou je sombrai. Le noir sembla tout absorber, je ne voyais plus rien. L'obscurité et le froid était partout, en moi, en mon esprit. J'avais du mal à penser, je ne me sentait même plus chuter dans cet abyme infini.

&nbsp;

— Mais tu le provoque toi même ! Mais ne t'inquiètes pas, tu ne sera plus jamais seul, je serais là, avec toi, pour toujours ! Je te parlerais, te susurrerais, ne quitterais jamais ton esprit… Nous sommes réunis maintenant, tu ne peux plus partir de l’œuf cosmique, de la monade… Tu as fermé la porte. Le corbeau, cet autre partie de ton inconscient, ce que quelques ineptes appelle ton aspect « raisonnable » ne faisait qu'insulter ta rationalité, la seule vrai raison. Tu es enfin chez toi… Je suis le seul autrui dont tu as besoin, mon très cher, et je suis là pour toi.

Je suis là pour toi.
