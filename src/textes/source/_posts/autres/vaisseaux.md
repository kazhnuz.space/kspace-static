---
import: ../../../../crowbook/common.book
title: "Les vaisseaux"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/26 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/vaisseaux.pdf
output.epub: ../../../../../dist/download/vaisseaux.epub
download: vaisseaux
categories:
  - Autres
---
Thalassa regardait fièrement la mer des nuages être fendue par la coque de son navire. Le fier vaisseau, ouvrage de centaine d’ouvrier de son peuple, était ce qui leur permettait d’aller plus loin à travers l’immensité du vide. Il y a des siècles, ils avaient été abandonnés ici par le peuple qui les employait comme colons.

La planète, avait été déclaré comme non-viable par le sénat solaire, et ce après trente ans de colonisation, et malgré les dizaines de milliers d’habitants qui y vivaient déjà.

&nbsp;

Et les quelques humains et zoomorphes employés par le système solaire avaient été abandonné ici, devant se débrouiller pour survivre sur les îles flottantes qui parsemait la couche haute de l’athmosphère. La vie était dure. Il fallait réussir à vivre dans une atmosphère empoisonnée, et miner l’oxygène dans des poches d’air respirable, située à la surface de la planète, là où la pression était trop forte pour y vivre. C’était une entreprise périlleuse, puisqu’il fallait traverser une mer de nuages plongés dans un orage permanent, et réussir à surveiller les tubes et lutter contre les éléments et la piraterie.

Avoir des réserves d’air personnelles étaient un luxe que peu pouvaient se permettre, la plus grande partie du peuple devant vivre dans les espaces communs en faisant des travaux pour la communautés, n’ayant des bouteilles d’oxygène que quand on les envoyait travailler dehors.

Sans technologie du système solaire, sans l’accès aux savoirs de la planète-mère, chacune des colonies étaient coincé sur leur île, devant lutter pour survivre.

&nbsp;

Mais ces navires flottant étaient l’espoir d’un monde meilleurs. Mélange entre les navires et les ballons dirigeable, ces vaisseaux étaient capable de relier un continent à un autre. Thalassa était en train de réaliser la première liaison entre les deux plus grande capitales. Cela faisait plus de 600 ans que tout contact était impossible entre les deux. Elle avait hâte de rejoindre sa destination. Ce serait le début d’une nouvelle ère, la fin de l’âge sombre. Thalassa imaginait les possibilités : en unissant leur forces, ils pourraient automatiser les stations de récupérations de l’air pur, et partager leurs connaissances pour construire des moyens de le purifier. Toutes la planète, unie pour améliorer leur condition de vie.

Mais surtout, c’était une vengeance symbolique. La preuve qu’ils pouvaient se débrouiller sans le Système Solaire. Ils les avaient considéré comme une ressource qu’ils avaient le droit d’abandonner quand celle-ci devenait trop coûteuse ? Maintenant les habitants de la planète prouvaient qu’il n’avaient pas besoin d’eux. Ils atteindraient d’eux même à nouveaux les étoiles, et prouveraient à l’univers entier les agissements de la Terre. Au fond d’eux, ils espéraient même que l’empire ancien des humains s’était effondré.

Tant pouvait s’être passé en 600 ans !

&nbsp;

Thalassa regardait fièrement la mer des nuages être fendue par la coque de son navire. Toute sa vie, elle avait travaillé sur ce projet. Toute sa vie, elle avait attendu le jour où les habitants de sa planète pourraient à nouveaux être réunis. Toute sa vie, elle l’avait destinée à permettre le début d’une ère nouvel. Ce premier voyage serait le début d’une grande série.

D’ici quelques années, ce serait une liaison régulière qui serait faite entre les deux villes.

D’ici quelques décennies, les cieux seraient remplis de ces bateaux volants.

&nbsp;

Thalassa était contente. Ils avaient pu se relever de leur abandon, même si cela leur avait mis des siècles, et étaient en train de construire tous ensemble une ère nouvelle. Une époque d’infinies possibilités s’ouvraient à eux tous.
