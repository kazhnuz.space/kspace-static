---
import: ../../../../crowbook/common.book
title: "Le monument"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/03 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/monument.pdf
output.epub: ../../../../../dist/download/monument.epub
download: monument
categories:
  - Autres
---
Celia avançait avec difficulté dans les vallées. Cela faisait déjà plusieurs jours qu’elle était au fond de cette vallée, se dirigeant vers le monastère qu’on lui avait indiqué. « Au bout de la vallée, Après la statue ». L’ancienne aventurière avançait parmi les roches et les herbes hautes, faisant attention aux serpents et autres dangers de la montagne. L’ascension était pénible et difficile.

Arrivée au bout de la vallée, elle comprit qu’elle n’était pas très loin de son objectif. Une immense statue de roc brut, qui montait jusqu’au flanc des deux montagnes qui entourait la voyageuse, se dressait devant elle. Ce qu’elle représentait était pourtant simple : Juste une femme, qui portant un grand châle. Une figure presque fantomatique, peut-être une déesse ancienne ou une incarnation de la nature.

Mais même si cette représentation ne surprenait en rien, cette statue la surplombait comme jamais une statue ne l’avait faite.

&nbsp;

Quel âge avait cette colossale femme de roche ? Celia n’en avait aucune idée, mais les plantes et l’érosion semblait indiqué un age très ancien. Célia se prit à se demander comment cette statue avait pu voir le jour. Était-ce creusé dans le roc, avaient-ils amené ici la matière première ou avait-elle était creusé à partir de la vallée ? Elle n’avait ni les moyens, ni les connaissances de savoir à coup sûr les réponses. Mais cela n’empêchait pas les questions de venir et de se bousculer dans sa tête. Était-ce ne serait-ce que possible pour un peuple ancien de construire une telle chose ? Sur combien de temps ?

Toute la journée, Célia fit des hypothèses sur cette femme, cherchant à percer le mystère de cette statue.

&nbsp;

Était-ce le travail de tout un peuple, rendant hommage à une cheffe qui les avaient protégés d’un clan ennemi ou d’une menace quelconque ? Sur des générations, ils avaient creusé la montagne ou un mégalithe pour créer cette statue, mué par leur envie de rendre un hommage postume. Était-ce la représentation d’oracles, de femmes mystiques en communion avec les esprits qui avaient livré à leur peuple des secrets cachés et des prédictions qui les avaient sauvés ? Un monument à ces femmes désormais anonymes, mais qui resteraient vivante grâce à cette création de roche brute. Une manière pour elle de ne pas tomber dans l’oubli.

Était-elle plus ancienne ou plus récente que le monastère ? Avait-elle veillé sur les moines, telle une mère de toutes celles et ceux qui priaient ici. Était-celle dont ils imploraient la protection, quand les sombres nuages de la fatalité semblaient s’abattre sur eux ? Était-ce à elle qu’ils attribuaient les guérisons miraculeuses, les coups de chances et les instants de bonheurs ? Était-ce elle que de jeunes couples en besoin d’enfant venaient voir, telle les déesses de la fécondité de jadis ?

&nbsp;

La nuit tombait. Celia devait quitter ce colosse, cette statue, cette déesse ancienne, cette incarnation de la nature, cette cheffe, cet oracle, cette mère du monastère, cette divinité de la fécondité. Elle se sépara d’elle comme d’une vieille amie, se promettant d’y retourner pour la voir le plus régulièrement possible.

Elle n’était pas plus avancée qu’avant ses réflexions sur la vérité sur cette statue. Mais peu importait. Cette statue était désormais pour elle l’entrée vers un nouveau pas de sa vie, et sa compagne d’un moment de rêverie et d’imagination. La véritable histoire importait peu face à ce que lui avait fait vivre la simple vision et les questions que cette statue géante.
