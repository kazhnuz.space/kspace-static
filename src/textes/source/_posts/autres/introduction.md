---
import: ../../../../crowbook/common.book
title: "Introduction"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/03/26 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/introduction.pdf
output.epub: ../../../../../dist/download/introduction.epub
download: introduction
categories:
  - Autres
---
Devant toi, une infinité de lignes, qui fondent à travers l’espace, vers un horizon obscur. Des lignes qui s’entrecroisent, se séparent. Chaque ligne est un univers, et tu sais qu’à l’intérieur de chacune de ces lignes se trouve une infinité de fils de vie, chacun commençant et se terminant en un point d’une des lignes de monde.

Chacun se divisant à chaque ramification d’univers. Une même personne, dispersées sur une infinité de temporalités et de possibilités.

&nbsp;

Devant toi, des mondes exactement comme le tien, qui semblent étrangement identique à celui d’où tu viens. Quelques petits détails qui diffèrent… où peut-être pas ? Un monde qui pourrait être le tiens, mais qui ne l’est pourtant pas. Cette personne, existait-elle ? Cette action, est-ce qu’il s’est véritablement déroulée comme cela ? Comment est-ce qu’à bien pu se passer cet événement du passé dont on ne sait pas grand-chose ?

Chaque fragment d’action qui diffère suffit à créer un univers qui est différent de celui d’où tu viens.

&nbsp;

Devant toi, une infinité de futur possible, de présent possible et même de passé possible. Une infinité d’uchronie. Où un événement aurait pu faire sombrer le monde vers quelque chose de différent. Vers un autre monde. Et si l’Empire Romain avait pu trouver une structure stable lui permettant de continuer à exister ? Et si Hitler était mort dans sa première tentative ratée de Putsch ? Et si les grandes révolutions ne s’étaient jamais passée ? Et si l’URSS n’était pas tombée ? Et si Christophe Colomb avait coulé ?

Et si…

&nbsp;

Devant toi, une infinité de futur possible, par rapport à ton présent. De quoi sera fait demain ? Quels sont les facteurs, dans l’infinité d’événement que vit le monde aujourd’hui qui influeront de ce que sera le monde dans lequel tu continueras ta vie, dans lequel les enfants qui naissent aujourd’hui vivront ? Est-ce que tout est joué d’avance, ou est-ce que la contingence et ses accidents, dans leur infini ironie, propulseront ton univers vers un futur que nul n’aurait pu deviner ?

Est-ce que lorsque tout semblera aller pour le mieux, un coup du sort, un crime, un coup d’état, l’action terrible de ceux qui veulent imposer leur puissance feront naître un sombre destin ? Est-ce que lorsque tout semblera aller pour le pire, un coup de chance, une découverte, des idées, l’espoir de chacune des destinées qui constitue ce monde feront naître un futur plus radieux ?

&nbsp;

Devant toi, une infinité de mondes aux règles toutes différentes. Des mondes qui ne fonctionnent pas selon les mêmes lois que le notre. Univers qui pourraient sembler imaginaire, mais qu’un pacte te fait accepter quand tu les lis. Magie, mystique, divinités… tu acceptes tout cela, combien même ils ne te semblent pas être présent dans ton monde, à condition que le monde reste cohérent. Tu suspends ton incrédulité et tu acceptes l’étrange implicite que contient cette histoire : quelque part, au milieu des infinies possibilités du multivers, pourrait se cacher le monde qui t’es décrit. Un fin fond d’un des mondes possibles, se trouverait la fantasy, les créatures mythiques et les orques.

Ces épopées épiques seraient celles de mondes qui existent, quelque part à travers cette infinité des fils des Moires des mondes.

&nbsp;

Écrivains et lecteurs, voici la mer dont vous êtes les navigateurs, voici les cieux que vous traversez avec vos aéronefs de verbes. Voici les terres que vous explorez, terres d’hypothèses où chaque possibilité peut exister, où vous pouvez découvrir chacune des ramifications des questions que vous vous posez, qui s’étendent pour former des lieux, des intrigues et des êtres.

Entendez ce discours, celui d’un de ces nombreux passeurs. Mais vous en êtes aussi un, à chaque fois que vous vous imaginez comment quelque chose a ou aurait pu se passer, à chaque fois que vous vous questionnez, à chaque fois que vous vous poser la simple question « et si ? », à chaque fois que vous imaginez comment le monde pourrait être. Toute histoire est avant tout une question. Une question qui fait naître un univers. Qu’il te paraisse possible où non, il existe quelque part, dans cette infinité de probabilité. À chacun de ces instants, tu es navigateur, tu es explorateur.

Soyez tous bienvenus dans le véritable multivers, celui de la pensée et des histoires. Maintenant, c’est à vous d’écrire la suite de cette histoire.

&nbsp;

De ces histoires.

&nbsp;

De vos histoires.
