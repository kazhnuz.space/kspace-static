---
import: ../../../../crowbook/common.book
title: "Crooked"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/08 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/crooked.pdf
output.epub: ../../../../../dist/download/crooked.epub
download: crooked
categories:
  - Autres
---

La ville de Rectus se réveillait ce matin avec colère. Dans la nuit, une atteinte aux symboles de la ville avait été fait. Un vandalisme honteux, une attaque qui n’aurait jamais dû être. Tous les poteaux, mats de panneaux et de lampadaires avaient été tordu et noué. Les dégâts semblaient s’étendre sur toute la métropole. Plus un seul cylindre de métal n’était droit. Même le grand mat, symbole de la droiture de la ville, avait été touché par le mal mystérieux.

Ce monument innovant vendu à un prix tout à fait concurrentiel par une entreprise locale de lampadaire et de poteaux téléphoniques dirigée par un ami du maire de l’époque, avec tous les lampadaires et poteaux de la ville.

Avant l’arrivée de ce mat, la ville n’avait jamais eu de symbole ou de monument. Cela coûtait trop cher. Non pas que la ville manquait spécialement d’argent, mais elle avait de bien meilleures utilisations de l'argent. Mais lorsque la proposition avait été faite, il était vendu avec le reste, donc pourquoi se priver ?

Depuis, le phallique monument était devenu un incontournable de la vie. Il était la fierté des habitants, malgré les mesquines moqueries des métèques à la métropole. A Noël, ils le décoraient même, avec des guirlandes.

Si on exceptait le manque de piquants, de bois, d’odeur de pin et de vert, il pourrait presque passer pour un vrai sapin.

&nbsp;

Cependant, cette attaque avait fait atteinte à cette pensée. Le maire déclara que c’était la droiture d’esprit même de la ville qui avait été nouée multiple fois comme ces mats pourtant forgés dans de l’acier. Que c’était la pensée rectiligne dont le maire était le garant – lui qui était maire après son père, son tonton rigolo, son grand-père, son arrière grand-père, et bien d’autres générations avant lui – qui avait été bistournée. La ville avait toujours fait pareil, pensé pareil, poussé sur une route droite depuis sa fondation. C’était le sentier vers le futur qui avait été rendu sinueux avec ces quelques tonnes de métal.

Les psychanalyste les plus renommés dirent que c’était même une émasculation symbolique de la ville.

&nbsp;

L’occultiste du coin déclara que c’était une onde éléctro-magnétique, lié à une espèce extra-terrestre, qui avait tout tordu dans le but d’envoyer un message : que la course vers les étoiles étaient interdites à l’humanité. En nouant tout ce qui montait vers le ciel, ils avaient signifié que c’était le voyage spatial qui serait sinueux, et qu’ils feraient tout pour que les hommes n’aient pas une ascension rectiligne vers l’espace.

D’autres théories furent avancées : conspiration secrète, problème atmosphérique, ou « les jeunes ». Mais l’enquête sur ces énigmatiques torsions, sur ce noueux attentat, n’avançait pas. Les profils se succédaient, et nul ne semblait vouloir ni pouvoir effectuer de pareils actes. Des battut furent effectué.

Pourtant, le coupable était simple à trouver : il suffisait que ce soit une personne ayant la capacité de tordre des centaines de barre d’acier de plusieurs dizaines de centimètre de diamètre en une seule nuit. Avec un tel profil, trouver le coupable ne pouvait qu’être simple ! Mais ce fut en vain.

&nbsp;

Cependant, à l’insu de tous, reposait dans les archives d’une entreprise locale de lampadaire et de poteaux téléphoniques des documents sur la production de mars 1965.

Elle indiquait un défaut de fabrication sur toute la production, et un débat sur comment réussir à écouler ce stock.
