---
import: ../../../../crowbook/common.book
title: "Un crissement"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/11 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/un-crissement.pdf
output.epub: ../../../../../dist/download/un-crissement.epub
download: un-crissement
categories:
  - Autres
---
Une journée ordinaire. Un ciel bleu. Un petit vent frais. Le temps idéal pour un jogging. Un peu de musiques, des foulées.

Un crissement de pneu.

Un choc.

&nbsp;

Angus était dans le brouillard. Il ne savait pas ni où il était, ni ce qu’il y faisait. Le monde autour de lui semblait se limiter à la douleur, qui l’empêchait de penser clairement, et à des cris et voix autour de lui. Mais que disaient-elles ? Rien de compréhensible, semblerait-il. Il essayait de se concentrer. Il essayait de comprendre. Mais il se sentait glisser, les ténèbres le happaient. Il ne comprenait pas.

&nbsp;

Angus se réveilla dans un lit. La salle était entièrement aseptisée, des machines autour de lui faisaient des « bips » réguliers. Pas de doute, il était à l’hosto. Il regarda son état. Il n’arrivait pas à se lever, et il voyait des bandages partout. Il essaya de se rappeler sa journée. Il était la veille d’une course. Pour se détendre, il avait décidé de faire un petit jogging, avec son baladeur. Il se souvint du crissement et du choc. Il jura intérieurement. Il s’était fait renversé par une voiture. Il commença à prendre peur. À quel point les dommages à son corps étaient-ils graves ? Ce crissement, c’était un accident, c’était tout ce qu’il en savait. Tout le reste était inconnu. Ne serait-ce que ses chances de se remettre.

Il maudissait cette voiture, il maudissait tous les conducteurs. Ils n’étaient là que pour mettre en danger les piétons ?!

&nbsp;

Au bout de quelques minutes, une infirmière arriva, lui disant qu’elle avait téléphoné à ses parents, et qu’ils seraient là d’ici une heure. Elle lui apprit les circonstances de l’accident, et fini par lui donner son diagnostique. Angus retint sa respiration, terrifié. Le verdict tomba comme un couperet : même après rééducation, il ne pourrait plus jamais totalement marcher correctement, dans le meilleurs des cas.  

Elle lui déclara également que son pronostic vital avait été engagé. Il s’en était sorti, et ses jours n’étaient plus en danger. Il avait eu de la chance sur ce point.

&nbsp;

Angus se sentit indigné par une telle expression. Son monde s’effondrait, et il avait de la chance ?!  

Le jeune homme avait travaillé pour devenir sportif. C’était sa passion, et son rêve était d’atteindre un haut niveau. Il avait toujours aimé le sport, c’était le seul domaine dans lequel il avait jamais eut l’impression d’être bon. Ce crissement, ce n’était pas que sa carcasse qu’il avait renversée, c’était aussi ses rêves et objectifs. Il n’avait plus de but, et tout son avenir avait été balayé d’un coup, comme un château de carte.  

Pour atteindre son but, il avait enduré le régime spartiate de son entraîneur, il avait accepté de fermer les yeux sur les piqûres de « vitamines » qu’il devait prendre. Il s’était dit que de toute façon il n’avait pas le choix, et que tout le monde le faisait. Mais cela n’empêchait pas la culpabilité d’exister. Ce crissement, c’était le dernier son qu’il avait entendu à l’époque ou ces sacrifices avaient servi à quelque chose. Désormais, il s’était privé et s’était mis la santé en danger pour rien.

&nbsp;

Le temps passait, uniquement rythmé par le bruit des machines. Il tenta de se retourner, mais il était trop bien maintenu. Cependant, en tournant sa tête, il vit des fleurs, et un mot. « Désolé ». Sans même avoir besoin de demander, Angus pu deviner de qui s’était. Le conducteur.  

Il comprit. Un conducteur sur la route, en plein dans son travail habituel, voit débouler un jeune homme qui écoute de la musique et ne fait pas assez attention. Il tente de freiner. Mais rien à faire, il le percute. Ce crissement de pneu, c’était également la tentative d’un conducteur d’éviter toute cette tragédie. Le dernier réflexe possible. Angus se demanda s’il s’était senti coupable. S’il avait regretté ne pas avoir été plus rapide.

&nbsp;

Angus sentit sa colère se retourner envers lui-même. Pourquoi n’avait-il pas fait plus attention ? Pourquoi avait-il fait un jogging ! Il laissa tomber sa tête, la seule partie de son corps qui pouvait encore bouger, sur son oreiller, et regarda le plafond. Il ne voyait plus son avenir devant lui, et ne savait pas ce qu’il pourrait faire maintenant que courir lui était impossible.

Peut-être qu’avec le temps, de nouveaux chemins s’ouvriraient. Mais pour l’instant, il était trop tôt pour voir la lumière au bout du tunnel.
