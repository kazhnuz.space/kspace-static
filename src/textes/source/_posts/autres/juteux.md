---
import: ../../../../crowbook/common.book
title: "Juteux"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/03 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/juteux.pdf
output.epub: ../../../../../dist/download/juteux.epub
download: juteux
categories:
  - Autres
---
Le soleil commençait à peine à se lever, et avec ce nouveau jour débutait également une nouvelle quête. Notre héroïne se dirigeait vers le plus grand des trésors avec une discrétion toute aussi grande. Elle désirait un Saint-Grall : les fruits les plus juteux, dont le nectar était tel l’ambroisie des dieux anciens.

Camille commençait sa quête quotidienne : Faire son petit déjeuner.

&nbsp;

Cependant, il lui fallait être des plus prudents. Les grands maîtres du donjon dormaient encore. Là était tout le sel. Elle devait traverser les grandes crevasses du couloir, sautant sur les dalles qui tenaient en équilibre au-dessus d’un précipice sans fond, sans faire le moindre bruit. Mais elle n’avait pas peur : elle se savait capable d’un tel exploit, capable de réussir une telle quête, capable d’accomplir sa destinée.

De bond leste en bond leste, la petite fille devenue grande héroïne effectua sa périlleuse manœuvre, dirigée par l’envie d’avoir accès à son juteux trésor.

Ce furent de véritable kilomètre de corridor obscur, traversant le château.

&nbsp;

Cependant, ce ne fut pas sa plus grande difficulté. En effet, devant la pièce du trésor se trouvait un énorme canidé, un chien-loup à la mâchoire terrible, au cri rugissant, et à la terrible habitude de piquer les mouchoirs ou chaussons : Le teckel de la famille. Heureusement, pour l’instant, la créature démoniaque était encore dans les bras de Morphée. L’aventurière commença à tenter de passer à côté, mais la créature commençait à ouvrir les yeux et bailler.

Diable, que faire face à un tel coup du sort, une telle attaque ironique d’un destin toujours prompt à se moquer des pauvres hères qui tentaient d’échapper à sa cruelle griffe ?

Mais une idée vint subitement désarmer la destinée. L’héroïne s’élança vers la porte-fenêtre et l’ouvrit brutalement. Le canin, voyant ce mouvement brusque, eut immédiatement l’idée de fondre vers le jardin, se retrouvant alors emprisonné à l’extérieur, comme une andouille. La voie était libre.

Le chien alla faire ses besoins, la première chose importante à faire chaque matin, puis revint atteindre avec un air un peu idiot devant la porte-fenêtre.

&nbsp;

Mais une dernière épreuve l’attendait. Les fruits étaient en haut de l’armoire, des kilomètres au-dessus du sol. Alors commença la terrible ascension des meubles, pour accéder à la corbeille de fruits. Chaque prise n’était pas aussi stable que le voudrait la petite aventurière, mais pour la gloire et pour les fruits, pour leur sucre et leur pulpe juteuse. Elle grimpa difficile sur la table, et tendit la main, s’approchant de son Graal. Elle attrapa la corbeille, mais manqua de perdre l’équilibre.

Elle élança d’un coup son bras pour se rattraper à l’armoire, tenant bien contre elle la corbeille avec les précieux fruits. Elle descendit ensuite avec prudence, s’asseyant sur le plan de travail avant de bondir vers le sol.

&nbsp;

L’héroïne, se dirigea avec son trophée d’un pas fier vers la table de la salle à manger. Camille aimait énormément avoir des fruits pour son petit-déjeuner, et encore plus jouer.

Sa quête était accomplie, mais moult autres l’attendaient encore dans sa vie.
