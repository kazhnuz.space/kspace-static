---
import: ../../../../crowbook/common.book
title: "Camps"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/23 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/camps.pdf
output.epub: ../../../../../dist/download/camps.epub
download: camps
categories:
  - Autres
---

_« Et toi, dans quel camp tu es !? »_

&nbsp;

Nicolas n’aurait jamais pensé que son retour au monde, sa renaissance se serait possiblement le théâtre de la fin d’un monde.

&nbsp;

Hier même, une simple phrase lui avait semblé être la plus belle du monde : « tout est en règle, vous pouvez sortir ». Il avait écouté avec calme et attention toutes les conditions de son droit de sortie : Pas trop de sel, pas trop de sucre, surtout pas d’alcool et de tabac, et pas d’excitation.

Il avait accepté chaque injonction, et même l’idée de retourner au lycée lui avait parue superbe, malgré l’épée de Damoclès qu’était le rattrapage de leçon. Il n’avait eut qu’une hâte : quitter ce lieu. Il avait souhaité retrouver le monde, son monde, qui n’aurait jamais dû se réduire à une pièce et au bruit régulier de machine.

&nbsp;

_« Et toi, dans quel camp tu es !? »_

Nicolas avait toujours été dans le même groupe d’ami depuis son enfance. Il s’y sentait bien, et c’était un peu son univers. Il n’était ni très connecté, ni très sociable, et se sentait donc à l’aise d’avoir ce petit groupe qu’il pouvait retrouver. De ce fait, il avait été impatient de les retrouver, n’ayant eut des nouvelles que de ceux qui lui avaient envoyé des textos pour lui tenir compagnie. De longues discussions sans queue ni tête, dont il savait qu’une partie des messages avaient été envoyés au nez et à la barbe d’un professeur pas assez attentif.

Il avait attendu avec impatience la première journée de cours, la première récréation.

&nbsp;

_« Et toi, dans quel camp tu es !? »_

&nbsp;

Dès le début de la journée, il avait commencé à comprendre que quelque chose n’allait pas. Les félicitations étaient sincères et chaleureuses, les blagues sur sa victoire héroïque pleine de virilité sur les terribles clans infectieux avaient fusée. Mais à côté de ses rires, il avait sentit la gêne et la tension. Ses amis ne discutaient pas entre eux, juste avec lui.

Il avait demandé si tout allait bien. La guerre fut déclarée.

D’abord, juste quelques remarques sarcastiques. Des réponses acides. Visiblement, une trahison de confiance. Et finalement, virent les mots et les cris. Les insultes.

&nbsp;

_« Et toi, dans quel camp tu es !? »_

&nbsp;

Ces mots étaient à la fois une attaque, une menace et une insulte. Une attaque envers lui, qui avait juste écouté les récits contradictoires sur l’effondrement de son groupe d’ami. En lui demandant de prendre position, on le plongeait sans ménagement dans cette situation. Une menace, parce qu’il savait très bien qu’ils ne lui demandaient pas avec qui il serait ami – mais avec qui il allait être brouillé. Et une insulte, parce qu’il lui demandait de designer un coupable et un innocent sans rien savoir de la situation.

&nbsp;

_« Et toi, dans quel camp tu es !? »_

&nbsp;

Cette phrase marquait la rupture, la fin de son monde. Le groupe d’ami n’existait plus, il n’y avait que deux clans ennemis, et il ne pouvait se résoudre à en trahir l’un pour rejoindre l’autre. Il ne savait pas. Et il savait que dans ce genre de conflit, ne pas choisir n’allait pas lui permettre de garder de bon contact avec les deux groupes.

Il avait le choix entre décider arbitrairement de qui il voulait perdre, ou perdre tout le monde.
