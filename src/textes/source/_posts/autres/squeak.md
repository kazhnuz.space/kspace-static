---
import: ../../../../crowbook/common.book
title: "Squeak"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/28 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/squeak.pdf
output.epub: ../../../../../dist/download/squeak.epub
download: squeak
categories:
  - Autres
---
Des rats géants.

&nbsp;

Des foutus rats géants.

&nbsp;

Les soldats regardaient avec un air des plus médusé la menace peu commune, mais néammoins dangereuse, qui se trouvait devant eux. Ils avaient juste eut des des informations comme quoi des bestioles énormes s’attaquaient à la ville. Ils s’étaient attendu à des fauves échappés du zoo suite à la négligence de quelque gardien un peu fatigué, mais s’était rapidement rendu compte que ce n’était pas ça. Et face à eux, c’était des rats tout à fait ordinaire, si ce n’étiat le fait qu’ils faisaient trois mètres au garrot.

La menace pourquoi ils avaient été appelé était des foutus rats géant. Et évidemment, la transformation les avait rendu agressifs et les faisait attaquer tout le monde.

Bref, une sacré journée de merde en perspective.

&nbsp;

En effet, les créatures se promenaient dans la ville, et plutôt de que suivre tranquillement les chemins de balade, avaient décider que s’attaquer au passant serait une activité parfaite pour des rats mutants. Il y avait déjà un grand nombre de victime, et c’était la panique. Sans compter le plus important : les dégâts matériels.

Quelques morts ça allait, mais des frais de réparations, c’était vraiment terrible. En tout cas, une chose était certaines : ces bestioles avaient été rangé dans la catégories des « menaces ».

&nbsp;

Ils se demandaient ce qui avaient pu provoquer une telle chose. Était-ce un cours d’un laboratoire du coin ? A leurs souvenirs, il y en avait quelques uns dans le centre ville, qui étudiait un peu tout et n’importe quoi. C’était bien le genre de truc qui pouvait arriver. Tout d’abord, il fallait une expérimentation sur les gênes, que ce soit pour améliorer un truc, avoir plus de bouffe, ou n’importe quelle autre raison que ce soit. Ensuite, il fallait soit une erreur de dosage, soit un membre de l’équipe recherche et développement qui décidait d’un seul coup de se reconvertir en savant fou, et paf, des paramètres étaient changés.

Et à cause de cela se provoquait une réaction en chaîne qui finissait par créer des rongeurs mutants géant doté de volontés meurtrières et anthropophages. C’était le genre de truc qui arrivait tout le temps, les expériences qui tournaient mal.

D’autres théories semblaient plus ou moins plausibles : Effet des rejets de produits étranges dans les égouts, animal domestique un peu trop nourri, secte occulte dont le but était de créer des rats géants, ou encore le une quelconque espèce qui n’était pas encore connue à ce jour, et qui avait décidée d’être découverte en plein milieu des rues de la ville.

&nbsp;

Ils soupirèrent. En fin de compte, comment cela été arrivé importait peu, pour eux. C’était le travail ensuite de la police, des enquêteurs et du commité de bioéthique de la ville. Eux, ils n’étaient payé que pour tenter de dézinguer la menace… tout en essayant de pas se faire bouffer au passage.

Et c’était pas spécialement gagné d’avance.

&nbsp;

Bref, une sacré journée de merde en perspective.
