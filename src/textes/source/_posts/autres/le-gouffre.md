---
import: ../../../../crowbook/common.book
title: "Le gouffre"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/30 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/le-gouffre.pdf
output.epub: ../../../../../dist/download/le-gouffre.epub
download: le-gouffre
categories:
  - Autres
---
Édeline regardait le gouffre qui se trouvait devant elle. Un trou béant dans le sol. Une crevasse dont le fond n’était pas visible. Des kilomètres de diamètres, et une profondeur inconnue. Une brume emplissait le gigantesque cratère, rendant impossible toute mesure. Seule l’ombre, l’obscurité la plus totale semblait se trouver au bout de la chute.

Les bords étaient abrupts et impossibles à escalader, dans un sens comme dans l’autre, et même avec une corde ou un appareil volant, les vents violents qui s’engouffraient feraient se percuter violemment contre les murs quelqu’un qui tenterait une descente.

&nbsp;

Personne ne savait ce qui se trouvait au fond de ce gouffre, derrière toute cette brume.

Était-ce une vallée perdue comme dans les récits fantastiques ? Un monde isolé du reste de la terre ou se trouverait une vallée perdue ? Un monde où pouvait se trouver une ancienne civilisation, un ancien peuple, une ancienne espèce. Un monde où la terre de jadis pouvait perdurer en dessous de la brume, un monde ou un fragment de passer pouvait continuer à vivre, isoler du monde dans lequel Édeline vivait.

Était-ce une porte vers un quelconque monde fantastique, terre de magie et de possibilité inconnue ? En dessous de la brume se trouverait une autre terre, terre non pas de logique et de réalité scientifique, mais de sorcière et de dragon, de monstres et de dieux, de magie et de héros. Un monde qui serait dangereux mais fascinant, un monde qui offrirait toujours une part de mystère.

Où une porte plus sinistre, vers le royaume des morts, vers l’Hadès, porte ou les grands héros antiques devaient se rendre pour chercher chez les morts des conseils ou un être cher ? Cette brume était-elle le soupir des âmes, les plaintes des morts qui se matérialisaient, faisant oublier tout être qui y rentrait qu’il avait été vivant, telle des vapeurs de Léthé. Par delà cette brume l’on va dans la cité des pleurs ; par delà cette brume l’on va dans l’éternelle douleur ; par delà cette brume l’on va chez dans un monde perdu.

&nbsp;

Mais au dessus de cette brume se trouvait la peur, la peur de tomber. Le vertige prit Édeline, qui se sentit tituber. La chute était longue, forcément mortelle. Il y avait quelque chose d’à la fois fascinant et terrifiant. Repoussant et attirant. Ce n’était pas qu’elle avait envie de tomber. C’était qu’il y avait quelque chose de fascinant, qui donnait envie de se rapprocher. Comme une sorte de magnétisme morbide de la chute, tel l’odeur de la plante carnivore attire les insecte vers leur funeste destin.

Mais toujours en arrière plan se trouvant cette peur continue, permanente de tomber. De faire un faux pas, de chuter vers une mort certaine. La peur de se jeter volontairement dans le vide. Toujours, le problème n’était pas la chute en elle-même – cette partie là est toujours assez inoffensive – mais le contact avec le sol.

&nbsp;

Édeline fit un pas en arrière, et secoua la tête. Il était temps de revenir à la réalité, et de quitter cet endroit.
