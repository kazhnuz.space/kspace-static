---
import: ../../../../crowbook/common.book
title: "Hydrophobie"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/04 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/hydrophobie.pdf
output.epub: ../../../../../dist/download/hydrophobie.epub
download: hydrophobie
categories:
  - Autres
---
C’était la première fois que Naï se rendait sur Ark’Dulah, ville la plus importante du satellite Galahad 3b. Dans l’ascenseur, il s’était même senti nerveux : c’était la première fois qu’il se rendait sur un astre-océan, et encore plus important, la première fois qu’il se rendait dans une ville sous-marine.

Le scientifique travaillait depuis des années dans le domaine des recherches en technologie cristalline de sa nation, le Conglomérat d’Albion. Il se rendait aujourd’hui dans un colloque dans cette ville, situé sur une planète neutre face aux grands empire multi-planétaire. L’université de cette planète était une référence en manière de technologie cristalline, et il était certaine que ce serait enrichissant. Même si cela voulait dire aller dans une ville située sous des tonnes d’eau.

Il eut un frisson en y pensant.

Le fait de connaître en quoi les boucliers protégeant les villes sous-marines était entièrement sûr ne suffisait pas à ne pas lui laisser cette peur irrationnelle de finir écrasé sous des tonnes d’eau.

&nbsp;

Lorsque la porte s’ouvrit, il eut son premier regard sur la ville sous-marine. Il voyait d’immense bâtiment cristalin qui s’élevait, dans une architecture lui rapellant le gothique, mais dans des tons bleutés. Les tours étaient ouvragées, des petites statues de créatures sous-marines se trouvaient sur les bords des toitures. Et en haut, en guise de ciel se trouvait le bleu de l’océan, légèrement éclairé par les boucliers. Il pensa aux tonnes d’eau se trouvant au-dessus de sa tête et se sentit mal. Et sa sensation ne fit qu’empirer en voyant une immense ombre passer au-dessus des barrières. Cela ressemblait un peu à un poisson, mais il se rendit compte que la créature devait faire plusieurs kilomètres.

Il avait déjà vu des gigafaune, ces écosystèmes de créatures dépassant la centaine de mètres, mais jamais en vrai, et dans une situation qui lui semblait aussi effrayante.

&nbsp;

Il tenta de se défaire de cette image qui lui venait en tête de la créature qui brisait les boucliers. C’était sécurisé, se disait-il, ils ne peuvent pas ne pas avoir prévu des incidents avec ces bestioles. C’était impossible de ne pas avoir prévu ça. Mais même avec ça, la peur restait, encrée dans un coin de son esprit. Naï détourna son regard pour observer la colonne centrale, dont il était sorti. Cette colonne contenait des centaines d’ascenseurs qui reliait la ville sous-marine à une île artificielle se trouvant à la surface. Plusieurs cascades sortaient de cette colonne, remplissant les différents canaux de la ville.

Le scientifique devinait que l’eau ne venait pas d’en dehors du dôme mais était recyclé depuis l’intérieur, mais il se demandait pourquoi diable ils voulaient utiliser de l’eau comme symbolique alors qu’ils étaient coincés sous plusieurs tonnes de ce même liquide.

&nbsp;

Prise dans cette pensée, il en fut sorti par un immense bruit, et une vibration terrible. Naï poussa un glapissement terrifié. Les pensées fusèrent dans sa tête. Ça-y-est, c’était la fin, ils allaient tous mourir écrasés par la pression de l’eau ! Il regarda vers le haut pour voir ce qui se passait. La bête qu’il avait vu se cognait contre les boucliers. Cependant, ceux-ci tenaient sans la moindre difficulté, nullement impacté par le choc avec la bête titanesque. L’homme regarda autour de lui.

À l’exception de quelques quidams regardait la scène, les passants continuaient leur chemin, nullement effrayés par la situation. Ce qui voulait sans doute dire que c’était courant.

&nbsp;

Naï soupira. Il devinait que ce séjour allait lui sembler long…
