---
import: ../../../../crowbook/common.book
title: "Retrouvé"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/30 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/retrouve.pdf
output.epub: ../../../../../dist/download/retrouve.epub
download: retrouve
categories:
  - Autres
---

Dans la voiture, l’enfant dort, épuisé. Il a été retrouvé.

&nbsp;

C’est la fin d’un cauchemar.

&nbsp;

C’était une simple visite dans une forêt, ce qui aurait dû être un moment de joie à visiter un endroit où ils allaient rarement.

Une simple promenade en famille. L’enfant avait suivit d’un peu plus loin ses parents, répondant « oui oui » à chaque fois qu’ils lui disaient de ne pas trop s’éloigner.

Mais il s’était perdu. Il avait prit un autre chemin derrière leur dos, qu’il pensait être un raccourci, pour arriver devant eux.

Mais il s’était perdu.

&nbsp;

Il avait couru, les larmes aux yeux, tentant de retrouver son chemin. Il avait cru que courir l’aiderait à retrouver plus rapidement son chemin. Toutes les méthodes telles que chercher le nord grâce à la mousse des arbres.

Il avait couru, appelé, crier. Mais seuls les écho de la forêt et les bruits des arbres lui avait répondu. Il avait subi la fatigue, épuisé par la peur, par sa propre course, et par le désespoir qui montait.

Depuis combien de temps avait-il couru, perdu loin de ses parents ? 15 minutes, une heure, une demi-journée ? Il ne savait pas, le temps lui avait semblé infiniment long.

Il s’en était voulu de mal avoir suivit, il s’en était voulu de ne pas avoir écouté les consignes de ses parents.

*« Est-ce que je serais un jour retrouver ? Est-ce que je vais devoir rester ici à jamais, perdu loin de mes parents ? Comment vais-je manger ? Est-ce qu’il n’y a pas des grosses bêtes ici qui peuvent attaquer ? C’est quoi ce bruit ? Et celui-là ? »*

&nbsp;

Des sons qui avec la présence protectrice de ses parents lui aurait semblé être intéressants et intriguant lui avaient paru terrifiant, véritable dangers qui rodaient tapis entre les arbres et les buissons.

&nbsp;

Ses parent conduisent la voiture, rassuré.

Ils l’avaient appelés, chacun de leur côté, avec leur téléphones pour se retrouver. Ils s’étaient maudits comme jamais dans leur vie.

Ils avaient pensé à tout ce qu’ils auraient dû faire pour éviter cet incident. C’était inutile, mais ils n’étaient pas arrivé pas à s’en empêcher, pendant toute la recherche.

Ils s’en étaient voulu d’avoir fait reposer autant la responsabilité de ne pas se perdre sur lui, ils s’en était voulu de ne pas avoir fait plus attention.

&nbsp;

Un moment inattention et leur enfant était parti.

La peur les avait tiraillé : « Et s’il est blessé ? Il doit être terrifé à présent, tout seul dans les bois. A-t-il soif, a-t-il faim ? Il y a une rivière dans cette forêt, il peut se noyer dedans ! »

Tout les scénarios catastrophes leur était venu en tête, amplifiant leur panique.

Après un temps, ils avaient entendu ses cris. Ils avaient couru, et l’avaient retrouvé en pleur, blotti en boule, terrifié.

&nbsp;

Certains dirait qu'ils auraient dû le rouspéter, mais en ce moment ils étaient trop rassuré de l’avoir retrouver.

Il est en vie.

Leur fils est encore là, ils ne l’ont pas perdu.

Ils avaient cru qu'ils ne le reverait jamais, qu'il était parti dans la peur et la solitude.

Mais il était là.

&nbsp;

Et puis, pourquoi le punir quand il avait déjà bien trop subit les conséquences de sa transgression ?

Il avait déjà trop subit, il avait apprit cette leçon d'une manière qui était déjà terrifiante.

&nbsp;

Dans la voiture, l’enfant dort, épuisé par ces événements. Il a été retrouvé.

&nbsp;

C’est la fin du cauchemar.
