---
import: ../../../../crowbook/common.book
title: "Élégance"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/17 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/elegance.pdf
output.epub: ../../../../../dist/download/elegance.epub
download: elegance
categories:
  - Autres
---
« Nan, mais c’est pas possible, je peux pas porter ça, je vais être ridicule ».

&nbsp;

Sous cette complainte classique, c’était un cri du cœur qui sortait de la bouche de Lucie. Dans cette robe de soirée, elle se sentait gênée comme jamais. Elle regardait avec envie le costume de son frère, Lucas. Celui lui semblait tellement plus proche de ce que les deux jeux adolescents portaient habituellement, juste à un prix beaucoup plus élevés.

Cependant, comme s’il avait entendu les pensées de sa jumelle, il rétorqua.

&nbsp;

— « Te plains pas, toi tu ressembles pas à un pingouin… »

&nbsp;

Les deux jumeaux étaient obligés de se rendre à une soirée huppée, pour des raisons obscures liés au métiers de leurs parents. S’ils avaient bien compris, ils avaient fait un truc, ce truc avait été apprécié, et du coup ils étaient passé à la télé – ça c’était super cool – et ils avaient été invités avec les enfants à la soirée.

Et les deux jeunes adolescents étaient donc forcé de porter ces tenues.

Ils se sentaient tellement gênés dans leurs costumes qu’ils se demandaient s’ils ne ferait pas mieux de venir tout nus…

&nbsp;

On leur avait dit que c’était important, que là-bas, tout le monde était élégant, et qu’ils devaient l’être aussi. Aucun des deux ne s’était jamais décrit comme « élégant », et ils avaient l’impression que leur faire croire qu’ils allaient être élégant juste en portant ce costume, c’était un mythe. Lucas savait parfaitement qu’il n’allait pas parler de la même manière que les autres, et que ça se remarquait. Lucie avait lu une fois quelque chose sur le nombre de couvert qu’il y avait. Et si elle se trompait de fourchette ? Tous deux comprenaient qu’il y avait certains codes pour être élégant, et qu’il leur manquait les clefs pour les déchiffrer.

Ils n’auraient pas les mêmes références, ils seraient plongés dans un monde dont ils ne connaissaient rien. Lucas avait même voulu prétexter de devoir réviser une dictée pour échapper à cela.

&nbsp;

Ils avaient peur. Et si par leur comportement, ils étaient reconnus comme ne faisait pas partie de ce monde ? Seraient-ils des intrus, n’ayant aucun droit d’être présent en ces lieux ? Seraient-ils ridicules de par leurs vêtements et leurs comportements ? Ils ne savaient pas si c’était vrai ou faux, mais en tout cas avaient assez peur que ce soit vrai pour vouloir éviter que cela arrive. Lucas remarqua que c’était sans doute la première fois que lui et Lucie étaient inquiets d’être à un endroit où ils ne devaient pas être.

Cependant, malgré toute leurs protestations, les parents les amenèrent dans la voiture, scellant leur triste sort.

***

Au sortir de la soirée, les enfants se dirent que ça avait été un succès, contrairement aux adultes qui l’étaient moins, et ce pour la même raison.

Même si le début de la soirée fut prise d’un silence gênant avec les enfants des autres adultes, il y avait un langage universel, qui pouvait transcender les différences de langage, et composer leur manque dans cette discipline auguste et ancienne qu’était l’élégance :

&nbsp;

Les bêtises.
