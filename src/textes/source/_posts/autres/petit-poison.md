---
import: ../../../../crowbook/common.book
title: "Petit poison"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/03 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/petit-poison.pdf
output.epub: ../../../../../dist/download/petit-poison.epub
download: petit-poison
categories:
  - Autres
---
— « Nan ! Je veux pas ! »

&nbsp;

Le grand chateau de l'Empire Skelfing, controlant la majorité de la planète Aestrus, faisait face à une nouvelle crise, des plus terribles. Une crise qui touchait même la famille royale.

Vekyna, princesse de la famille Skelfing, refusait de mettre la tenue d’apparat, et le faisait savoir avec tous les moyens dont elle disposait.

&nbsp;

La tenue d’apparat était la tenue que tous les membres de la noblesse impériale se devait de porter lors des évènements important. Si cette tenue était identique pour les hommes et les femmes, son nombre d’ornement et sa complexité augmenté au fur et à mesure que le rang augmentait. Et en tant que princesse héritière de tout l'empire, Vekyna devait porter une tenue encore plus compliquée que celle de son père, le roi Hetjul.

Et la petite lionne noire détestait les tenues compliquées.

&nbsp;

Si on lui demandait son avis, elle se promènerait nue partout : c’est bien plus pratique. Hélas, les conventions sociales et la température froides de la province de Bymann forçaient de porter des tenues. Quelle idée de vivre dans le froid ! Mais si pour les tenues usuelles, la petite fille pouvait faire des exceptions, elle ne voulait pas se faire emballer dans cet ensemble de bout de tissus incompréhensibles.

Et c’est ainsi qu’elle se retrouva à courir partout en culotte, projetant tous les gardes à l’aide de ses pouvoirs, et que la pièce où elle avait du se changer avait sa tapisserie déchirée et deux armoires projetées par la fenêtre, laissant la neige s’occuper de la seule chose qui était encore intacte : le magnifique tapis.

&nbsp;

Pour couronner le tout, au plus grand malheur de Myrkur et Dahor, guerriers émérite de l'empire, c’était eux qui devaient réussir à la récupérer et à la forcer dans la tenue. Myrkhur était le cousin du roi Hetjul, et avec son époux, était parmi les guerriers les plus réputés. Ils avaient été assignés à la protection de Vekyna.

Ou, comme ils le disaient parfois, à la protection du monde entier face à cette menace qu’était la petite princesse.

&nbsp;

— « Nan ! C’est trop nul ! »

&nbsp;

Dahor fut projeté par la vague d’énergie qui se fracassa contre lui. Il poussa un juron. Ils l’avaient amené à l’étage exprès, afin de bloquer ses pouvoirs. Les Skelfing tiraient de ses pouvoirs de la terre elle-même, comme tout les Soldat de la Terre. Cependant, dès qu’ils dépassaient quelques mètres au-dessus du sol, ils étaient incapables de tirer l’énergie tellurique. Enfin, la plupart d’entre eux. C’est ainsi qu’ils découvrirent que déjà à 8 ans, elle faisait partie des très rares suiveur de Silice à être capable d’accomplir cet exploit.

Étalé contre le sol, Dahor se dit que les duels qu’il avait dû accomplir contre des Gigantiens lors de la campagne d'Alcofribas furent des expériences moins pénibles que surveiller cette gamine.

&nbsp;

Myrkur s’était tapi dans l’ombre. Comme tous les assassins, il était un spécialiste lorsqu’il s’agissait de prendre par surprise ses ennemis. Il entendit la petite s’approcher : elle commettait l’erreur de chantonner. Myrkur se demanda un instant si Hetjul lui pardonnerait de ne pas inculquer la discrétion à sa fille si c’était pour une bonne cause tel qu’éviter les destructions qu’elle pourrait causer.

Il bondit d’un geste leste sur la petite lionne pour l’attraper. Celle-ci, poussant un cri de surprise, sauta pour grimper en haut de la grande armoire qui se trouvait à côte d’elle.

&nbsp;

— « Noooooooooooooooooon », protestait-elle.

&nbsp;

Myrkur réussi à lui attraper une jambe, la tenant par les pieds. La petite tomba et il l’attrapa, la tenant sur son épaule comme un sac à patate. Elle poussa un cri de protestation, outrée de s’être fait avoir. Dahor arriva, se frottant le dos, bougon.

&nbsp;

— « Myrkur, chéri, la prochaine fois, évite de m’utiliser comme appât, tu veux ? »

&nbsp;

Myrkur rendit un sourire à Dahor. Il était fier d’avoir réussi à endiguer la crise avant qu’il y ait eut trop de dégâts. Mais dans son sourire, il y avait encore la peur d’un futur qui s’annonçait plein de pièges et de malheurs.

Il savait que ce n’était qu’une victoire, et que la guerre restait à gagner, une guerre qui s’annonçait difficile et capable de ternir à jamais la vie de la famille impériale, voir de l'Empire tout entier !.

&nbsp;

Réussir à habiller cette petite peste.
