---
import: ../../../../crowbook/common.book
title: "Introduction"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/03/31 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/illusions.pdf
output.epub: ../../../../../dist/download/illusions.epub
download: illusions
categories:
  - Autres
---
Une pièce blanche. Aseptisée. Entièrement vide. Une moquette tout aussi pâle, et des murs qui ne revêtent pas plus de couleurs. Pas de doute, j’y suis de retour. Je prends une chaise en attendant son arrivée. Il ne devrait pas tarder. Je m’assieds, et regarde le vide des murs. Dans un lieu qui n’existe pas, une longue attente hors du temps d’une personne qui n’existe pas. Pour patienter, je prends un magazine et un petit gâteau.

Il finit par arriver. Mon Némésis, mon meilleur ami. Mon oppresseur, ma victime. Celui qui toujours veut me chasser, celui qui toujours cherche à me retrouver. Celui qui me console, celui qui m’humilie. Celui qui veut voir mes couples s’effondrer, celui qui me donne des conseils pour aider les personnes que j’aime. Celui qui excite ma rage, celui qui la tempère.

Ma conscience, mon démon intérieur.

&nbsp;

— Cher monsieur, commençait-il, vêtu d’un costard. Je vous ai fait quérir dans mon bureau parce qu’il faut qu’on parle d’un sujet très important.

&nbsp;

Je m’assieds sur la chaise, lui est bien enfoncé dans son confortable fauteuil, dos à la fenêtre. Derrière lui, une superbe vue sur la ville et ses multiples lumières, ce tableau étrange composé de taches de couleurs sur un fond noir.

&nbsp;

— Es-tu bien certain que tout dans ta vie est bien réelle ? Me demande-t-il avec un air sérieux à travers ses lunettes.

&nbsp;

Je fronce les sourcils. Où est-ce qu’il veut en venir ? Je ne suis pas certain de comprendre quel est son but.

&nbsp;

— Comment te dire… Je pense qu’il doit y avoir quelque chose qui n’est pas normal dans tout ce qui se passe. Je regarde tes notes, je regarde ta situation, je regarde ton nombre d’amis… C’est vraiment pas mal. Mais quand je vois ensuite ton investissement dans tout ça… Y’a comme un truc qui colle pas.

&nbsp;

Il se penche un peu plus vers moi, comme pour m’examiner du mieux qu’il peut.

&nbsp;

— En effet, notre conférence d’aujourd’hui portera sur ce sujet très important. Est-ce que le monde existe ou n’est qu’une illusion ? Cette question est très intéressante aux vues de la polysémie du mot « monde ».

&nbsp;

Je suivais la conférence, dans mon siège de l’amphithéâtre miteux ou j’ai eut une partie de mes cours, à une époque qui me semble étrangement lointaine. Il me faisait face, j’étais son seul public. Autour de moi, les sièges n’étaient pas vides, mais plein d’ombres sans visage.

&nbsp;

— En effet, si le monde peut rapporter à la réalité physico-mathématique où l’on vit, il peut également s’agir d’un sens plus « mondain », si je puis me permettre. Il peut en effet s’agir de la société humaine dans laquelle nous évoluons, où quelque chose de plus proche comme notre cercle d’amis.

&nbsp;

Il fit quelques pas, au centre de sa scène. Il était dans le feu des projecteurs. Il a toujours aimé ça.

&nbsp;

— L’une des premières particularités de l’humain, c’est sa tendance au mensonge. En effet, en tant qu’une des seules espèces intelligente, l’humain à ce pouvoir de mentir. La « guerre juste », « tous les hommes naissent libre et égaux ». Toute la base de l’humanité est le mensonge : En effet, après avoir mordu le fruit originel, Adam et Ève s’aperçoivent de leur nudité, et en ont honte. C’est la genèse qui nous montre le premier mensonge, qui naît avec l’arrivée de l’humanité : Le fait de cacher sa nudité.

— En effet, regarde le monde autour de toi ! Regarde tous ces mensonges qui remplissent notre réalité ! Regarde tous ces faux-semblants. Qu’est-ce qui te prouve que quand toi tu penses que c’est vrai, ça l’est, hein ?

&nbsp;

Terrassé par le coup de poing qu’il venait de m’assener en pleine figure, je vins m’aplatir contre le bitume de la cours. Qu’est-ce qui m’avait pris d’accepter de me battre, je savais pourtant que je n’avais jamais été fait pour ça.

&nbsp;

— Qu’est-ce que tu te fais dire que tout ce qu’on t’a dit et que tu considères comme vrai, ça l’est ? Des mensonges, tu en as eut des tas ! Mais encore et toujours, tu te raccroches sottement à l’espoir que dans le lot, il y ait quelques trucs vrais.

&nbsp;

Il m’attrapa par le col, et me souleva. Je vis dans ses yeux les reflets des miens.

&nbsp;

— Parce que l’autre possibilité te fait peur.

&nbsp;

On est de retour dans la salle blanche. Il était face à moi. Plus de mise en scène grotesque. Plus de saut du coq à l’âne. Il ne faisait qu’une petite introduction pour en venir à son sujet. Nous étions toujours dans la même position que précédemment. Il attendit un moment.

&nbsp;

— Parce que l’autre possibilité, c’est qu’en fait, tous ce qu’on t’a dit est faux.

&nbsp;

Il me relâcha. Je fis quelques pas pour m’éloigner de lui… Mais je ne pouvais pas fuir.

&nbsp;

— Regarde un peu autour de toi, et surtout la vérité en face. Regarde cette fable que tu appelles la vie, et ose me dire encore un peu qu’elle ait un sens. Et donne-moi le sens des gens qui sont morts. Et donne-moi les preuves que les mots qu’on te dit sont sincères. Donne-moi les preuves que tous ne roulent pas les yeux dès que tu ne t’es pas éloigné, avec toutes les conneries que tu dis. Avec toutes les fois où tu te donnes toi-même des défis que jamais personne t’as demandé de relever, pour les foirer lamentablement devant tout le monde.

&nbsp;

J’essaie de préparer mes mots pour lui répondre. Il suffit de trouver les bons mots, et je peux le faire partir.

&nbsp;

— Et je ne peux pas simplement les croire. Je tiens à eux, et je sais qu’ils tiennent à moi. La confiance, c’est un peu la base de tout ça ? Si je commence à être paranoïaque et croire que tout le monde me veut du mal, ça ne va pas le faire.

&nbsp;

Il fit quelques pas. Il rigolait. Je déteste quand il fait ça.

&nbsp;

— Parce que tu crois que c’est par méchanceté qu’on ment ? Tu irais dire à quelqu’un de pathétique qu’il l’est, pour l’enfoncer encore plus ? Tu irais lui dire à quel point il est mauvais ? Où tu préférerais pas lui mentir, pour éviter de lui faire du mal ?

— Les choix ne se limitent pas à ça, m’énervais-je ! Déjà, primo, personne n’est « pathétique », on peut être positif ! Tu crois que j’ai pas assez potassé la positivité et tout ? Suffit d’avoir la bonne tournure d’esprit, ce n’est pas du mensonge.

&nbsp;

Un sourire amusé.

&nbsp;

— Sérieux, le coup larmoyant de l’éducateur positif. « Regardez-moi, comme je suis le grand chevalier pourfendeur de la croyance en la hiérarchie, regardez comme je suis un saint qui jamais n’irais juger quelqu’un comme en dessous de lui ». T’as besoin que je te fasse les flashback de toutes les fois où tu as pensé ce genre de chose ?

&nbsp;

Il marqua un temps.

&nbsp;

— Et puis, combien même ce ne serait pas le cas sur le cas… Les raisons de soupirer c’est pas juste de croire que quelqu’un est « pathétique » ou « ridicule ». Voici les autres cas : Il peut être agaçant, insupportable. Il peut donner des envies de le baffer… Mais on est bien obligé de le supporter, donc on prend sur nous.

&nbsp;

Il pointa du doigt, victorieux.

&nbsp;

— Ce genre de mensonges, ceux que tu as entendu par le passé, ceux que tu as vu quand tu as découvert que le monde était une grande boucherie sans aucun sens… Tout cela remonte au père noël ! Le monde est un grand tas de mensonges, auquel tu contribueras à chaque fois que tu feras croire à quelqu’un qu’il a de l’importance, que sa vie sert à quelque chose…

Pense à tous ces détails douteux, à tout ce qui ne colle pas dans ta vie par rapport à ce que tu mériterais. Pense à tout ce qui est trop beau.

&nbsp;

Il me fait face, enfoncé dans son fauteuil

&nbsp;

— Pense aux mensonges de l’humanité. Pense à toutes les « guerres justes » commises à coup de bombe sur des villes.

&nbsp;

Il me fait face, éclairé par les projecteurs sur l’estrade.

&nbsp;

— Pense aux doutes qui t’habite, pense à toutes ces fois où tu n’as pas compris pourquoi on pouvait t’accepter.

&nbsp;

Il me surplombe, tandis que j’essaie de me relever, étalé sur le bitume.

&nbsp;

— Pense à tous ceux qui vont devoir supporter le fait de chuter après avoir cru que leur vie avait un sens, comptait pour quelqu’un.

&nbsp;

Il me domine, dans une pièce blanche, aseptisée et entièrement vide.

&nbsp;

— Ce n’est qu’en acceptant l’absence de vérité que tu pourrais apprendre la véritable paix intérieur : tout est faux, donc je ne dois plus me préoccuper de tout ça.

&nbsp;

Un blanc. Je ne sais pas quoi dire. Je prends une inspiration.

&nbsp;

« Peut-être que la vie n’a aucun sens. Peut-être qu’on est juste qu’un amas d’atome qui font des réactions cheloues entre eux. Peut-être qu’il y a des tas de trucs qui sont « trop beaux ». Peut-être que le monde est bourré de mensonge. Peut-être qu’il y a des tas de gens qui me déteste, en fait. Peut-être. Et peut-être que non. Cependant… Il existe quelque chose qui à un sens dans tout ça.

Ou plutôt, il y a quelque chose qui a encore moins de sens : toutes ces questions. En fin de compte, si la vérité n’existe pas, est-ce que le mensonge peut exister ? Si tout est mensonge, alors, est-ce que la réalité n’est pas l’ensemble de fausse vérité dans laquelle on nage, ce qui leur donne une réalité. La nôtre. Parce qu’on existe dedans.

Peut-être que tu as raisons, et que je devrais croire en rien. Mais où serait le but. Qu’est-ce que douter de tout m’apporterait en plus, à part le fait de me questionner sans arrêt encore plus ? Remplacer la question « est-ce que c’est faux » par « qu’est-ce qui est faux là-dedans » n’apporte pas la paix de l’âme. Elle n’apporte qu’encore plus de désarrois. Elle remplace l’appréhension du coup de poignard, par celle de quel organe sera transpercé par le poignard. Elle remplace la crainte par la terreur permanente, elle remplace l’espoir du bonheur par une vague espérance que ça ne fera pas trop mal. Elle remplace le risque de voir sa confiance trahie par la solitude de ne pouvoir l'accorder.

Pour répondre à ta question : Je ne sais pas si le monde existe vraiment, si y’a quoi que ce soit de vrai. Voilà ton aporie. Peut-être que tout est faux, peut-être que y’a des trucs vrai. Je ne sais pas. Peut-être que tout est faux, et c’est pour ça que je veux tenter de vivre ma vie comme je l’entends moi, et en croyant à ceux autour de moi.

Cependant, voilà la véritable réponse : En fait, ce n’est pas qu’on a la réponse, c’est qu’on a pas trop le choix. »

&nbsp;

Et je referme le rideau.
