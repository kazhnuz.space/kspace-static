---
import: ../../../../crowbook/common.book
title: "Grouillement"
lang: fr
version: 0.1.0
license: CC-BY-SA 4.0
date: '2017/10/15 12:00:00'
proofread.grammalecte: true
output:
  - pdf
  - epub
  - proofread.html
output.pdf: ../../../../../dist/download/grouillement.pdf
output.epub: ../../../../../dist/download/grouillement.epub
download: grouillement
categories:
  - Autres
---

Uther était prêt. Il avait vérifié tout son équipement, de quoi survivre, de quoi réussir à fuir si le danger devenait trop important. Des vêtements lisses, antidérapant, avec une forte capacité de protection. Des chaussures confortable pour avancer d'un pas ferme et décidé. Aucun ne devait réussir à agripper à lui, et toute substance sortant de leur bouche devait pouvoir être lavée vite.

Uther pensa à sa famille, à sa femme, à ses enfants, pour se donner du courage. Il se devait de réussir cette épreuve.

&nbsp;

Il allait devoir traverser l’espace où ils grouillaient. Où ils pouvaient la voir. Où ils se rapprocheraient sûrement d’elle. Ce n’était pas qu’il les détestait. C’était plus une capacité peu courante à les voirs comme ils étaient. Leurs grands yeux vides et globuleux qui la fixait si il s’approchait, comme s’ils se préparaient à faire quelque chose. Les bruits bizarres quand ils s’approchaient de lui. Les substances indéfinies – et qu’il ne souhaitait surtout pas définir – qui pouvaient s’en échapper. Ils étaient de l’autre côtés.

Il les voyait comme les création du diable qu'ils étaient

&nbsp;

Il se maudissait. Tout cela à cause d’une erreur bête, tout cela à cause d’un oubli. Il n'avait pas fait attention, et avait laissé un document important. Et pour le chercher maintenant, il devait aller dans la salle ou ça grouillait, dans la salle avec les sons étranges.Lorsque l’informaticien avait accepté ce post, il savait qu’il devrait faire quasiment tous les bâtiments de la ville.

Tout ce que possédait la commune contenait des ordinateurs, et tous demandaient souvent d’être réparés. Il se devait d’intervenir partout ou il y avait des pannes, quel que soit l’endroit, même dans les pires lieux, les plus dangereux.

Et même dans cette antichambre de l’enfer, cet antre délabré né du pêché et qui abritait des êtres vils et répugnants. Des êtres dégoulinant, bruyant, qui s’attaquaient les uns les autres. Des êtres sournois, capable de torturer juste par jeu. Les cris et les bruits étaient audibles de l’autre côté de la porte. Il allait falloir y aller, traverser tout ça.

&nbsp;

Il hésitait. Devait-elle y aller véritablement ? N’y avait-il pas une autre solution ? Il savait qu’il avait absolument besoin de ce dossier. Que sans lui, il ne pouvait pas avoir les informations nécessaires pour modifier les pilotes. Fichue technologie, qui le trahissait au pire moment ! Encore une fois, les machines montraient qu’elles n’étaient pas digne de confiance. Mais il pouvait peut-être travailler sur autre chose, le temps que ça se calme ? Trouver un autre truc à faire, et le faire lentement pour repousser l’échéance ?

Uther se ressaisit. Il devait être fort. Rendre fièrs sa famille, sa femme et ses enfants. Vaincre l’adversité. Que valait-il si il abandonnait face à la difficulté ? Que valait-il, si il n’affrontait pas la peur, l’horreur. Ce n’était pas parce que ça grouillait de ces créatures qu’il devait abandonner. Il devait affronter sa peur, réussir à passer le fouillis. Il devait voir cela comme une épreuve. Et faire attention à ne pas marcher sur un. Ce serait le pire. Il inspira et expira lentement.

&nbsp;

Il était prêt.

&nbsp;

Uther devait aller chercher le dossier contenant toutes les données sur les ordinateurs. Et pour cela, il devait traverser la salle de jeu de la crèche, infestée par la plus vile créature : les enfants des autres.
