# Ère originelle - Création du monde (légendes)

  1.1 - Arrivée d'Hespéra et construction de la planète
  1.2 - Les "enfants" d'Hespéra commence à tenter d'aider des dieux.
  1.3 - Début de guerre entre les enfants. Hespéra commence
  1.4 - Blessure et "mort" de Styx. La légende dit que c'est sa souffrance qui contamina le Styx.
  1.5 - Hesper se retire du monde. Les enfants délaissent la planète.
