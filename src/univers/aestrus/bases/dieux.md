# Les dieux et religions d'Aestrus

Sur Æstrus, les dieux et les religions fonctionnent différemment que dans notre mondes. En effet, les dieux y sont ici des faits historiques, et font partie de l'univers au lieu d'en être "séparé" (par un aspect métaphyisique ou autre). Les dieux et leur pouvoir y ont des effets concrêt dans la vie de tout les jours, à travers les *16 guildes élémentaires*.

Les religions y sont de ce fait plus des regroupement philosophiques et des hiérarchie, voir des institutions qui ont été créé dans des buts précis.

## Hesper, déesse créatrice

Hesper est la déesse-mère de l'univers d'Æstrus, et est considérée comme une déesse bienveillante et protectrice. Créatrice du monde et fondatrice de la lignée des philosophes-roi (qui étaient à l'origine plusieurs en même temps, avant l'époque pré-classique), elle est la déesse principale des trois religions d'Æstrus.

On raconte que ses pouvoirs et sa sagesse sont infinie, et que le but des philosophes-roi est d'atteindre ne serait-ce qu'une partie de sa sagesse. Tout les cultes la mette au centre du panthéon des dieux.

La légende raconte qu'après la mort de sa fille cadette Styx, elle a été rempli d'un grand chagrin et s'est retiré dans la Lune pour y dormir à jamais. C'est à ce moment qu'elle aurait nommé les 12 philosophes-roi pour la remplacer. Cependant, elle protégerait toujours la planète et auraient le pouvoir d'amener la vie vers le bonheur pour celles et ceux qui feraient le bien autour d'eux.

### Les 16 dieux et leurs guildes

Les 16 dieux et déesse sont les 16 enfants de la déesse Hesper. Ils ont régnés dans les premiers temps, avant de laisser leur place à la Civilisation Elysienne. Cependant, à cause de l'hybris de la civilisation Elysienne et ses envies de conquête, la légende raconte qu'ils envoyèrent les anges, des êtres magiques purs, pour vaincre la civilisation.

Iels représentent les 16 éléments du monde aestriens, et sont la fondation - mais pas nécessairement les fondateurs - des 16 guildes æstriennes. Ces guildes sont apparues durant toute l'ère pré-classique partout à travers le monde, et ont eu une influence considérable sur l'évolution des lieux où elles sont apparues. C'est eux qui fournissent les pouvoirs des membres de leur guilde, mais ils ne sont pas toujours en contact direct avec : les dieux ont des caractères très différents, et restent généralement en retrait même si certains sont plus proche que d'autre de leurs guildes. Les guildes suivent les enseignements nés du caractère de leur dieu, et restent encore aujourd'hui parmi les principales influences qui existent sur la planète Æstrus.

La 17e déesse, Styx, aurait été tué par un bandit lors des premiers temps du monde, conduisant la déesse à s’exiler et la fondation des philosophes roi.

## La théoturgies des anges

Ce culte est le plus ancien des culte Hespérien encore existant, et une religion surtout présente sur le continent d'Etherial, dans les terres de Tuhan et de Télémie. C'est également la religion de l'Empire de Skelfing, même si de grands différents idéologiques existent entre l'empire et la théoturgies.

On raconte que cette théoturgie est une religion de repentance fondée par le dernier survivant des Philosophes-Roi de la civilisation Elysienne, après l'évenèment légendaire de la *Descentes des Anges*, quand des envoyés des 16 dieux auraient mis fin à la civilisation Élysiennes, qui avait trop tenté. Le chef de cette religion, le Basileus, se revandique comme "dernier philosophe-roi", et était à l'origine le conseiller du Heriocrate de Tuhan.

Ce culte à pour objectif de combattre l'hybris, et de cacher aux mondes les savoirs anciens de la civilisation élysienne, en grande partie perdus. Elle a été la religion de la Hierocratie d'Aztlann, qui a fondée une civilisation inspirée de celle Elysienne mais entièrement fondée en réaction à l'ance. Cette religion estime que les Empire ne peuvent être éternel, et qu'il ne faut pas chercher à retrouver leurs savoirs.

La théoturgies des Anges s'oppose activement à l'exploration des Ruines Elysiennes et Aztlannienne.

## L'Église d'Alicia

L'Église d'Alicia est la principale entité lié aux dieux de la planète Æstrus. Cette église a été fondée au début de l'époque classique - sa fondation marquant le début de cette ère - par la prophétesse Alicia, première Archidiacre. Alicia l'aurait fait pour faire face aux excès de la guerre permanente dans laquelle les guildes se trouvaient. Il s'agit d'une philosophie mettant au centre du monde d'Æstrus les 16 guildes, dirigée par une Archidiacre, nommée à vie par un conseil fondée des 16 évêques, représentant de chaque guilde.

L'objectif de l'église est en cela d'unir les 16 guildes, dans le but d'éviter que leur conflits ne s'enveniment et embrasent le monde. Sa vision du monde est que les 16 dieux étant les 16 enfants de la déesse Hesper, leurs fidèles doivent vivre en harmonie, afin de ne pas commettre de parjure envers les autres dieux. Dans l'église, chacun est donc libre de vénérer principalement le dieu élémentaire qu'il veut, mais doit respecter les autres.

Elle est aujourd'hui un des interlocuteur du Magister lorsqu'il a besoin d'intégrer les guildes à son armées. Certain⋅e⋅s membres des troupes radiantes sont d'ailleurs plus affilié à l'église qu'au Magister.

## Le culte du Grand-Prophète

Le culte du Grand-Prophète est le plus récent des cultes, et est particulier parce qu'il s'agit du seul culte dirigé directement envers la déesse Hesper, où plus précisément un culte visant à la compassion envers la souffrance causée par la mort de Styx. En cela, ce culte est également souvent nommé "culte de Styx". D'après les légendes de ce culte, la tristesse de Styx se trouveraient partout dans le monde à travers le Styx.

Ce culte met en avant la douleur de la grande déesse, qui voit son monde constamment attaqué par les guerres et la haine. Pour lui, tout les Æstriens sont enfant de la déesse, et chaque guerre est un blasphème quand elle n'est pas justifiée. Ce culte est principalement présent en Andale et dans le royaume d'Alcofribas.
