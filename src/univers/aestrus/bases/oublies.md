# Les quatres peuples oubliés

Les quatres peuples oubliés, ou « sans dieux » sont quatre peuples qui ont toujours vécus hors des questions de divinités et des religions. Ils n'ont "d'oublié" que le nom : leur connaissance est relativement connue de tout les milieux lettrés, et n'est en fait pas véritablement un secret. Ils sont plus oublié dans le sens où ils ne vivent que peu dans la communauté internationale.

Ils sont parfois aussi nommé les "sans dieux", sur ce qu'ils ont pratiqué des formes de magie proche des guildes, mais sans dieux pour les protéger. Peu est connu sur eux, et les rumeurs les plus absurdes peuvent circuler sur leurs pouvoirs.

Tout les membres de ces peuples, où les practiciens de leur maitrise magique se reconnaisses par l'effet que leur magie à sur les yeux de ceux qui la pratique.

## Le peuple Sidh

Le peuple Sidh est un peuple tribal, fonctonnant sous forme de clans plus ou moins nomade dans la région de Sylvera. Ce peuple sylvestre reste éloigné des affaires du monde, aimant peu les contacts avec le monde extérieur. Ils auraient le pouvoir du poison, pouvant ronger les êtres vivants, plante où animaux.

Leur magie laisse des cernes profond et distinct d'un violet sombre.

## Le peuple Tublehtohl

Ce peuple est un peuple vivant dans une ville fortifiée d'Anvêbre, la cité de Tubleh. Peuple à l'organisation hiérarchique complexe, et très militaire. On raconte qu'ils ont le pouvoir de démultiplier leur force, aux point où cela peut être dangereux pour eux. Ils sont des combattants surtout à mains nues.

Leur magie provoque l'apparition d'une fine ligne brillante au niveau de leurs yeux, entourant toute leur tête.

## Le peuple de Choris

Ce peuple est un peuple qui a existé dans les premiers temps du Premier Royaume de Bymann, mais qui a disparu en tant que civilisation durant l'ère Classique, suite à différents soucis dans le royaume, tel que leur mise au ban. Leur savoir ne sont conserver qu'en secret, même s'il n'y a plus de persécution aujourd'hui. Leurs techniques sont basées sur leur maitrise du son.

Leurs magie provoque une l'absence de pupille dans les yeux, ainsi qu'un iris extrèmement pale.

## Le peuple du Troisième Oeil

Ce peuple à des origines complexes. Il viendrait originairement d'Agonie, mais aurait été présent également sur diverses îles telle que l'archipel iladiens, notamment l'île Spectrale dont ils ont protégé la faille Stygienne. Ils vivent surtout dans l'Empire Draämn d'Agonie. Ce peuple utilise les pouvoirs de l'esprit et de la pensée, et sont réputé pour être télépathe.

Leur magie prend les yeux entièrement noir, avec une pupille violette.
