# Le Magister

Le Magister est une « république » confédérale parlementaire qui couvre la majeure partie du continent de Cardia, ainsi que l'Agonie. Plus puissante nation de la planète, elle regroupe les anciens état envahi par l'Empire de Skelfing, qui ont décidé de s'unir pour éviter de se faire à nouveau envahir.

Plus puissante nation de la planète, elle devra cependant faire face à de nouvelles menace durant la fin de l'époque contemporaine.

## Les Troupes Radiantes

Les troupes radiantes est l'armée du Magister, qui a pour objectif de protéger tout le Magister des menaces qui peuvent exister. Il s'agit en grande partie de membres de guildes, qui fonctionne en tant qu'unités de protection des civils contre les différentes menaces.

Ces troupes sont constitué de petites unités de 5 à 7 individus, et font des missions à l'aide de technologies élémentaires. La majorité des membres de ces troupes sont aussi des membres des guildes, certains directement sous l'autorité de l'Église d'Alicia (iels ont donc un status particulier dans les troupes).

On trouve de toutes les guildes dans les Troupes Radiantes.

## Chelmos Corporation

La société Chelmos Corporation est l’un des principaux exploitant de l'énergie stygienne. Holding co-détenu par des actionnaires privés et par le Magister, la société recherche dans le domaine des énergies stygiennes et de leur utilisation, à la fois en tant qu'énergie pure et que par les application de leur énergie raffinée.

L'entreprise est également l'une des principales sources de *technologies élémentaires*, qui utilisent les énergies élémentaires afin de pouvoir créer des technologies adaptées. Elle construit aussi bien des armes élémentaires, que des machines volantes...

### Les centrales stygiennes

Les centrales stygiennes sont une source d’énergie de plus en plus présente sur toute la planète. Construite sur les veines stygiennes et sont utilisées depuis environs vingt ans (pour le marché publique) à vingt-cinq ans (pour le Magister). L’énergie produite par les centrales stygiennes revet deux forme : l’éléctricité, ou les fragment stygiens. Les centrales Stygiennes sont l'invention de la scientifique , dirigé sous l'égide du

L’intérieur d’une centrale chaotique est cependant un endroit dangereux à cause de l’influence de l’énergie chaotique et de sa transformation en électricité ou en *fragment stygiens*, et est réservé à des connaisseurs de l’énergie stygiennes. Le cœur d’une centrale chaotique, nommé le **noyaux stygiens**, est un endroit ou l’énergie circule suivant des patterns très précis.

Ils utilisent notamment pour cela les thèses de Teal Ishtar sur ce type d’énergie, quand il était chercheur à l’université de Cardiopolis. La première centrale a été fondée sur l'Île Spectrale dans l'archipel Iladien. Cette île avait la particularité d'être le dernier territoire du peuple du Troisième Oeil, l'un des quatre peuples oubliés. Elle a été implémentée par la fille d'Ishtar, elle-même membre de ce peuple, sous la supervision d'Ys Gradlon, dirigeant des forces du Magister à l'époque.

Cependant, un incident mystérieux provoqua la disparition de la majorité du peuple, ainsi que du commandant Gradlon.
