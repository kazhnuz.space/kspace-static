# Espèces de Æstrus

## Lierbil

Les Lierbil sont la deuxième espèce la plus courante sur le continent de Cardia, habitant à l'origine dans les climats plus tempérés et plat du continent. Cette espèce herbivore est réputée comme étant plus faible physiquement, et peu avantagée pour le combat. Cependant, ils sont également vu comme débrouillard, et pouvant rapidement s'adapter aux situations.

Dans les sociétés traditionnelles, les lierbils ont souvent occupé les classes sociales les plus basses de la sociétés. Ils sont de petites tailles, ont des fourrures généralement courte, une queue (touffue ou non) et de grande oreilles.

## Danvad (grand herbivore)

Les Danvad sont la troisième espèce la plus courante du continent de Cardia, habitant à l'origine dans les climats plus frais ou montagneux. Ces herbivores sont plus fort physiquement en règle général que les Lierbil, ayant une taille moyenne (1m70), et une ossature plus forte. Ils sont vu par les clichés comme "banals". Ils disposent souvent de cornes, et une grande parties des membres, notamment venant des zones les plus froides, ont également une couche de laine.


## Raggins

Les Raggins sont une espèce venant originairement du continent d'Anvêbres, mais ils se sont retrouvé un peu partout comme toutes les espèces. Ces petits carnivores font environs 1m55 en moyenne, sont réputé plutôt faibles mais ayant l'agilité pour compenser. Les clichés les vois comme rusé, fourbes, manipulateur, voir quelque peu sadiques. Cette réputation à notamment donnée la croyance qu'ils seraient plus nombreux dans la ville de Maupertuis, croyance cependant erronée.

Ils ont une forme svèlte, des oreilles pointues, parfois des motifs de rayures sur leur fourrure et des griffes particulièrement tranchantes. Leur queue peut être touffue (plutôt comme un renard chez nous) ou longue suivant les sous espèces.


## Canensis

Les Canensis sont une espèce de carnivore de taille moyenne (1m70), provenant originellement des plaines de Io. Doté d'une bonne force, ils sont l'espèce de carnivore la plus nombreuse. Ils sont souvent représenté comme loyal, honorables, protecteur… mais parfois aussi comme stupide et facile à berner. Les canensis ont des museaux généralement moins fin que les Raggins, et des oreilles plus ou moins grande tombantes. Leur queue peut être de tout types. Leurs griffes ne sont pas rétractiles.

Ils sont très présents notamment dans les armées et chez les gardes, et sont nombreux parmi la noblesse Aymonnienne. La lignée des roi d'Aymon est d'ailleurs Canensis.

## Leönth (fauve, ours)

Les lëonth sont une espèce de très grands carnivores (1m90 en moyenne), d'une grande forme et d'une grande prestance, provenant à l'origine du sud à la fois d'Anvêbres et d'Etherial. Ils sont considéré comme fort, fier, nobles, mais également arrogant, prones à la colère et violents. Ils sont une espèce grande, généralement naturellement musclée, avec des oreilles arrondies, leur queue pouvant être soit dotée d'une touffe (ceux originaires d'Anvêmbre), soit juste une petite queue arrondie. Certains peuvent être doté de grandes crinières, notamment chez hommes. Leurs crocs et leurs griffes sont puissantes.

Les grandes familles nobles sont souvent Leönth, et la famille royale Skelfing est notamment Leönth

## Gigantiens (éléphant, rhinocéros)

Les Gigantiens sont le plus grand de tout les peuples (de loin), faisant en moyenne 2m30. Ils viennent en grande partie du royaume d'Alcofribas, royaume où il sont le plus nombreux. Cette espèce est considérée comme sage, cultivé, intelligent mais également méprisant, isolationniste. Ils sont souvent vu également comme très spirituels et religieux. Au niveau de l'apparence, ils n'ont pas de fourrure, mais une peau épaisse, difficile à percer. Leurs oreilles varient de taille, pouvant aller de toutes petites à très grandes, ils ont des extrémité de membres épais, et peuvent être dôté de défense et/ou de jusqu'à deux cornes frontales.

Si les Gigantiens sont peu nombreux, ils sont cependant une force à en surtout pas sous-estimer.

## Thalasiens

Les Thalasiens sont les espèces qui vivent dans la mer, ces espèces ayant la capacité de respirer sous l'eau aussi bien que dans l'air. Les Thalasiens sont divisé généralement en deux, entre ceux des rivières (vivant plutôt dans les eaux douces) et ceux des mers (préférant l'eau salée). Cependant, l'eau salée n'est pas mortelle pour les thalasiens des rivières, et inversement. Ils sont réputé comme étant sauvage, imprévisible, et épris de liberté. Iels ont généralement de grandes nageoires, et une peau lisse.

## Drakh

Les Drakh sont une espèce de reptiles, de taille très variables, présents sur tout les continents. Ils sont l'espèce la plus courrante et diversifié d'Aestrus. Ils ont la réputation d'être chaotiques, moqueurs et mystérieux. Cependant, c'est plutôt leurs caractéristiques qui créé les réputations. En effet, les drakh peuvent être pourvu de cornes, ailes et/ou carapaces. Ils peuvent cracher du venin, parfois inflammable. Leurs grande diversité en font. Ils sont couverts d'écailles.

## Les Scyzzes

Les Scyzzes sont une petite espèce insectoïdes, faisant en moyenne dans les 1m40. Surtout présents en agonies, ils ont la réputations d'être très territoriaux et d'être particulièrement prompt à attaquer les intrus. Ils possède plusieurs paires d'yeux, deux paires de bras, parfois des antennes et/ou des ailes, mais surtout une carapaces et un exo-squelette leur conférant une force souvent incroyable pour leur tailles.

On raconte qu'ils viendraient d'ailleurs.

## Elysiens (oiseaux)

Les Elysiens sont l'espèce la plus rare des terres d'Aestrus. Venant à l'origine des terres célestes, ils sont une espèce entourée de mystère, peu ayant vécu sur les terres avant la redécouverte des terres célestes. Ils sont réputés comme étant ceux qui ont provoqué la colère des dieux, plongés dans l'hybris et la décadence. Ils ont des ailes, et sont couverts de plumes. Leur bec peut être particulièrement dangereux s'ils décident de mordres.
