# Le Styx

Le Styx, ou énergie stygiennes est la forme d'énergie qui existe dans l'univers de Æstrus. Il s'agit d'une énergie puissante et instable, qui peut cependant être rafinée en une énergie plus controlable. Cette énergie est dangereuse et difficile à contrôler de par sa nature hautement corruptrices. Utiliser l'énergie stygienne directement peut en effet corrompre le corps et l'esprit.

Il s'agit d'une variation du Paradoxe, énergie interdimensionnelle née. Sous sa forme pure, elle peut avoir deux noms : l'éclat (forme bénéfique) et l'anomie (forme néfaste). Son nom vient de la déesse Styx, dont on raconte qu'elle aurait été la créatrice de cette énergie et que sa mort serait la cause de son instabilité (théorie aujourd'hui peu appréciée des scientifiques).

## Formes naturelles

L'énergie stygienne existe sous plusieurs formes naturelles : liquide, gazeuse, ou solide. Ce sont ses différentes formes qui peuvent être utilisé sous forme d'énergie, ou rafinée pour devenir des énergies élémentaires.

Les **veines stygiennes** sont la principale source de Styx qui existe, et sont généralement exploitée par les *centrales stygiennes* et par la société Chelmos. Il s’agit de zones d’émission spontanée d'Énergie Stygienne qui existent à quelques endroits de la planète, relativement rares mais trouvable sur tout Æstrus. Il est possible de produire de une énergie propre et durable à l’aide de ces failles. Quand elles ne sont pas sur-exploité, ces failles ne sont pas trop instables.

La forme naturelle du Styx est donc utilisée principalement comme source d'énergie, mais ces centrales peuvent aussi rafiner l'énergie du Styx sous forme de « Crystaux Élementaires » (qui sont cependant bien moins puissant - mais plus controlables - que les reliques élémentaires), ou la stocker sous forme de **fragment stygiens**.

Les fragments stygiens sont la forme solide du Styx, dont la plus utilisable et transportabler. Cependant, dans les veines stygiennes pures, c'est cependant plus les **brumes stygiennes** (formes gazeuses) et **sources stygiennes** (formes liquides) qui existent.

## Energie élémentaire

Pour la rendre utilisable, l'énergie stygienne peut être rafinée sous forme d'énergie élémentaire. Elle est généralement stocké sous forme de crystaux élémentaires, des version rafinée des fragments stygiens.

Si cette énergie est plus contrôlable, elle est de fait plus spécialisée dans l'élément dont elle est rafinée, et les éléments physiques (donc très spécialisé et moins "puissant") sont plus simple à faire que ceux "métaphysiques".

Cette conversion est notamment utilisé par les membres des guildes pour la pratique de leurs techniques propres, ou pour la construction des technologies élémentaires.
