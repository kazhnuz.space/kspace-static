# Arcane de la guérison

## Les hospices de la lumière

Les hospices de la lumière sont une guilde guérisseuse nées en Thélémie. L'un des deux grandes ordres de la région Thélémienne, il occupe une grande place dans la vie du Saint-Pays, étant protecteur des personnes fragiles et des malades. Cet hospices met en avant la générosité et le don de soi. On raconte qu'ils passent une grande partie de leur apprentissage dans la guilde à apprendre, et qu'ils ne quittent le statuts d'étudiant que vers la quarantaine, même s'ils peuvent déjà soigner avant.

Fondée par la déesse **Quartz** pour protéger et soigner les habitant de la région d'une grande peste, et y empêcher la maladie de revenir. Le **fragment de la lumière** se trouve dans la plus grande Cathédrale de la Lumière, et est utilisé pour soigner toutes les pires maladies possibles.

## Les marchands d'énergie

Les marchands d'énergie sont une guilde particulière, dont le principe est les échanges et les équilibres d'énergies, notamment vitales. Le nom "marchand" ici est vu dans un sens plus large que le terme normal. Leur vision est que chaque chose s'échange, et qu'il faut le plus possible échanger ce qui peut l'être, afin d'atteindre "l'équilibre dans un système en mouvement". Cela vaut aussi bien pour les matières, que les richesses, que les flux d'énergies.

Ils sont les producteurs de l'énergie utilisée à Cardiopolis, devant même Chelmos Corporation, via leur immense générateurs d'énergie magique.

Leur quartier général se trouve dans la citadelle d'Héliodore, fondée comme la guilde par le dieu **Héliodore**. Au coeur de la base, le **fragment d'énergie** est utilisée comme la source de tout le pouvoir de la base.

## Les navigateurs de l'Eau

Les navigateurs de l'eau est une guilde-peuple unique en ce que la guilde est entièrement fusionnée à la nation qu'elle protège. Tout membre de la guilde est considéré comme faisant partie du peuple de l'eau, et tout membre du peuple de l'eau est sous la protection de la guilde. Le peuple de l'eau se considère comme appelé à naviguer et comme faisant partie intégrante de la mer. Ce peuple-guilde possède les territoire d'Ilade et Uraziel.

Leur idéologie vient de la déesse marine et navigatrice **Aigue-Marine**, qui noya son chagrin dans l'éternelle aventure qu'est la navigation. Une grande partie des membres de la guilde de l'eau est nomade entre les différents territoires et comptoirs commerciaux de la guilde de l'eau, vivant pour un idéal de liberté et d'aventure.

Le **fragment de l'eau** est offert au Grand Navigateur, tout les 5 ans lors d'un grand tournoi. Si celui-ci ne le rend pas avant le concours, on raconte que le fragment le fera sombrer dans l'eau avant de revenir naturellement à Uraziel.

## Les druides de la nature

Les druides de la nature sont une guilde décentralisé, historiquement situé sur toute la Thélémie et la région voisine de Sylvera. Cet ordre consiste en la protection de la nature, et pratique une magie grandement fondée sur la reconnaissance et l'utilisation des plantes qu'on peut y trouver. Ouverts à tous et égalitaires, sa particularité est qu'il y existe grace à la grande diversité des potions presque autant de pratiques que de membres. La tenue classique des druites de la nature sont une grande robes simple et souple, dont une partie du bas peut se raccrocher à la taille, formant une sorte de jupette. Leur idéologie est fondée sur le fait de vivre sa vie sans poser de soucis aux autres, et de "cultiver son jardin".

Cet ordre a été fondée par la déesse **Smargdite**, déesse protectrice des jeunes adultes et des forêts. On raconte qu'elle aurait fondée la guilde après avoir sauvé des jeunes filles et garçons d'un roi tyran cruel, forçant tout les enfants des rivaux à servir sa cours. Elle aurait fait du jeune prince du tyran son premier disciple, et déthrona ensuite le roi. Il n'y eu ensuite plus jamais de roi sur le territoire de Sylvera. La déesse enseigna à ses disciples le respect de la nature, teinté de méfiance envers ses caprices. Elle apprend à être humble comme manière d'être libre. Et à aimer les jupettes.

Le **fragment de la nature** se trouverait caché dans un temple, dans la forêt de Sylvera.
