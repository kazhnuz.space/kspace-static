# Les éléments du combat

Les quatre éléments de l'arcane du combat sont des éléments dont les dieux sont considéré avant tout comme des combattant exceptionnels, pour une raison ou une autre. Les guildes inspirées de ces éléments sont donc souvent considéré comme étant plus dangereux.

## Les disciple du Néant

> Alignement : DPS/Soigneur

Les disciples du Néant sont une guilde se situant à Sépulcre, cherchant en découvrir toujours plus sur le néant et ses secrets. Cette guilde est cependant également très méfiante de ses dangers, et reste en retrait. Cette guilde d'érudit mettent la connaissance en avant, mais également le danger de cette connaissance. La prudence est la qualité ultime de l'érudit selon eux... cependant tous ne le sont pas toujours.

Ils tiennent leur idéologie et leurs pouvoir de la déesse **Obsidienne**, qui a déjà plongé dans le Néant et qui connait ses dangers. Le **fragment du Néant** est scellé sous la guilde, interdit de toute utilisation.

## Les arquebusier de la Foudre

> Alignement : DPS²

Les arquebusier sont une guilde combattante d'Alandale, expert des armes à feu, et armes à distances en règle générale. Cette guilde est une guilde de mercenaires et chasseurs de primes, offrant leur services pour rattraper les bandits, ou régler des soucis personnels. Ils sont cependant aussi nombreux à être employé par le Magister ou Alandales.

Leur idéologie est fondée sur l'indépendance et le fait de ne devoir obéïr à aucun seigneur. On dit souvent que les arquebusier se veulent être "un peu moins cruel que le monde, surtout si ça rapporte". Leur vision est une forme nihilisme : le monde n'a pas de sens, mais on a la force de le faire évoluer par notre *volonté de changer*, idéologie venant de leur fondatrice, la déesse **Zircon**.

Le **fragment de foudre** est loué à l'armée Alandalienne.

## Les chevaliers des Flammes

> Alignement : DPS/Trickster

Les chevalier des flammes sont un ordre combattant de la ville de Byrell, suivant la discipline guerrière du dieu Grenat. Cette armée est souvent considérée comme moins organisée que les *soldats de la terre* voisins. Ils combattent généralement à l'aide l'armee qu'ils enflamment, et leur stratégie consiste souvent à foncer dans le tas.

**Grenat**, dieu impulsif, colérique mais fier, enseigna aux mortels le courage, l'honneur. Pour lui, il y a deux type de combat : le combat juste, et la juste guerre. Le premier est le duel : on raconte des combattant des flammes qu'ils ne fuient jamais un duel honorable, et qu'ils n'attaquent jamais une ennemi à terre. Le second révèle une réalité plus violente : lorsqu'une guerre est juste, les chevaliers des flammes veulent l'effectuer jusqu'au bout, et refuse l'abandon. On dit souvent que les chevaliers du feu sont imbus d'eux-même, mais peuvent se révéler juste le moment venu. Cependant... cela dépend beaucoup de leur vision de la justice.

Le **fragment de flamme**, relique de la guilde des flammes, est toujours portée par le membres considéré comme le plus fort de la guilde. Il a cependant l'interdiction totale d'être chef de la guilde également.

## Les rodeurs des Ombres

> Alignement : DPS/Trickster

Les rodeurs de l'ombre sont une guilde de rodeur, brigands et voleurs dont les quartiers généraux se trouve à Katra, mégapole d'Agonie. Leur vision du monde est assez simple : le monde ne nous fait pas de cadeau, et les gens qui possèdent le possèdent souvent de manière injuste, ou en tout cas grâce à un système injuste. De ce fait, ils estiment leurs activités, souvent essentielles à leur survie, comme étant un "mal nécessaire". Ils ne se voient pas comme des "bonnes personnes", mais comme une partie nécessaire du monde.

Plus qu'une "mafia", cette guilde est un réseau d'entraide pour petit voleurs, avec tout de même un code d'honneur : pas d'attaque envers les enfants, éviter de tuer sauf si nécessaire. Tuer un tyran peut cependant être parfaitement acceptable. Cette guilde accueille souvent ceux dans le besoin.

Leur idéologie vient du dieu **Jais**, qui a construit sa guilde sur le continent Agoniens après avoir fuis Cardia à cause d'une sombre histoire avec son frère Heliodore, et une halebarde d'énergie. Le **fragment de l'ombre** est quelque part dans le monde, passant souvet de voleurs en voleurs. On en trouve régulièrement des traces dans des faits divers.
