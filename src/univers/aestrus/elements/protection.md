# Arcane de la protection

## Les paladins de l'ordre

> Alignement : Tank/Healer

Les paladins de l'ordre sont une guilde suivant les enseignement de la déesse **Agate**. Déesse stricte et vertueuse, elle refuse tout écart moral. Les membres de la guilde sont considéré comme devant rester noble de corps comme d'âme, tout laissé allé étant considéré comme un pécher. Tout ce qui est corruption, le fait de recevoir des aventages non-mérité est vu comme la pire des trahisons ! Cela ne veut cependant pas dire que ces choses n'existent pas dans la guilde... Mais ils le refusent.

Ils sont en grande partie dévoué à la protection du peuple Aymonnien, même si de nombreux rejoignent les rangs des troupes radiantes. Ils ont la particularité d'avoir de nombreuses petites guildes locales partout dans le royaume d'Aymonn, même dans le duchée Bymanniens.

Le **fragment de l'ordre** est la propriété du chef de l'ordre, et se passe de chef en chef.

## Les auriges du Metal

> Alignement : Tank/DPS

Les aurige du métal sont une guilde installée sur l'île riche en minéraux d'Énoa, célèbre pour leur maîtrises des chars, puis des véhicules mautorisés. Ils sont également cependant des experts en maniement des armes, armures et outils, et sont également des grands défenseurs de l'évolution technologiques. La guilde encourage la curiosité intellectuelle, et le pragmatisme : mieux vaut se faire attaquer en armure et avec cinquante armes que nu avec juste son courage et sa foi en son destin.

Ils tiennent leurs idéaux de la déesse **Fer**, qui fut une forgeronne experts qui selon la légende à inventé beaucoup d'armes, d'amrures et d'objet du quotidien.

Le **fragment de fer** est une partie intégrante de la Forge d'Énoa.

## Les soldats de la terre

> Alignement : Tank²

Les soldats de la terre est une armée de la ville de Byrell, organisée et méthodique. Ils sont avent tout des protecteurs, notamment de la ville. Là où les soldats du feu attaquent, ils protègent. Grands experts des armures et des boucliers, leur méthode est souvent d'empêcher toute percée ennemi, et de pouvoir tenir le siège même le plus violent.

L'idélogie de la Terre est celle du dieu **Silice**, dieu protecteur et rationnel. Il est le grand rival de Grenat, rivalité qui se reproduit entre les guildes, et son opposé total. Pour Silice, la guerre n'est pas un art mais une science. Il n'y a pas de justice dedans : juste des intérêts à protéger et défendre. Les soldats de la Terre ont pour objectif premier la protection, et ont comme idéal de n'être que des rouages dans une mécanique de protection parfaite.

La légende raconte cependant que Silice n'est pas sans sentiment : la guilde de la Terre aurait été fondée dans Byrell par pure rivalité avec la guilde du feu.

Cette guilde est connue notamment pour avoir été la guilde de Fyrst Skelfing, fondateur de la dynastie Skelfing, qui aurait pris le contrôle de la ville de Byrell. Le **fragment de la Terre** est d'ailleurs la propriété actuelle du clan Skelfing, qui l'a récupéré lors de la prise de Byrell, et qui désormais l'utiliser pour entrainer ses troupes.

## Les gardiens du temps

> Alignement : Tank/Trikster

Les gardiens du temps sont une caste de gardiens, vivant dans la province Aymonnienne de Bymann, notamment dans la ville de Sophale. Cette ancienne caste protège les Ruines du Temps, et ont pour rôle d'empêcher quiconque de s'intéresser à leur pouvoir. Ils sont également les gardiens du **fragments du temps**, le rendant inaccessible à tous.

Ils tiennent leur pensée du dieu du temps, **Ambre**, qui a fondé cette guilde afin de protéger les pouvoirs qu'il avait créé en ce monde. Leur idéologie est fondée sur la compréhension du temps, et sur le fait que l'ont doit accépter les mouvements du temps, qui ne sont pas sous notre contrôle. La vieillesse et le hasard sont des choses qu'on ne contrôle pas, et le mortel doit se concentrer sur ce qu'il peut contrôler - ici protéger ce qui est important. Ils mettent en avant la capacité à garder des secrets et à gardé caché ce qui doit l'être.
