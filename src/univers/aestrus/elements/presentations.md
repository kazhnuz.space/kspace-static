# Les éléments

Les éléments sont l'une des notions les plus fondamentales du monde d'Æstrus. Il existe 16 éléments principaux, divisant le monde en principes représentant l'entiereté de l'existences.

D'un côté existent huits éléments physiques (eau, glace, air, foudre, feu, metal, terre, vegetation), représentant la matière et ce qu'elle peut former. De l'autre se trouvent les huit éléments métaphysique (néant, espace, ordre, lumière, énergie, temps, chaos, ombres), décrivant la réalité même.

## Élément et arcanes.

Ces éléments sont également trié par *arcanes*, représentant le principe fondateur de l'élément, s'il est centré sur le soin (arcane de la guérison), sur l'attaque (arcane du combat), la défense (arcane de la protection) ou sur ses propres principes (arcane de la malice).

(Note : la ligne verticale représente l'arcade principale (et donc dans quelle catégorie vous pourrez les retrouver), et celle horizontale l'arcane secondaire)

|  | Guérison | Combat | Protection | Malice |
|:-|:--------:|:------:|:----------:|:------:|
| **Guérison** | Lumière | Néant | Ordre | Air |
| **Combat** | Energie | Foudre | Metal | Glace |
| **Protection** | Eau | Feu | Terre | Espace |
| **Malice** | Nature | Ombre | Temps | Chaos |

## Les reliques élémentaires

Les reliques élémentaires sont des sortes de disques de métal, qui offrent un pouvoir élémentaire considérable à la personne les utilisant, tout en étant assez dangereux pour la santé si utilisé trop longtemps. On dit qu'ils offrent le pouvoir de maîtriser dans une certaine mesure l'élément lui-même, au point ou leur utilisation pouvait renverser des guerres entières.

Ils sont tous gravé du symbole de l'élément, et sont dit venir des dieux eux-même (d'où leur nom de relique).

Les reliques sont au nombre de 16, une par élément.

## Les guildes

Les guilde sont 16 groupes de suiveurs des dieux des éléments, qui se faisait originellement la guerre avant la fondation de l'Église d'Alicia pour leur réunir et en faire des alliés. Si les tensions peuvent rester, aujourd'hui les guildes travails ensemble, généralement étroitement avec le Magister.
