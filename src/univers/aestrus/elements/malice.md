# Arcane de la Malice

## Les danseurs de l'Air

> Alignement : Trickster/Soigneur

Les danseurs de l'air sont une guilde artistique, qui vise avant tout l'art et la beauté. Le chant et la danse de cette guilde sont parmi les plus divers du monde, puisqu'ils invitent toutes personnes de tout peuple à rentrer dans la guilde pour enrichir son art. Pour cette guilde, le premier outil pour l'art est son propre corps, le premier instrument de musique sa propre voix... mais ils ne sont jamais contre les embellir avec des tenues resplendissantes ou des instruments de musiques. Pour eux, l'art est soin de l'âme mais aussi du corps.

À Maupertuis, cette guilde admet de nombreux Oikeus "laissé de côté" dans leur rangs, leur offrant de nouvelles perspective quand le sort les met dans une positions infortunes.

Le chef de la guilde est le dieu **Turquoise**, connu pour son androgynie et sa beauté. On raconte qu'il aurait volontairement prit à sa demande l'apparence d'une danseuse mortelle décédée afin de préserver son art et de lui permettre de "continuer à exister", formant ainsi la guilde des danseurs de l'air. Il est également le dieu qui invite à briser les frontières et les tabous inutiles, afin de vivre sa vie pleinement et de la manière la plus artistique aussi.

Le **fragment du vent** est généralement légué à l'armée Maupertuisienne en échange de la tranquilité de la guilde.

## Les mages des Glaces

> Alignement : Trickster/DPS

Les mages des glaces sont des experts en *magies offensives*, vivant dans la ville cachée d'Alsfar dans les montagnes. Ils sont expert à la fois en l'utilisation d'armes de glaces, d'attaques à distance et surtout de magie servant à affaiblir ou rendre malade l'ennemi. Leur idéologie est une idéologie austère. Pour eux, il faut apprécier le peu qu'on peut trouver, et le chérir. Nos relations, la nourriture à notre table, sont déjà une grande chance, et vouloir trop serait une mauvaise chose. Pour eux, le combat ne doit se produire qu'en cas de dernier recours, et doit être bref mais efficace. Les mages des Glaces ne sont cependant souvent pas très résistants.

Ils tiennent leur idéologie du dieu **Perle** qui s'est isolé dans les montagnes de Thermona et fondé la ville d'Alsfar pour y vivre loin de la douleur de la mort de Styx. Le **fragment des glaces** n'est utilisé par la guilde qu'en cas d'extrème urgence.

## Les astronomes de l'Espace

> Alignement : Trickster/Tank

Les astronomes de l'espace sont une très ancienne guilde d'érudit vivant à Tuhan. Pour les membres de cette guilde, il est important de sortir de ses préjuger et de partir à la recherche du Vrai. Cette recherche se fait par l'observation et les tentatives de comprendre le monde.

Guilde scientifique, et très portée sur la recherche, elle est souvent vu comme étant moins combattante que les autres... mais peuvent se battre si nécessaire, et surtout tenir longtemps un siège. Ils peuvent fausser l'espace et modifier les perceptions, en faisant d'excellent causeur de trouble chez leurs ennemis.

Ils tiennent idéologie et pouvoir de la déesse **Opale**, qui fonda le Grand Observatoire de Tuhan, dédier à la découverte du monde. Personne en dehors de la guilde ne sait ce qu'est devenu le **fragment d'espace**.

## L'ordre du Chaos

> Alignement : Trickster²

L'Ordre du Chaos est une belle bande de casseur de pieds. Cette guilde a été fondée en Bicéphalie, connue pour ces disputes entre ses deux rois, juste parce que "c'est le pays le plus fun". Considérés comme étant irresponsables, ne pensant qu'à s'amuser, quelques membres de la guilde du Chaos revendique cet état de fait... mais ils sont en fait minoritaires. Beaucoup trouve un aspect plus sérieux dans la maîtrise du Chaos, tel que le contrôle de sa propre vie, où dans un aspect plus "punk", le refus de l'autorité.

En tout cas, ce n'est pas l'avis de la déesse du Chaos, **Rhodonite**. Trickster notoire, elle est connue pour ses nombreux tours fait à ses frères et sœurs, les ayant parfois fortement irrités.

Le **fragment du chaos** est perdu depuis des générations.
