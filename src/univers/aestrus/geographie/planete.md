# La planète Æstrus

La planète Æstrus est une petite planète, composée de deux continent : le continent de Cardia et celui plus petit d'Agonie. Le climat est tempéré, proche de celui de notre Terre, mais est composé de nombreux paysage incroyable et plus "fantasistes".

![Planète Æstrus](aestrus.png)

Si le continent de Cardia est considéré souvent comme une seule entités - notamment opposée à l'agonie qui est une immense île unique - elle est composée de trois sous-continent dans les faits radicalement différents (tous relié par une immense chaîne de montagne au nord) :

- Le sous-continent d'Io

- Le sous-continent d'Etherial

- Le sous-continent d'Anvêbre

Existent aussi les Cieux, un archipel aérien aux iles éparses étalée dans toute l'athmosphère du globe, plein de technologies anciennes et inconnue, datant de l'age d'or des Elysiens, qui commence à être exploré voir colonisé par le magister en XXXX.
