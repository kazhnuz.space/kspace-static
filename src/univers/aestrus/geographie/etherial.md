# Le sous-continent d'Etherial

Le sous-continent d'Etherial est un continent tempéré et fortement boisée, avec de nombreuses plaines. Elle a été le cœur de la Hiérocratie Aztlann durant l'ère pré-classique, dont les ruines existent partout dans Etherial, jusque dans les montagnes thermoniennes.

## Territoire de Tuhan

Cœur à l'époque pré-classique de l'empire Aztlann, devenu bien plus tard l'un des Trois-Duchés dirigé par la dynastie Skelfing qui formera ensuite l'Empire Skelfing (Avec le Duchée de Bymann et la Cité-État de Bihrel), grand-centre du Culte des Anges. C'est une région qui a perdu une grande partie de son importance après la fin de la Hierocratie, et qui est devenu un espace peu développé, et connu surtout pour ses ruines. Grandement aride et secoué par des vents violents, c'est un lieu désormais relativement pauvre, composé de petit territoires locaux, sous la protection du Magister.

Tuhan est l'une des seuls endroit ou se trouve des ruines de la Civilisation Céleste Elysienne, puisqu'un temple de l'envol s'y trouve. Conservé par les différentes dynastie qui s'y trouve comme un lieu d'expiation des fautes des mortels, il est de ce fait relativement préservé par a considération comme un lieu dont il fallait garder l'aspect parjure comme "témoignage de notre laideur".

## Péninsule des Cités-Etats

La péninsule des Cités États est une ancienne dépendance Aztlannienne qui s'est transformé petit à petit en un espace dirigé par un certain nombre de cités-état plus ou moins rivales.

Les trois principales sont Byrell, Maupertuis et Sépulcre

### La république de Maupertuis

La république de Maupertuis est la plus riche et la plus puissante des cités états de la péninsule. C'est également la cité états possédant le plus de cités vassales, ainsi qu'un empire économique puissant.

Dirigée par un Capitaine et fortement influencée par l'oligarchie de la ville, Maupertuis compense son armée faible par des formidables agences d'espionnages, auquels les autres pays font régulièrement appels. Tout le monde sait qu’ils avaient forcément des espions de Maupertuis quelque part dans leur territoire, mais personne ne sait qui ils sont.

La république est également tristement connue pour pratiquer une forme de servitude plus ou moins payée, les Oikeus. Ce sont des êtres qui, généralement vendue par leur famille à la suite de dette, subissent un status de serviteurs. Le sortilège les transformant en Oikeus, situé dans un tatouage sur le front, a différents effets : stérilité, incapacité à s'énerver où à se mettre en colère de manière violente, à ressentir des émotions négative envers son maître, ou de la jalousie ou possessivité de manière générale, besoin de sommeil amoindri et capacité de concentration accrue. Le sortilège est souvent décrit comme "redirigeant les énergies lié aux pulsions vers plus constructifs".

Ils sont généralement utilisé dans les milieux où il y a peur de la formation de dynastie, des révoltes. Ils sont très utilisé en tant que gardiens du savoir dans la ville de Sépulcre.

Maupertuis suis le culte d'Alicia, et possède comme guilde XXXXX.

## La Sapiocratie de Sépulcre

La sapiocratie de Sépulcre est une cité état, grandement dirigée par une aristocratie lié au niveau d'étude, dirigé par un conseil des sages. La ville décrète la sagesse comme condition la plus importante de l'humain, et l'unique moyen d'atteindre le bonheur. La contemplation philosophique et le débat sont érigée en chose la plus importante.

Le nom de "sépulcre" désigne une réalité cependant plus sombre : cette ville serait née de la mort.

À sépulcre, le savoir est pouvoir. C'est pourquoi les gardiens de la bibliothèque de Sépulcre sont des Oikeus,

## La Cité de Byrell

La cité de Byrell est une cité état guerrière et fortement militarisée, dirigée par un conseils de militaire venant des deux guildes de la ville : le **Feu** et la **Terre**. La rivalité entre ces deux guildes guerrière a façonnée toute l'histoire idéologique de la ville, qui a toujours hésité entre l'idéal héroïque du feu, ou la protection rationnel de la terre.

La ville est une ville pétrie d'honneur, où généralement il y a peu de place pour ceux qui ne veulent pas se battre. Tout les membres de la ville doivent participer au combat et savoir manier les armes.

Première cité état de la péninsule à avoir pris son indépendance de la Hiérocratie, par l'action du soldat Fyrst Skelfing, elle est rapidement devenu une pierre angulaire de l'Empire Skelfing. Après la chute de l'Empire, la ville - qui jouissait déjà d'une forte autonomie - est devenue indépendante.

## Terres de Sylvera

### Foret mystique de Sylvera

Cette immense forêt, où résonne une immense énergie mystique. Cette forêt semble s'être constituée durant l'ère pré-classique, parce qu'elle recouvre de nombreuse ruines de la Hiérocratie Aztlann, ce qui laisse supposé qu'elles ont existé avant la fin de l'Empire. Peu de trace existe de l'apparition de cette forêt, même dans les territoires voisins.

Cette foret est l'endroit où se cache le peuple Sidh, peuple du poison, l'un des *quatres peuples oubliés*.

## Archipel d'Ilade

L'archipel d'Ilade est un territoire qui a été conquis par le *Peuple de l'Eau* à la fin de l'ère classique. Moins sous contrôle que la capitale, l'archipel subit beaucoup de piraterie.

### Ilade

L'ile principale. Si elle est sous contrôle du Peuple de l'Eau, cette île est en vérité hautement multiculturelle. Il s'agit d'une île fortement forestière, avec deux villes principale Ilade et Estia. Cette île est fortement surveillée par la guilde de l'Eau, et est surtout connue pour être l'une des plus grandes forces commerciales avec Maupertuis.

### L'île spectrale

Ancien territoire sacré où vivait assez inexplicablement quelques membres du Troisième Oeil, l'île est désormais une île déserte après l'incident d'une centrale stygienne. Cette île est sous protection du Magister, bien que des pillards s'y rende régulièrement.

C'est a proximité de cette île que se trouve la forteresse maritime de Qader, une des grandes bases marines du Magister.

### Iskaia

Iskaia est sans doute la plus connue des îles d'Ilade, à part peut-être l'île éponyme. Pourquoi est-elle connue ? Parce qu'elle est la plus grande île de la piraterie ! En effet, c'est sur cette île - et l'archipel éponyme, composé en grande partie d'île couvertes de mangrove - que se retrouvent une grande partie des pirate de l'archipel.

Îles au fort taux de criminalité et de pauvreté, Izkaïa est la plus grande des épines dans le pied du véritable empire que forme de peuple de l'Eau.

### Amëia.

Amëia est une petite île balnéaire, connue à la fois pour ses plages et pour son architecture ancienne extrèmement bien conservée.

Elle est la plus grande des îles de l'archipel de Lupaia, un archipel rempli de ruines anciennes. Les plus ancienne sont les *ruines de la louve rouge*, connue pour la "malédiction" qui s'y trouverait.

## Plaines de Thélèmie

Le Saint-Pays de Thélèmie  est un regroupement de duchés semi-indépendant, qui élisent un Saint-Duc à chaque mort du précédant. Pays le plus pieux, suivant l'Eglise d'Alicia, c'est également un très grand territoire où la technologie à difficilement avancée, notamment dans les grandes plaines de l'île.

Composé de grande plaines, la Thélémie possède un grand nombre de petit village, et plusieurs grande villes.

- Ranvède est la capitale religieuse du pays, une grande ville religieuse, surtout habité par le clergé Thelemiens. Si ce n'est pas la capitale de la religion d'Alicia, pour les Thélèmiens, c'est tout comme.

- Drianne est la capitale politique et économique du pays. Grande ville maritime, cette ville est composée d'une ancienne ville, où se trouve tout les centres d'influences politiques du pays, et la nouvelle ville où se trouve tout les centres d'influences économiques. Drianne est entourée d'une mégapole immense, l'une des plus grandes du monde.

Drianne et Ranvède étant assez proche, d'autres villes secondaires, telles que Naone et Braade, s'occupent de tenter de dynamiser les autres territoire thélèmiens.

## La Bicéphalie

La bicéphalie est un pays connu pour le désordre politique constante qui y règne. Son nom vient de son système d'héritage complet : en effet, la Bicéphalie possède des loi faisant qu'il y a toujours deux souverains simultanément sur le territoire bicéphalien.

Officiellement, cette loi devrait permettre une gouvernance plus sage, la compétition des deux souverains les forçant à avoir les grâces du peuples en faisant le plus possible, à être honnêtes et travailleurs afin d'être le plus appréciés... Dans les faits, cette compétitions les a rendu prêt à tout pour gagner, faisant les pires crasses, trahisons, manipulations. L'histoire bicéphaliennes est pleines de coups bas d'un souverains sur l'autre.

Cependant, depuis l'ère contemporaine, le passage à la démocratie parlementaire à diminué fortement l'influence et le pouvoir des deux souverains... cela n'empêche pas la rivalité de continuer à exister.

Assez peu surprenamment, c'est ici que l'Ordre du Chaos à décidé de s'installer, parce que c'est le pays où tout peux changer le plus vite.
