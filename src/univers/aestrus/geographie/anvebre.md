# Le sous-continent d'Anvêbres

Le sous-continent d'Anvêbres est un sous continent à l'ouest des plaines d'Io, au climat plutôt chaud, allant du sec (au centre du continent) au tropical (sur les côtes sud). C'est sur ce continent que se trouve les deux principaux centres technologiques d'Æstrus, la capitale du Magister, et l'île d'Enoa, grand centre d'innovation technologiques. Les deux grandes nations du sud d'Anvêbres (Andale et Alcofribas) sont indépendante du Magsiter.

## Plateau de Kab

Les plateau de Kab sont des grands plateau semi-aride où peu de chose pousses, formant le territoire de Kab. Ces terres sont surtout connus pour les citadelles qui s'y trouvent, et pour être l'endroit où l'on peut rencontrer.

### Cité de Cardiopolis

La Cité de Cardiopolis est une ville qui a été inventée pour être la capitale du Magister. Ville entièrement dirigée par sa mairie, elle ne fait partie d'aucun pays si ce n'est le Magister, dont elle doit respecter strictement les règles. Ville moderne et très dense, elle a eut le droit à une immigration très forte, ce qui lui a permis de se constituer en ville d'importance considérable, avec un dynamisme incroyable.

Pour certains ville dynamique, vivante et source d'espoir, pour d'autre il s'agit d'une ville sans aucune histoire, et qui ne sert que les intérêt de Magister. Elle ne possède pas de guilde.

Cette ville ne possède pas de guilde, mais de nombreux centres de recherches et de travaux sponsorisé par le Magister. C'est ici que se trouve la Chelmos Corporation, ainsi que le quartier générale des Troupes Radiantes. C'est aussi ici que se trouve le Palais Magistral, véritable centre du gouvernement Magisteriens.

### Citadelle d'Héliodore

Cette citadelle est une des nombreuses petites citadelles du plateau de Kab. L'unique vrai particularité de cette citadelle est sa position en tant que lieu ou se trouve la guilde de l'énergie. Cette citadelle est en grande partie sous contrôle de la Guilde.

## La seigneurie d’Alandale

La seigneurie d’Alandale est l’un des deux états du Magister à ne pas suivre le culte d’Alicia. Ils célèbrent plutôt le Grand-Prophete, un Gigantien originaire du royaume voisin d'Alcofribas, disciple de rescapés des Cieux ayant fondé la seigneurie qui a cherché à partager en dernier les secrets du monde, et établi la thèse de la souffrance de Styx.

Cette seigneurie a été envahie par l'Empire Skelfing, mais a été libérée par le royaume d'Alcofribas. Cependant, depuis cela, ils tentent de renforcer leurs propres qualités militaires, histoire de ne plus être dépendant du royaume voisin. La guilde de la foudre y joue un rôle important, se trouvant à sa capitale éponyme, Alandale.

Avec l’Ancien-Royaume, ils font partie des entité politique à ne pas faire partie du Magister.

### Citée fortifiée de Tubleh

La citée fortifiée de Tubleh est un endroit unique sur Æstrus pour plusieurs raison. Tout d'abord, cette forteresse est en réalité un état indépendant qui existe depuis l'ère pré-classique, mais elle est également constitué du peuple oublié éponyme. Ce peuple n'est en aucun cas en rivalité avec la seigneurie, cependant. De nombreux tublehtohl sont engagée comme garde ou comme membres éminents de l'armée Alandalienne.

C'est le peuple "oublié" le plus connu de ce fait. La cité reste cependant en grande partie interdite aux non-tublehtohl. Quant à la cours royale, elle est interdite à toutes autres personnes que la famille royale et leurs serviteurs, afin d'évité toute possibilité de tentation de prise de pouvoir et d'établissement d'autre dynastie.

### Désert de Zircon

Ce désert est selon la légende là où Zircon, soeur de la défunte Styx, à déchainée toute sa colère et sa tristesse. Cette grande étendue désertique subit des orages secs perpetuels, le rendant très difficilement praticable. C'est au centre de ce désert que se trouve la Relique de la Foudre.

## L’Ancien-Royaume d’Alcofribas

L'Ancien-Royaume d'Alcofribas est singulier de nombreuses manières. C'est le seule territoire ancien de Cardia qui n'est relié à aucune guilde, mais du coup il les accepte tous. Cependant, malgré cela, Alcofribas est un royaume fier et avec un fort sens de l'identité. Il n'a jamais été envahi, et son territoire n'a jamais changé depuis l'ère pré-classique : en effet, il est aussi anciens que la Hiérocratie. Il a combattu tout les grands empires qui se sont porté à ses frontières, mais reste majoritairement pacifiste.

Ce royaume australe est puissant et à la particularité d’être à la fois isolationiste diplomatiquement, mais de former des alliances protectrices avec les autres états. C’est ce royaume qui a empêché jadis l’empire Skelfing de tout envahir son sous-continent, et qui a notamment libéré la seigneurie Alandale. Pour beaucoup d'autres région, son ancienneté et ses différences avec ce qu'ils connaissent en fait une terre auréolée de mystère.

Cette terre est à l'origine du culte du grand prophète, qui serait "tombé de nul-part" et aurait enseigné la souffrance né de la mort de la déesse Styx.

### Jungle de Daral

La jungle de Daral est une jungle épaisse qui recouvre tout l'ouest d’Alcofribas. Cette région d'Alcofribas contient de nombreuses tribues plus nomade que dans les villes. La jungle est cependant en grande partie un sanctuaire, au centre se trouvant le temple qui sert de tombeau au grand-prophète.

## L'Ile d'Ënoa

Aussi nommée *île du fer*, l'île d'Ënoa est une grande île située à l'est du royaume d'Alandale. Cette île était surtout prisée pour ses mines de métaux, et pour ses nombreuses veines stygiennes. C'est ici que s'est former la guilde du Métal. Cette ville a considérablement grimpée en importance lors de la révolution industrielle, et ce encore plus lors de la découverte des Centrales Stygiennes.

Sa capitale et plus grande ville est nommée Fer, en hommage à la déesse éponyme, et est la ville considérée comme la plus technologiquement avancée de tout Æstrus.

Anciennement comptoir commercial de Maupertuis, elle est devenu un pays indépendant sous le Magister.
