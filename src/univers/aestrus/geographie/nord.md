# Nord de Cardia

Le "Nord de Cardia" est le non généralement donnée à un ensemble de zones au nord de Cardia. Ces zones sont l'immense chaine de montagne de Thermona, quelques îles, et le sous-continent d'Io.

## Les montagnes de Thermona

La chaine de montagne de Thermona est une immense chaine de montagne qui recouvre le nord des deux plus grands sous-continent de Cardia, et les réunies. Recouvert par une immense calotte glacière, cette chaine de montagne est un lieu extrèmement difficile à vivre, où très peu de peuple se trouve. Il descend sous forme de haut plateaux dans les continents d'Anvêbres et d'Etherial, formant un lieu plus vivable, mais ou se trouve en grande majorité des peuples nomades.

Cependant, on y trouve quelques villes, en grande partie descendant de l'époque de la grande hiérocratie de l'époque pré-classique. Le Consulat d'Alsfar, en grande partie dirigé depuis la ville souterraine d'Arak'nur situé dans les montagnes, est une forme de nation réunissant les nombreux peuples nomades des steppes. Malgré l'existence de ce Consulat, le territoire est administré par le Magister, sous forme des Territoires Non-Organisés de Thermona, et le Magister y possède un certain nombre de base militaire et/ou de recherche.

La prison d'Oxyda s'y trouve, non loin des ruines Aztlannienne du même nom.

## Ile de Septentrion / L'Empire Skelfing

L'île de Septentrion est le dernier vestige de l'ancien empire Skelfing, un puissant empire né durant l'époque post-classique (de la réunion de trois territoires dirigé par la famille Skelfing), et ayant envahi presque la totalité du monde durant cette ère. Après la guerre. Après avoir été défait, il a réussi à subsisté sur cette île, qui était avant un petit gouvernat de l'Empire (et encore avant une colonie du Duchée de Bymann). Aujourd'hui, l'Empire Skelfing est encore indépendant du Magister, et n'a pas véritablement les forces de l'attaquer.

Le Magister se méfie cependant encore beaucoup de lui. Cet empire est l'un des territoire à encontre suivre la religion de XXXX et à ne pas s'être converti au culte d'Alicia, tout en ayant une forte présence de guildes. Les instances locales des guildes sur le territoire Skelfing n'y obéïssent pas à l'église d'Alicia, mais directement à l'empereur.

La capitale de l'empire est Fyrst, cependant la famille impérial n'y vit pas, vivant plutôt à cause de l'incident de l'assassinat d'Hetjul (évenement ayant conduit progressivement à la chute de l'Empire) dans la Citadelle Skelfing dans les terres gelées de Septentrion.

### La famille Skelfing

La famille Skelfing est une ancienne famille noble de Leönth, provenant à l'origine de l'Empire Aztlann. Fyrst Skelfing, fondateur de la dynastie, est connu pour avoir pu s'emparer de la Relique de la Terre (qui est toujours aujourd'hui la propriété de la famille Skelfing), ce qui en fait une des unique relique active et connue, qu'il a utilisé ainsi qu'une petite armée afin d'envahir la ville Aztlannienne de Bihrel.

La famille utilise majoritairement la magie de la Terre, et ont tous une certaine capacité à utiliser la relique.

## Le territoire maritime d'Uraziel

Le territoire maritime d'Uraziel est un petit territoire costale situé vers les montagnes de Thermona, entre les continents de Io et d'Etherial. Ce territoire a été colonisé par les **navigateur de l'eau**, qui en a fait le centre de la guilde-peuple. C'est un endroit difficile d'accès pour les navigateurs qui n'ont pas de pouvoir sur l'eau.

Mélange de peuples, de religions et de coutumes, mais grandement peuplé par les membres de la guilde de l'eau, sa capitale est la cité flottante de Zhûria.

## Les plaines d'Io

Io est le nom données à de grandes plaines encatrée dans la chaine de montagne de Thermona, séparée par elles des deux sous-continents. Les pleines d'Io sont aujourd'hui dirigé en grande partie par le Royaume d'Aymon, qui a progressivement pris contrôle de tout le sous-continent.

### Royaume d'Aymon

Le royaume d'Aymon est un grand royaume, dirigé depuis plusieurs siècles par la famille d'Aymon, en majorité rurale et champêtre qui vit surtout de l’agriculture mais également de quelques mines au nord du pays. En effet, ce pays majoritairement peuplé par les espèces Canensis, Lierbil et Danvad est composé d'immense prairie fertiles, avec quelques grandes villes, ancienne cité-états à l'époque pré-classique. Aujourd'hui encore, il conserve un caractère très traditionnel, ainsi que des rancoeur ancestrales et complexe entre les différentes villes, et la noblesse y garde une importance considérable. Une grande partie du royaume est encore boisé, et si les technologies s'installe dans les villes, le royaume d'Aymon garde une image de petit royaume paisible.

Les plus grandes villes du royaume d'Aymon sont Erles (capitale du royaume, ville connue pour ses nombreuses horloges gigantesque), Varel l'Antique (une très ancienne cité, plus grande ville du royaume), Linnes (une citadelle ancienne fortifiée, refuge historique des rois d'Aymon lors des invasions, aujourd'hui capitale militaire), la cité portuaire de Sonate qui est le plus grand port commercial du royaume, et la grande ville étudiante de Talane, connues pour ses nombreuses bibliothèques.

De nombreuse villes de l'époque pré-classique en ruine y subsistent, tel que l'île d'Ether (située proche des côtes Bymanniennes) et la ville de Stylène. Ces ruines sont de grands cites touristiques, et revêtes encore aujourd'hui une importance considérable.

Le royaume d'Aymon fait partie du Magister.

### Province de Bymann

La province de Bymann (toujours aujourd'hui controlé par le royaume d'Aymon) est un territoire froid et sec de l'ouest du continent d'Io. Cette province est historiquement plus industrialisée que le royaume d'Aymon de par les débuts d'industrialisation de l'Empire de Skelfing, et de part le fait qu'après l'annexion au royaume d'Aymon le royaume à concentré ses effort industrielle dans sa nouvelle province.

Bymann est unterritoire dont l'histoire compliquée à laissée au moins trois capitale, la citée ancêstrale éponyme de Bymann est l'ancienne cpaitale datant de l'époque ou Bymann était une royaume indépendant en grande partie conseillé par le peuple oublié de Choris, jusqu'à son premier effondrement. Durant la période Skelfing, le duchée puis province impériale de Bymann a pris comme capitale la cité de Ters. Cette ville a été délaissé après l'annexion au profit de la plus grande cité de la province, Uriss, ville nouvelle qui bénéficiait des grands gisement de crystaux stygiens du nord de Bymann. Cette histoire fait de Bymann une province multiculturelle.

Dans les terres les plus occidentales de Bymann se trouve de grandes fosses formant la Vallée des Esprit. Dans cette vallée, datant de l'ère de la première civilisation Bymann, se trouve les ruines de la Tour de la Mémoire, lieu désormais important pour les membres de la guilde du temps, qui sont les gardiens de la tour.

[[Ville de Sophale]]
