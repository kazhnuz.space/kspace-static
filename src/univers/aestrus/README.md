# Æstrus

> Depuis les débuts, nous regardons avec envie les cieux. Notre limite. Nous savons que nous avons été amené dans ce monde, jadis. Mais nos origines restent encore un secret. Nous ne savons pas d'où nous venons, juste que ceci n'est pas notre Terre. Mais là-haut, dans ces îles, se trouvent peut-être toutes les réponses. Le secret de nos origines. Le plus grand des mystères. Vous avez envie de découvrir tout les secrets de la planète Æstrus ? Regoignez les scientifiques et les aventuriers pour explorer les Cieux.

Æstrus est un univers de science-fantasy "lumineux" et coloré avec des animaux anthropomorphiques amusant, inspiré à la fois de Sonic the Hedgehog, mais également de séries comme Megaman ou Starfox. C'est l'univers de mes projets comme *Radiant Skies* ou autres projets proches.

Il a pour but de montrer également un univers mélangeant des inspirations du folklore du moyen-âge et de l'époque moderne (avec des duchés, royaumes, et républiques italiennes), tout en combinant cela à un univers plus futuriste, avec des technologies propres à l'univers.

Il utilise fortement dans ses concepts des références aux éléments mais avec des technologies fondées dessus, mais également des aspects "magiques". Dans ce monde, sur la planète Æstrus, les habitant recherchent à retourner dans les Cieux Radiant, un archipel aerien avec de nombreuses lignes. Quels secrets sont cachés dedans ? Ceux d'un monde à la constante recherche de ses origines.

> Peut-être que le jour où nous retrouveront la voie des cieux radiants, peut-être que les Créateurs reprendront confiance en nous et se remettront à nous parler. Peut-être que nous ne seront plus seuls.
