# L'ignum

L'Ignum est l'incarnation de l'éclat dans l'univers de Nouvelle Frontière. Plus rare, et moins volatile que dans d'autres univers, il est toujours une substence/énergie née des contradiction interne dans l'univers (phénomène nommé le "Paradoxe"). Il prend donc ici la susbtence d'une matière étrange, rare, aux popriété calorique (production d'énergie) et radioactive. Elle peut provoquer quelques phénomènes quantique (superposition d'état) ce à échelle macroscopique. L'ignium n'est pas une susbtance atomique (elle n'est pas de la matière ordinaire), mais à la particularité de pouvoir réagir avec des atomes de matières ordinaires.

Cette substance à eu plusieurs nom : non seulement l'ignum (en référence à une ancienne théorie de la chaleur comme matière), mais aussi le "calorique" ou le "lupium" (en référence à la quantité qu'on trouve dans le système solaire de Roma).

L'ignum a été découvert pour la première fois par les Centaurien, qui l'utilisèrent pour créer leur moyen de communication hyperspatiaux.

## Les différentes formes de l'ignum

L'ignium peut exister sous des tas de formes, combinées ou non, mais que l’on classifie en deux catégorie : l'ignium *stable*, et l'ignium *lourd* (bien plus radioactive et mutagène). Une grande partie de l'ignium lourd vient d’une catastrophe dont peu est connu, mais peut aussi être produit artificiellement en quantité plus réduite.

L'ignium possède différentes utilitée suivant sa forme :

- L'ignium stable pur possédant de forte propriété de génération et de stockage d'énergie est utilisé grandement pour créer des batteries à hautes performances, utilisée en grande partie pour allimenter les bases spatiales éloignée du soleil où les vaisseaux spaciaux. Le principe est qu'elles sont rechargées à l'aide de l'énergie solaire, puis peuvent tenir des temps records pour fournir ensuite de l'énergie. Les capacités de conservation d'énergie baisses spectaculairement sous forme d'alliage, mais les métaux et matériaux produit ont souvent des particularités magnétiques, voir supra-condutrice. En effet, il est possible avec de l'ignium de construire des supra-conducteur à température ambiante.

- L'ignium lourd pur est théorisé comme pouvant servir à construire des armes destructrices. C'est cette forme qui permet de produire des effets quantique à échelle macroscopique. Il sera utilisé pour maitriser plus facilement les voyages hyperspatiaux, mais également plus tard également les voyages interdimensionnels.

## La production de l'ignum

La récupération et le raffinement de l'ignium est surtout dirigé par des gouvernements et des grandes corporations. Il s'agit d'une substance difficile à maitriser, même si quelques espèces tels que les Eirons semblent plus facilement vivre en grande quantité d'ignum.

Trois sources d'ignium existent : les « restes », l'extraction du vide et les veines d'ignum

- les "restes", sont l'une des sources d'Ignium les plus présentent dans le système de Roma. Ce sont en fait des restes de cette matière présentent à l'état pur ou sous forme de mélanges avec de la matière ordinaire, datant visiblement de la civilisation Romulienne. On suppose que ce sont d'ancien espace de stockage ou des endroits où des catastrophes ont provoqué des fuites.

- Les secondes sources sont les stations de production d'ignium spatiale. Si toutes celles planétaires ont été détruites par l'érosion et les sols, certaines spatiales en orbites géostationaires ayant perdu leur air ont pu être assez récupéré malgré les millions d'années pour commencer à être exploité. Cependant, on estime qu'une petite poignée de stations sont utilisable sur les dizaines de miliers qui ont du exister. Des projets existent pour créer de nouvelles sources, avec des générateurs de conceptions humaine.

- Les Veines d'Ignium sont des zones d’émission spontanée d'Igum qui existent à quelques endroits de quelques planète, relativement rares mais trouvable sur tout les continents. Même si ces veines sont rien face aux restes, il est possible de produire avec une énergie propre et durable avec. Ces veines sont également des failles à travers le continuum espace-temps, qui permet de dépasser les limites de l'univers normal. Le projet *Walkers* utilisera une de ces failles, situé à l'emplacement de Monado Prime, afin de pouvoir effectuer les premiers voyages interdimensionnels.

Il existe également un commerce de contrebande d'ignium, avec des sources mineures sur des astéroïdes. Il existe une rumeur qu'un groupe de pirates spatiaux controleraient même une base de production Romulienne.
