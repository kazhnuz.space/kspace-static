# Nouvelles Frontières

> C'est vers la fin du 20 siècle que s'est produit l'évenement qui nous a donné envie de rejoindre les étoiles. Une personne ordinaire dans l'espace. Il nous a inspiré. Nous a donné envie d'avancer vers cette nouvelle frontière.

Nouvelles frontières est un univers de SF se passant dans notre système solaire plus d’un siècle dans le futur, plus précisément une sorte de mélange entre de l’anticipation et de l’uchronie. Il s'agit d'un univers relatant la manière dont l'humanité s'est étendu, jusqu'à dépasser le cadre de sa dimension.

Cette humanité fait partie des sept terres alternatives ayant été à la source de ce qui sera nommé la "civilisation de Phosophorus" dans le multivers.

Dans ce projet, le point de divergence est que l’explosion de navette spatiale Challenger le 28 janvier 1986, n’a pas eut lieu, et le projet Teacher in Space a marqué la volonté de faire de la conquête spatiale un des points qui amènerait l’humanité en avant.

(le second point uchronique est le fait que la science spatiale après cela progressera pas mal, permettant ce qui va suivre – mais l’idée est que la réussite du projet va entraîner encore plus de motivation pour la recherche spatiale).
