---
title: Les mystères
date: 2017/11/13 21:56:00
categories: Pensées
---
Quand j’étais petit, aucun lieu ne me semblait plus mystérieux qu’une bibliothèque. Un espace avec ses rituels étranges, tel que garder le silence. Un lieu ou tout pouvait se trouver, n’importe quel histoire pouvait se cacher au tournant d’un ouvrage. Ces lieux étaient comme des temples, non pas uniquement du savoir, mais également des temples des histoires, des lieux et des mondes à découvrir.

<!-- more -->

Quand j’ouvrais un livre, je me demandais toujours : comment quelqu’un a pu écrire ça. Comment pouvait-il savoir que les mots étaient les bons, comment pouvait-il réussir à écrire des choses aussi belles. Est-ce que chaque phrase était fabriquée, façonnée dans l’argile des mots et des figures de style afin de construire un ensemble ou chaque sonorité et lettre tomberait à la bonne place ? Ou était-ce un talent inné, les phrases naissaient toutes seules, naissant comme une mélodie harmonieuse de caractères couchés sur du papier. J’errais dans les bibliothèques à la recherche d’une réponse à cette question.

Comment une histoire pouvait-elle être faite ? Où trouvaient-ils des idées originales, comment pouvait-elle naître d’un coup dans leurs esprits fertiles. N’ayant point la clef de l’inspiration, la porte de la compréhension de ce mystère semblait rester irrémédiablement close. Comme si le secret des livres que j’adorais dévoré semblait impossible à découvrir par la simple lecture. C’est ainsi que l’envie d’écrire naquit. Comment réussir à faire pareil. Comment réussir à faire des histoires. Était-ce des jeux ? Est-ce qu’on jouait des personnages, puis on couchait ses jeux sur du papier ? Était-ce des plans bien suivi ? Était-ce un univers dans lequel on vivait un peu ?

Mais quand on passe de l’autre côté du miroir, n’y a-t-il pas un risque de perdre le mystère ? Ce qui semble impossible devient possible, et le pouvoir mystérieux de créer devient narratologie, construction du conte et théories du récit. La fulgurance de la naissance d’une histoire devient clapotis mécanique d’un clavier. Est-ce que le transformer en méthodologie n’a-t-il pas tué le mystère ? Les symboles devenus visibles n’ont-ils pas érigé un mur bloquant l’immersion dans le récit ? Lorsqu’on perçoit des structures et des formes dans un récit, lorsque l’expérience nous permet de prédire ce qui va se passer, est-ce que nous n’avons pas perdu un peu de notre enfance, du jeune nous qui aimait dévorer les livres avec la naïveté de la découverte ? La désillusion du récit n’est-elle pas la perte d’une partie de la magie des livres ? Et pire encore, n’est-ce pas la fermeture partielle de certains mondes de récits, remplacés par des microscopes et des fiches d’analyses ? Peut-on encore errer dans une bibliothèque, alors ?

C’est ainsi que l’on part alors sur une nouvelle route. Après une quête pour remplacer le savoir par le mystère, on cherche le mystère au-delà du savoir. Récit qu’on a jamais lu, dans un style tout nouveau. Récit dont on pensait déjà tout connaître, observés d’un œil nouveau. Peut-être que la recherche est-elle avant tout un amour du mystère.
