---
title: Fanfictions
date: 2019/01/29 22:33:00
categories: Souvenirs
---

Comme pas mal de créateurice, mes premières créations étaient de la fanfictions. J'ai écrit de nombreuses fanfictions jamais terminé, avant d'avancer vers de nouveaux types de textes (de la poésie, des contes, des légendes, et actuellement je tente une petite collection de romans).

Dans cet article, j'aimerais faire une petite retrospective sur

*Note : ce petit article est fait à partir des réponses que j'ai donné à [un thread](https://twitter.com/Kazhnuz/status/1090362098243260435) qui questionnait les ancien⋅ne⋅s écrivain⋅e⋅s de fanfictions. Du coup, le texte risque d'être un peu "guidé" dans le sens où j'ai beaucoup gardé la trames des questions/réponses.*

<!-- more -->

J'ai commencé à écrire et imaginer des fanfictions vers 2008~2009 (j'avais 11/12 ans), avec comme première fanfiction postée une vieille fanfic Sonic, qui était une uchronie de l'univers de Sonic où le GUN serait devenu mauvais (sans vraiment d'explication pour), avec plein d'idée chelou tel que Mighty qui était armée d'une épée. Je suppose pour aller avec l'armure naturelle ?

J'ai commencé à en écrire parce que j'aimais raconter des histoires dans les univers que j'aimais, et qu'au début l'écriture était le seul outil que j'avais à ma disposition. C'était une extension de ma façon de jouer aux jeux, en racontant des histoire dedans.

Je faisais d'ailleurs pas mal cela même *en jouant aux jeux*, notamment à tous les jeux classiques/retro.

Cela avait plusieurs qualités et défauts.

Les qualité sont que c'est sympa à faire (c'est très agréable d'utiliser un univers qu'on aime et de l'étendre) et que c'était un excellent exercice pour apprendre et s'améliorer en écriture et en scénarisation (par les contraintes du fait d'avoir des éléments à respecter). Cela me permettait d'apprendre, tout en m'amusant.

D'ailleurs, chose amusante : je *détestais* le français en tant que matière avant de commencer à écrire, et la fanfic me l'a fait apprécier, parce que j'y a vu dedans un moyen de m'améliorer et de prendre plus du bon temps en écrivant des trucs qui me plaisaient et qui "rendaient bien".

Si je devais dire un défaut, ce serait que du coup on a moins l'impression d'avoir créé un tout cohérent, avec des éléments s’emboîtant ensemble. Aussi, trop dépendre de l'œuvre originale peut dans certains cas devenir un frein à nous développer nous, si on fait que ça, je trouve. Je ne dis pas que c'est forcément un soucis, mais je pense que les deux vont ensemble.

Une solution à cela, et que je fais encore aujourd'hui, sont aussi les AU (Alternates Universe). Personellement, je les adores, ma première fiction en étant encore un (et ma deuxième était un AU mélangeant le monde de Sonic à celui des Burning Rangers). Et j'en imagine encore souvent sur pas mal d'univers. Je trouve qu'imaginez les AU d'univers qu'on apprécie, et voir comment les personnages évolueraient dans ces variations d'univers, est aussi un excellent exercice pour apprendre à gérer des personnages, à bien les connaîtres.

Une autre chose que je faisais pas mal, et qui affectait beaucoup ma façon d'écrire des fictions était les fameux « fancharacters » ou « original characters ». J'ai toujours aujourd'hui beaucoup de personnages que j'ai créé pour les adapté à un univers prééexistant, soit parce que je trouve qu'ils apportaient quelque-chose, soient parce qu'ils étaient utiles à mon histoire, soit parce que j'aimais bien le concept ou l'idée derrière eux... Où juste pour m'amuser et pour en créer !

Notamment, j'en faisais beaucoup pour faire du RP, grand souvenir de cette période, qui est pour moi aussi très proche de la fanfiction, dans le sens où les deux permettent de vivre l'univers de fiction d'une certaine manière.

C'est au bout de quelques années, vers la fin du collège, que j'ai commencé à vouloir passer aux univers originaux. C'est une transitions qui s'est faites de manière assez « fluide », je trouve. Je voulais faire un immense projet "chorale" que je présenterais avec une histoire énorme. (Ce projet a d'ailleurs fini divisé en pleins de plus petits projets, plus gérables)

De plus, j'étais à l'époque surtout dans le fandom Sonic, et mon déclic a été le moment où je me suis rendu compte que ce serait toujours le bordel dedans. Du coup, j'ai eu envie d'avoir des bases un peu plus "stables" pour mes histoires.

Le projet qui m'a fait passé à la fiction originale est parti d'une histoire qui mélangeait de nombreux fandoms que j'aimais, avant que petit à petit, je veuille inventer ma "propre histoire". Et au début, qu'est-ce que cela me faisait peur ! Je pense qu'on est nombreux à flipper un peu dans ce genre de cas.

Et pour ma part, ce qui m'effrayait le plus, c'était l'aspect énorme que ça avait pour moi ! Créer tous les personnages, tout l'univers, trouver des concepts intéressants surtout… Cela me semblait une tâche énorme, surtout que j'aime les univers très complets avec beaucoup de lore et de détails… Du coup, j'avais peur de ne pas être capable de la tâche.

D'ailleurs, j'ai beaucoup de perso d'histoire originaux qui étaient des fan-charas que j'ai réadaptés, et quelques persos qui sont des expy de version alternatives de personnages officiels que j'ai imaginé dans un AU. Au début, je me suis beaucoup raccroché aux branches.

Cependant, avec le temps, et les projets, je me suis petit à petit senti capable de créer des univers. Je devenais de moins en moins dépendant, je me sentais de plus en plus capable d'imaginer. Et je pense que pour beaucoup de créatifs c'est aussi ça "se sentir capable de". Ça a commencé par la création d'AU et de fanfics plus basé sur nos OC à moi et mon frère, de plus en plus jusqu'à une fiction (Hinsein) qui était encore officiellement une "fanfic", mais qui n'avait quasiment plus rien de l'univers originel.

Et petit à petit j'ai créé des univers bien plus "perso", basé sur des concepts que j'imaginais.

Aujourd'hui, je n'écris plus de fanfictions "littéraire" que je publie, et je me concentre sur mes univers pour ce qui est écriture. Cependant, je continue d'imaginer des AU et des histoires dans des univers. Et j'écris plus des histoires de fangames.

Cependant, je suis très heureux de toute cette expérience. Écrire des fanfictions a été une grande partie de mon adolescence, et je suis super content d'avoir vécu cela.
