---
title: Les commentaires et la liberté d'expression
date: 2020/03/21 09:26:00
categories: Pensées
---

En quelque sorte une suite de mon thread/topic sur les commentaires dans l'art et le fait d'en vouloir : est-ce qu'un artiste doit accepter tout les commentaires qu'on lui fait ?

Cet article est adapté d'un thread que j'ai fait à propos d'un message de Shasha Brown, qui indiquait qu'elle allait modéré les commentaires étant uniquement du "c nul" ou du "c'était mieux avant" (message qu'elle a supprimé depuis, sans doute pour mettre plus en avant les autres messages où justement elle explicite mieux ce qu'elle disait)

Ce qui m'a fait réfléchir, c'est que pour critiquer cela, beaucoup disait que c'était une atteinte à la liberté d'expression. Je me suis donc dit qu'il était temps pour une mise au clair sur la liberté d'expression.

<!-- more -->

Beaucoup de gens sur internet ont donc tendance à dire qu'un⋅e artiste qui supprimerait les commentaires négatifs serait de la "censure" ou une atteinte à la "liberté d'expression". Et qu'un⋅e artiste devrait accepter tous les commentaires tant que ce serait des commentaires « constructifs ».

## De la liberté d'expression

( Wesh y'a des titres et tout )

Sur le premier point, je pense qu'il est important de rappeller ce qu'est la liberté d'expression.

La liberté d'expression ce n'est pas le droit de tout dire, sans avoir de conséquence. La liberté d'expression, c'est uniquement le droit à ne pas être inquiété par l'état pour le fait d'avoir exprimé votre avis sur une question, c'est la lutte contre une censure d'état qui tenterait de définir ce quoi il serait possible de parler.

*Petit aparté* : Cependant, cela ne veut pas dire que l'état n'a pas le droit et le devoir de lutter contre certains messages. En France et dans de nombreux pays, cette liberté d'expression est cependant limité par des lois. L'idée que tout discours n'est pas juste un discours neutre uniquement théorique, mais que les mots peuvent être des armes. Ainsi, les insultes (qui sont des attaques visant à diminuer le bien-être mental de la personne visée), la calomnie et la diffamation (attaquant la réputation) et les appels à la haine (qui sont globalement tenter de provoquer des actions néfaste envers une personne ou un groupe) ne sont pas couvert par la liberté d'expression. Et c'est d'autant plus le cas quand c'est fait envers des personnes subissant déjà de la haine. La haine et le mépris envers des groupes de personnes discriminé ne sont pas juste des « opinions », mais l'expression de systèmes oppressifs.

Mais trève de cet apparté. Donc, je disais que la liberté d'expression ne vous donne que le droit de vous exprimer - tant que cet avis n'est pas insultant, diffamatoire ou discriminatoire. Cependant, lorsque vous commentez quelque chose, vous n'êtes pas dans un espace public à vous, mais dans un espace pour parler avec læ créateurice. Et ça change tout.

Iel à tout a fait le droit de décider avec qui iel veut parler. C'est son espace. Vous pouvez toujours dire ce que vous pensez, mais ailleurs. Un⋅e artiste n'a aucune obligation d'écouter où de mettre en avant un commentaire, d'autant plus quand celui-ci est désobligeant.

Pour donner un superbe exemple qui me vient de [Marcel Dupoignard](https://twitter.com/MDupoignard), si vous entrez chez quelqu'un pour dire « ta décoration est moche », celui-ci sera tout a fait en droit de vous mettre à la porte de chez lui.

![Oh, you've redecorated ! I don't like it !](https://66.media.tumblr.com/0f900f708c5103497793e2d3c5a51924/tumblr_mwtj8hjnmb1s6wq07o1_250.gif)

C'est comme les forums, c'est comme les espaces d'échanges avec les personnes, le blocage sur twitter : la liberté d'expression n'oblige personne à vous écoutez.

Et je pose le XKCD obligatoire sur le sujet :

[![Une BD XKCD, portant sur la question de la liberté d'expression](https://imgs.xkcd.com/comics/free_speech.png)](https://xkcd.com/1357/)

## Des commentaires constructifs

Maintenant, parlons des fameux « commentaires constructifs ».  Sur internet, le « commentaire constructif » est souvent émis en opposition avec le simple commentaire de type « j'aime » ou « j'aime pas ». Cependant, je trouve que c'est un peu devenu une sorte de "meme" (au sens [de l'utilisation qu'en fait Mike Godwin](https://www.wired.com/1994/10/godwin-if-2/)) dans la communauté créative internet, notamment pour détermine les « bons commentaires négatifs ».

Mais c'est là où j'ai un soucis sur la question des « commentaires constructif » : juste expliquer ce qu'on n'a pas aimé n'est pas forcément constructif pour un⋅e créatif⋅ve, même avec l'explication la plus complète du monde.

Prenons un exemple : dans une histoire, l'auteur décide de mettre moins en avant le fonctionnement du monde, pour passer plus de temps sur les personnages et leurs interactions. Vous n'aimez pas et trouvez que ça rend le monde "moins bien", qu'on "ne peut pas s'y intéresser".

Votre avis est pertinent dans un sens, parce qu'il représente votre ressenti. Cependant, est-ce que c'est vraiment "constructif" ? Parce qu'en fin de compte, il ne s'agit parfois que de questions de choix, voir de gouts. Et les gouts, bah y'en a des tonnes de différents, donc on peut se demander en quoi un serait « plus pertinent »

( C'est un peu comme la question du "écoutez les fans" ou "savoir faire un truc qui plais au fans" que je vois pas mal de temps en temps... Y'a exactement le même genre de soucis, mais multiplié par le nombre de fans en question. « *C cho* » comme on dit. )

Je ne dis pas qu'il n'est pas possible de faire des commentaires constructif pour "aider à s'améliorer", où que c'est mal. Mais qu'il ne faut pas penser qu'un "avis" l'est forcément, et que dire "ce que j'ai pas aimé" va l'aider à "s'améliorer".

Parce que ce qu'on aime ou pas n'est pas toujours pertinent.
