---
title: On the road again
date: 2016/03/21 12:00:00
categories: Perso
---

Bonjour, et désolé de ne pas avoir donné plus de nouvelles depuis mon dernier journal, depuis que je vous ai annoncé la perte de mon père.

<!-- more -->

J'aimerais commencer par tous vous remercier pour le soutiens apporté, pour vos messages. Les lire m'a fait beaucoup de bien. Mais aussi remercier tous ceux qui sont venu me parler. Tous ceux qui m'ont proposé de venir parler. Et même tous ceux qui ont ne serait-ce qu'eux une pensée du type « merde, c'est pas cool ». C'est vraiment gentil, et dans ce genre de moment ça fait du bien de se rappeler qu'on est pas tous seul.

J'aimerais aussi m'excuser de pas vraiment être venu parler. Vos propositions étaient très gentille… C'est juste moi qui ait pas réussi. J'ai souvent du mal (sauf parfois, mais c'est rare) à me lancer de moi-même dans une conversation directement avec quelqu'un… x) Je sais déjà répondre quand on me parle, et parfois venir discuter dans des groupes de plusieurs personnes ou je me sens pas trop stressé. C'est déjà pas mal, je suppose ? x)

Quant aux dernières nouvelles, même si je peux pas vraiment dire que « ça va », je pense que je réussi à me remettre en chemin. Il reste encore beaucoup à faire, un sacré bout à parcourir, mais je suis à nouveau sur la route. C'est déjà quelque chose, je suppose ? J'ai enfin notamment réussi à m'inscrire au code. J'ai pendant plusieurs mois repoussé l'échéance, et je le faisait déjà depuis mes 18 ans… 5 ans ! Mais cette fois, c'est fait, et je commence à apprendre et à m’entraîner.

Je me suis également remis à l'écriture, et j'ai fait du gros ménage dans mes projets. J'ai notamment décidé de tout remettre à plat, et de prendre un nouveau départ. Avec des projets et objectifs plus petit au départ. Terminer des nouvelles inachevés, déjà. J'en ai quelques unes de déjà terminées, que je posterais petit à petit. Puis écrire des lights novels et des romans. Et quelques autres petits trucs, dont refaire chauffer ma version pro de Game Maker : Studio (merci à un Humble Bundle). Mais même avec ces nouveaux départs, je reprendrais pas mal de mes personnages, de mes idées, et je pourrais même sans doute plus me concentrer sur ces idées et persos :) (Katos ne sera pas attaché à un arbre au départ des vacances, je vous rassure–). Je posterais peut-être dans la semaine une liste des trucs en cours x)

Ce pourquoi j'écris aussi, c'est que je me demandais si y'avait d'autres personnes qui seraient intéressé par une/des conversation(s) de groupe sur Skype ? Désolé de pas proposer des convers' plus « ouverte » et accessible même à ceux qui n'ont pas Skype, mais pour ma part IRC fonctionne pas dans ma cité u, Epiknet accepte pas wifirst x). Je me suis dit que cela pourrait être quelque chose de fun, à plusieurs, en mode "on se discute quand on est plusieurs à être co et qu'on a le temps". Bref, c'est si y'a des gens intéressé, je n'oblige personne à rien x)

Bref, merci de m'avoir lu, et merci encore pour tous vos soutiens. Je vous n'aime les gens. Même si je suis souvent pas bien doué pour le montrer x)
