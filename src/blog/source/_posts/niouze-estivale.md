---
title: Niouzes estivales
date: 2018/06/25 12:00:00
categories: Niouzes
---

On est genre quelques jours après le début de l'été, et c'est un bon jour pour donner des news. Oui, cela n'a pas spécialement de logique, mais je ne savait pas comment vraiment débuter ce journal xD

Du coup, quelques nouvelles des différents trucs sur lesquels je bosse o/

<!-- more -->

## Evolution personelles

Comme j'ai indiqué en status, j'ai pris ces quelques derniers mois pour prendre un peu de recul, pour réfléchir un peu à ma manière d'évoluer, aux soucis dont j'ai parlé dans deux journaux consécutifs. Et si je n'ai pas trouvé de réponse magique, j'ai appris petit à petit à moins me trifouiller la tête… Et surtout j'ai compris qu'être quelqu'un de discret/peu locace/qui reste un peu dans son coin était aussi un trait de caractère tout simplement, et que j'étais pas forcément obligé de vivre ma sociabilité comme je pensais devoir la vivre. J'estime toujours que je devrais plus parler et tout… mais je me sens moins forcé de devenir sociable comme j'imagine que tout le monde l'est.

J'ai encore des efforts à faire, mais je suis dans un état sans doute plus stable et sain qu'il y a un moment, et plus en accord avec moi-même. xD

## Nouveau job

J'ai quitté à la fin de mon CDD mon ancien taf, et je suis pris pour un nouveau (plus que mon contrat à signé dès qu'il est signé par mon directeur d'atelier) qui va commencer en Septembre o/ Je vais faire une formation + boulot de développeur, je suis donc en recherche de logement sur la ville de Saint-Herblain, autour de Nantes. Je suis donc pour l'instant au chomâge en vacance, le temps que ça commence ^^ D'ailleurs, si y'a des gens qui sont intéressé pour qu'on se voit durant l'été, n'hésitez pas o/ J'ai (pour une fois) de quoi me déplacer et voyager, et je suis toujours partant pour venir voir du monde ! ^^

Merci à tout⋅e celleux qui m'ont encouragée, ça m'a fait très plaisir <3 Z'êtes des anges.

## chlore.net et sites hébergés

J'ai du mettre à jour y'a quelques temps suite à un grooos crash de mon serveur, chlore ! Tout n'est pas encore rechargé (juste le temps de remettre tout les certificarts et les config nginx), mais j'en ai profité pour rajouter quelques services dessus !

Quarante-Douze et mon blog sont up, et sont accompagné maintenant d'un service mail (avec webmail), d'un nextcloud, d'un serveur git (que j'utilise pour mes projets perso, les trucs plus "ouvert au moooonde" iront plus sur mon github) et de quelques autres trucs. Les services sont ouvert aux amis si besoin, donc n'hésitez pas si vous avez besoin d'un de ces types de services o/

Je réfléchi également a un redesign complet de mon blog, mais avant ça je termine un design pour un autre site ^^

## Projets de jeux et gamecore

Mes projets de jeux vidéo avancent pas mal ! Je bosse pour l'instant surtout sur gamecore, un petit moteur facilitant la création de jeux sous love2D, utilisant pas mal de POO et contenant un petit moteur de collision (utilisant du bump2D) afin de permettre de rapidement prototyper des jeux type retro. La version 0.5.1 est sortie y'a trois jours, et corrige quelques bugs du moteur de caméra, qui avait quelques soucis.

Niveau jeux purs, je bosse surtout sur deux projets : Imperium Porcorum (un plateformer 2D) et un fangame Sonic, Sonic Radiance.

## Écriture, JDR et rulebook

J'ai genre quasiment pas avancé en écriture pure… mais à la place je MJ depuis début janvier quasiment toute les semaines un JDR, DanseRonce, dont la saison 1 s'est terminé il y a quelques semaines (et reprend en septembre, quand mon frère sera de retour de son boulot) o/ C'est une histoire se passant dans une terre alternative avec de la magie "cachée" (enfin, pendant la saison 1…), plus précisément dans un pensionnat pour "jeune fille bizarre" situé au fin fond de la Bretagne. S'en suivi pleins d'histoire de dispute, d'intrigues politiques, de guerre entre deux sociétés anciennes millénaire et de phénomènes métaphysiques incomprénensibles.

À partir de ce JDR, j'ai décidé de créer une sorte de petit système généraliste (utilisant une base D100) sous licence Open Gaming Licence : rulebook (oui, gamecore & rulebook, j'adore les noms très complexes), qui vise à standardiser les règles que j'ai créé pour danseronce, sur lequel va se greffer danseronce et d'autres jeux ^^ (il contiendra aussi un example de "jeu de base" histoire de permettre un peu de mieux visualiser comment ça se passe une partie). Il sera téléchargeable librement en format PDF et ODT (modifiable) sur un site dédié o/

Je compte également reprendre quand j'aurais le temps Ailleurs, histoire de terminer la nouvelle version du tome 1

## Divers

À part tout ça, je réfléchis à quelques autres petits projets. J'hésite à écrire une histoire interactive utilisant twee2, mais je suis encore en train de réfléchir à comment le faire exactement xD Je réfléchissais aussi à faire un petit jeu de type "idle game" où on était un maitre du mal qui voulait envahir le monde avec diverses unités, sous idle game maker, mais l'outil est trop limité, et je préfère les outils open-source aux outils fermés ^^ Du coup je réfléchis à m'y mettre en utilisant Angular, le framework que je vais utiliser au boulot pour le frontend.

Niveau défis, je ne sais pas encore si je reparticiperais à l'Inktober-version-écriture, ou si je ferais le NaNoWriMo, ça risque de faire beaucoup avec les JDR et le boulot xD Par contre, peut-être que me remettre au "100 Theme Challenge" serait une bonne idée histoire de ne pas perdre la main. Peut-être aussi que je vais en faire des version "light", je sais pas. Au moins j'ai encore le temps pour réfléchir à tout ça ^^ J'aimerais bien participer un de ces quatre à une ludum dare aussi, ou à un truc du genre.

Voilà voilà pour les news ! J'espère ne pas vous avoir trop embêter avec mes petites bêtises xD
