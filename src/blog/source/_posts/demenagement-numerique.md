---
title: "Déménagement Numérique"
date: 2020/05/20
categories: Niouzes
---

Depuis le début de mai, j'ai commencé à vraiment bosser sur la nouvelle version de mon site, disponible à l'adresse kazhnuz.space. Mon but était d'avoir un nouveau système plus simple à maintenir, et qui correspondait plus à ma manière de travailler, et surtout me correspondant plus. D'avoir une sorte de "chez moi" numérique où je pourrais poster ce que je veux. En effet, mon ancien système me convenait pas trop, du fait d'avoir à constamment passer par une interface, ce qui faisais que je procrastinait énormément.

<!-- more -->

Du coup, j'ai décidé de tout rénover, et j'ai poussé la première version, qui contient des éléments pas encore terminée, mais qui au moins me permettent d'améliorer petit à petit à coup d'itérations. Cependant, outre ce blog, ce déménagement est un peu plus grand, puisque cela comporte tout les services que j'utilise.

Mon but est que tout mes sites/services/etc soient désormais sur deux serveurs, kobold.city (dédié aux sites webs) et kobold.cafe (dédié aux services personnels). J'ai d'ailleurs désormais une adresse mail en kazhnuz [at] kobold [point] cafe, ce qui est très fun à utiliser. Vous pouvez regarder sur [Kobold City](https://kobold.city) les sites que j'héberge déjà. J'y met tout mes projets plus "généraux", genre Birb (mon petit moteur de jeu utilisant le framework love2D) ou Rulebook (mon set de règle pour JDR). Kobold Cafe, quant à lui, est réservé aux amis qui aimeraient profiter desdits services, où qui veulent participer à un de mes projets persos. J'ai même des trucs genre mumble ou xmpp, si jamais y'a besoin d'un truc autre que Discord.

Mon blog, lui, garde un aspect plus personnel. Il y comporte mes projets vraiment personnels (univers, textes), et est divisé en trois grandes parties : le blog (pour parler de moiiii... ou de sujets qui m'intéressent, hors technologies), l'espace textes (pour publier mes écrits, avec téléchargement possible des fichiers au format PDF ou ePub, grace à l'outil crowbook de [Lizzie Crowdagger](http://crowdagger.fr/)) et l'espace "univers", composé de petits sites de docs décrivant mes univers.

## TODO

Qu'est-ce qu'il reste à faire, du coup ? Et bien... encore pas mal de chose. Je vais du coup un peu voir ce qui est fait et reste à faire dans cette section :

Sur la migration globale, il me reste encore beaucoup de boulot à faire, avant que tout soit sur les serveurs kobolds :

- Je dois migrer tout les codes sur git.kobold.cafe au lieu de git.chlore.net... Le soucis de cette migration c'est que ça veut dire aussi migrer tout les tickets, ce qui était en cours, etc.

- Je dois migrer Quarante-Douze, en le transformant en site "statique" (fonctionnant comme ce site). C'est aussi un gros morceau.

- Rediriger tout les mails envoyé aux domaines chlore.net ou kazhnuz.space vers le nouveau système, histoire de pas avoir trops de boites mails.

- Migrer rulebook.chlore.net vers rulebook2.kobold.city... ce qui demande que la version 3 de rulebook soit plus avancée. Damned.

- Mettre une photo de mon bbchat quelque part

Sur l'aspect perso/blog, il me reste encore pas mal de trucs à faire également, tels que :

- Améliorer le design de mon blog au niveau du responsive (y'a encore des bugs)

- Je n'ai pas mis tout les textes de l'Inktober 2017, puisque je trouvais que certain pourraient gagner d'une petite amélioration, notamment d'être rallongé ou amélioré, parce que justement j'avais beaucoup aimé les faires.

- Terminer et mettre mes textes inachevé, tel que celui de la fuite de Katos de son clan, ou Ailleurs.

- Compléter la fiche d'univers d'Aestrus

- Rajouter une fiche d'univers pour Imperium Porcorum.

- Me remettre à blogger/écrire régulièrement.

- Créer un espace pour pouvoir y mettre mes fiches de personnages.

- Ajouter le support des commentaires.

- Quelques améliorations de codes, pour éviter d'avoir trop de redondance.

Mon but va être du coup de poster régulièrement des updates, modifications et des nouveaux travaux o/ J'espère qu'avoir un nouvel espace personnel va plus me motiver à écrire/créer. En tout cas, cette refonte est un projet fun, et je me suis bien amusé à concevoir tout cela. Si ça intéresse du monde, je ferais un petit postmortem de la conception du blog.
