---
title: Erratum – Présentation de l’univers
date: 2018/06/25 12:00:00
categories: Projets
---

<p class="alert alert-warning">Attention, cette présentation est un peu vieille puisqu'elle date d'une ancienne version d'Erratum, qui depuis a été retravaillé à partir de mon JDR DanseRonce.</p>

> Depuis des temps immémoriaux, deux mondes existent, que tout oppose, mais lié par d’étrange portail. La Terre, le monde de la rationalité. La Contre-Terre, le monde de la magie.

> Les gardiens sont les garants d’un bon équilibre entre les deux mondes. Depuis la mort de Merlin et la fondation par cinq de ses disciples de l’Ordre, ils se sont occupé de baisser les ingérences de la magie sur Terre, et les ingérences des états terrestre sur la Contre-Terre, jusqu’à ce que notre existence deviennent un secret pour les terriens.

> Ils sont la barrière contre la prédiction la plus funeste qui a été faite : La guerre entre les deux mondes. »

<!-- more -->

Erratum est une saga de fantasy basé sur le concept d’un monde magique caché, qui regrouperait de nombreux éléments qui auraient inspiré les légendes et les histoires de créatures fantastiques. L’existence de ce monde serait maintenu secrête par un groupe secret nommé les « gardiens », qui aurait pour but d’éviter une guerre entre les deux mondes.

Ce projet présente donc les histoires de la Contre-Terre, un « clone » de la Terre qui existerait dans une autre dimension, où l’existence d’un phénomène étrange permettrait l’existence de créatures surnaturelles et de phénomènes qualifié de « magique », et les histoires de tout les êtres qui se trouvent un peu entre les deux mondes, tout les « passeurs ».

La Contre-Terre est un univers très sauvage, sans énormément de règle, et sans grande structures étatiques. À l’exceptions de quelques empires qui tiennent étrangement malgré les dangers magiques, les structures politiques y sont souvent petites, allant de la cité-état au petit état (généralement pas bien grand, genre taille d’une région en France). L’unique grande structure de cet univers sont les gardiens, qui possède des avoirs dans de nombreux pays de la Contre-Terre ou de la Terre, et qui dirige quelques villes secrêtes.

Pour un terrien, la Contre-Terre sera un monde où il est difficile d’avoir une vision globale, mais où tout un tas de surprise peuvent être découverte en l’explorant. Mais également où le danger peut être présent partout…

Cependant, ce monde est également celui où peut se trouver une puissance incroyable. Si quelques êtres biologiques sont capable de pratiquer ce qu’on appelle la magie, il est également possible d’avoir des pouvoirs plus puissant, avec les pactes. En effet, sur la Contre-Terre vivent des êtres nommés les « Esprits », des êtres de magies purent qui ont le pouvoir d’influencer sur diverses choses. L’esprit, la matière, la vie… ces pactes offrent à porter de mains aux êtres biologiques des pouvoirs incroyables.

Qu’est-ce que les gardiens protègent en fin de compte ? Est-ce qu’ils protège la Terre des puissances qui pourraient attaquer venant de la Contre-Terre, ou protègent-ils les puissances que renferme le monde magiques de personnes mal intentionnées ?

*

*   *

Pour les curieux, ce projet n’est pas le premier Erratum dont je parlais et que j’avais abandonné par le passé, mais une reprise du nom pour Antichton, qui est en fait un projet plus anciens (qui se nommait à l’origine « Rites »), que j’avais fusionné dans Erratum (premier du nom) pour former tout les aspect un peu fantasy d’Erratum. Lorsque j’ai arrêté le projet Erratum quand celui-ci à commencé à devenir trop fouilli, j’ai repris les projets qui le composait séparément, dont Rites qui est devenu Antichton puis finalement Erratum (nouvelle version).

De ce fait, d’une certaine manière Erratum nouvelle version est la version simplifié ( à savoir sans les éléments de science-fiction (qui font désormais partis de Nouvelle Frontière) et les trucs comme les cochons (qui sont désormais un projet à part : Imperium Porcorum) ) de Erratum première version.

---

Le principal projet dans cet univers sera "Les Gardiens", une saga présentant une classe de 21 apprenti-gardien, qui devront faire face aux questionnements que pose leurs convictions à un moment où le secret magique est mis en grand péril.

J'ai écris déjà quelques récits qui se passent dans cet univers, et pour les curieux en voici la liste. Certains textes sont des petits comptes et légendes que j'ai écrit qui se déroulent quelque part dans cet univers, tandis que d'autres sont en quelques sortes des "préquelles" à la saga Les Gardiens.

Donc, en bref, voici le monde de fantasy que j'essairais de développer avec quelques histoires, à côté évidemment de quelques one-shot et de Nouvelle-Frontière (mon monde de science-fiction).
