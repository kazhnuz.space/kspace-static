---
title: Carrefour
date: 2018/12/28 12:00:00
categories: Perso
---

Une année se termine, une nouvelle arrive. Nous entrons dans les temps du froid et des réveillons, mais aussi celles des bilans et des résolutions. Nous entrons dans un temps de passage. Chaque jour est un passage, chaque instant est une transition entre l’avant et l’après. Le passé et le futur. Cependant, le nouvel an marque un passage. Physiquement, cet instant n’a rien de plus important qu’un autre… Mais une borne a été posée. Et comme certaines bornes sont l’entrée vers le domaine des contes et des légendes, le nouvel an est l’entrée vers le futur.

2018 a été une année riche en… pas mal de chose. Ce fut une année commencée sur de mauvais augure. Perte, burn-out, fatigue et incertitude. Cette année fut surtout l’année des changements et des croisements. L’année des réalisations.

<!-- more -->

Le premier de ces carrefours fut celui entre mes choix d’avenir professionnels. J’ai échoué le concours une deuxième fois. J’ai réussi mon mémoire sans trop de difficulté (si ce n’est celles provoqués par le fait de procrastiner), mais mes stages étaient bien moins faciles pour moi. J’ai eu une période de chômage, avant de trouver un emploi et l’espoir de devenir dans le futur un développeur.

Le second fut celui de mes choix en tant que créateur. J’ai tenté de faire évoluer mon style. J’ai commencé une histoire plus longue que mes projets précédants. J’ai fait évoluer le monde d’Erratum, en un monde plus ambigüe, moins centré sur un scénario et un lore immense, mais sur des personnages. J’ai commencé à me définir en tant que développeur, mes projets, mes objectifs. J’ai commencé un projet de livre interactif. Cette année a été la plus riche créativement, même si tout n’a pas été visible. J’ai commencé à évoluer en tant que créateur.

Et pour une fois, je suis assez content de ce que je deviens, et j’ai pas mal d’espoir de réussir à faire des trucs sympa. Je sais que mon style est imparfait, que ma plume n’est pas celle d’un écrivain littéraire, et que je ne serais jamais un développeur de talent. Mais je suis heureux de pouvoir narrer mes petites histoires, mes petits jeux de vies



Le troisième fut celui de mon évolution en tant que personnes. Je me suis renfermé sur moi-même ces dernières années. Suite à des expériences malheureuses et ma tendance à l’anxiété, j’ai fuit les relations et le fait de montrer trop de moi-même. Je me disais que les gens étaient mieux sans moi. Que j’allais les déranger. Alors même que j’écris ces mots, je n’arrive pas à me dire que c’est faux. Ce doute est en moi, et le sera sans doute toute ma vie. Je ne sais pas si je suis une bonne chose. Cependant… Je ne dois pas utiliser cela comme une excuse. Je n’ai plus lancé de discussion de moi-même sur internet depuis longtemps, sans chercher une « raison ». Je fuis le fait de dire clairement mes soucis et ce que j’aimerais, et je le cache derrière quelques métaphores littéraires et pédantes fables où je ne fustige que mes défauts, mêlé dans des idées. J’essaie de m’exprimer, mais j’ai peur que quelqu’un comprenne.

J’ai tenté plusieurs fois de me « resocialiser ». Comme s’il s’agissait uniquement de la perte d’une habitude, et comme si le simple fait de dire « je vais faire ça » pouvait résoudre le problème. J’ai longtemps théorisé la « source » du problème, comme s’il s’agissait. Comme s’il y avait un ennemi que je pouvais affronter et vaincre pour me libérer de cette difficulté. J’ai souvent relu les messages qui me proposait de venir parler. Qui me rappellaient que je ne suis pas seul.

Je me suis demandé pourquoi je le faisais pas. La raison est évidente : j’ai la frousse de déranger. Peut-être aussi ai-je un peu honte de justement ne pas avoir demandé d’aide, et me dis-je qu’il est trop tard, où une autre bétise du genre ? Meh, ce serait bien mon genre. Je pourrais tenter des heures d’épiloguer sur la raison de cela. De tenter de me psychanalyser. Est-ce une peur de déranger ou du refus, me demanderait sans doute un psy. Honnêtement je pense qu’il y a souvent un peu de l’un dans l’autre, et que je n’échappe pas à la règle.

Toujours est-il que sur ce point, je suis à un carrefour. Je sais de plus en plus qu’un changement est nécessaire. Que je dois redevenir qui j’étais… mais peut-être est-ce que j’idéalise trop cette autre période de ma vie ? C’était aussi l’époque de quelques déboires sentimentaux, de mes déprimes de débuts de faq, et d’une grosse période où je n’allais déjà pas bien et où j’ai commencé à comprendre que j’avais des tendances à la dépression pour un rien. Mais j’avais quelque chose en plus. Je me sentais plus entouré. Déclarer un crush m’était bien plus facile. J’arrivais plus facilement sur l’improviste, pour aider où pour m’amuser. Je sortais plus, plus loin. Je partais dans des domaines qui n’étaient pas de bonnes idées d’un point de vue note et « avenir ».

Où peut-être que je devrais arrêter d’écrire ce texte déjà trop long, que je vais poster en fermant les yeux parce que j’en aurais honte, comme toujours. Où je vais me sentir super gêner en le relisant parce que j’ai encore en moi cette idée que me confier moi, c’est mal et ça va juste emmerder le monde.

Des textes comme ça, j’en ai écrit des tas. Parfois, je les ai postés. Souvent, je parlais moins de tout ça que là. J’osais moins.

Peut-être que cela devrait être ça, ma résolution pour 2019 ? Devenir moins « prudent », et oser plus ? Ne plus me laisser aller à laisser quelques merdes de mon passé être mes guides dans mes actions, et regarder les choses autrement ? Je ne pourrais sans doute pas arrêter d'être une boule de stress et d'anxiéter… Mais je peux apprendre à ne plus les laisser me pourrir l'existence.

Nan, ce sera sans doute 1920×108— *se fait violemment trucider*
