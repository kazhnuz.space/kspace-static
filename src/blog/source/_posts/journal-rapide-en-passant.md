---
title: Journal rapide en passant
date: 2017/11/21 12:00:00
categories: Perso
---
Aaaah, on est le 20 Novembre je suis en retard sur pleins de trucs aaaaah D:

<!-- more -->

*ahem*

Voici un petit journal rapide juste pour donner des nouvelles, et pour m'excuser du retard que je prend en commentaire et en réponse de commentaire. Merci encore à toutes celles et tout ceux qui ont commenté, m'ont mentionnés, répondu sur un de mes comm, ou n'importe quoi <3 J'ai tout lu et ça m'a vraiment fait plaisir, même si j'ai pas encore répondu.

Pour faire, bref, un petit retour de l'Inktober : c'était cool, je recommencerais surement mais en mieux organisé. Je me suis jamais autant éclaté, et c'était vraiment très fun. J'ai aussi était au début du moins dans les Utopiales, et c'était bien cool, notamment grâce à une conf sur la procrastination chez l'écrivain.

Mais le soucis c'est que j'ai depuis la reprise un gros gros coup de mou : Je commence à me rendre compte que je me suis écarté des trucs où je suis le plus à l'aise, et je me suis mis à douter plus que jamais sur si c'était une bonne idée d'être instit (du genre je commence à me rendre compte que mes soucis de stress et de tendance  à la dépression ça devient assez violent dans ce genre de cas (du genre je vis mal quand un élève à des difficultés, je m'en sens coupable, et même en rationalisant ça change rien), ça plus le fait que j'ai vraiment du mal avec les cadres de l'EN, et que je suis une daube en gestion de classe). Breeef, c'est pas la joie.

Résultat, c'est un peu déprimant, et j'ai un peu l'impression d'être face à un mur x) Avec un peu de chance, le stage que je commence aujourd'hui va changer la donne, mais j'en doute.

Mais bon, j'ai déjà commencé aussi à chercher des voies de sorties. De plus, ça m'a donné un coup de fouet sur le fait que j'avais énormément stagné sur le plan du social - alors que ça fait des années que je dis que je vais améliorer ça - et que du coup ça m'a donné l'envie de me donner un coup de pied au fessier sur cet aspect-là (je vais même chercher sur internet des tutos sur comment aller parler aux gens, wesh */décède*). Donc, tout ne vas pas mal non plus :)

Bref, tout ça pour dire que : j'ai avec tout ça pris du retard sur quasiment tout les sujets (ESPE, commentaires, écrits), et je m'excuse de ne pas répondre comme ça à vos messages, qui me font extrêment plaisir !

Bon, du coup ce journal n'est peut-être pas si petit que ça et j'y ai passé prêt de 45 minutes.
