---
title: Roadmap
date: 2016/03/29 14:39:00
categories: Pensées
---
Voici la liste de mes projets ! Écrit tout en stressant, parce que demain, 14h, je m'inscris à l'ESPE de Nantes, et y'a peu de place donc c'est rempli en genre 15 minutes :') Et j'ai un peu la frousse de me reprendre dans la tronche les soucis de l'an dernier.

<!-- more -->

Je ne dis pas que je ferais rapidement voir même dans un moyen terme tout ce qui est marqué dans cette liste. Le nombre de projet que j'ai listé ici est assez énorme, parce que c'est le résultat de mon gros ménage, et c'est du coup souvent du long voir très long terme. Et pour être honnête, tout ce que j'ai dans mes dossiers n'est encore pas listé, puisque j'ai des "bouts de projets" et des "idées" qui seront sans doute recombiné pour donner de nouveaux projets, ou fusionné à certains de ces projets. Du coup en fait peut-être que dans le futur, des projets en WIP voir fini vont popper d'un coup, juste parce que j'aurais trainé dans mes vieux trucs et bossé d'une traite un petit truc à partir de ça xD De même, ce n'est pas parce qu'un truc est en "WIP actif" que ce sera le premier truc que je finirait.

Alors, le plus gros changement que j'ai fait est le status d'Erratum. J'ai déjà séparé Erratum de l'histoire de l'Empire des Cochons. Ce premier sera retravaillé en un JDR inspiré d'un système D20 en simplifié, tandis que le second servira de base à un Shmup style NES. Pleins d'autres petits trucs seront réutilisé dans d'autres histoires, cependant, notamment au niveau de mes personnages :) Les deux projets ne sont pas en WIP actif en ce moment, étant donné que j'essai vraiment de finir au moins une partie de mon ménage au niveau de mes nouvelles et textes courts.

La première étape là est que j'essaie de finir mes nouvelles. Cela fera déjà un gros nettoyage et me permettra de me lancer dans le reste l'esprit tranquille ! Dans ces nouvelles, quelques unes sont rescapées de l'époque ou Erratum devait être un projet littéraire. Notamment toute l'histoire de la fin du clan de Katos, qui va être une nouvelle de bonne taille, j'en suis pour l'instant à 19 pages et 10 000 mots xD (je pense que je vais le poster en plusieurs parties...). La voie du succès est un remake d'une de mes premières nouvelles (en storage à présent et à jamais :p). Le Livre Noir et la Tour du Magicien sont lié, le second étant la suite du premier. Sinon, le Prix de la Paix est une nouvelle de SF, Impasse aussi, et Reset est plus lié à "Le Dieu Corrompu".

Sur le long terme, c'est plus compliqué. Je compte profiter le plus possible de tout ce qui est "faire une réalisation dans un temps limité" pour me booster en motivation et me concentrer sur des projets. Je compte notamment écrire l'un de mes romans non-commencé et sans texte déjà écrit (donc soit Panthéon, soit Noosphère) lors du NaNoWriMo de 2016. J'avais déjà songé participé à celui de 2015, mais les circonstances m'en ont empêché. Peut-être l'autre de mes romans non-commencé sera pour celui de 2017 ^^ ? Manque de bol, la NaNoRenMo, l'équivalent pour les Visual Novel se déroule en Mars, dix jour pour faire sérieusement le projet, c'est trop peu. Peut-être l'année prochaine, pour Upgrade, mon hybride "jeux d'aventure"-"visual novel" ? Je verrais, j'admet que j'ai tellement d'idée possible pour gérer que celui là que je suis pas sûr de savoir en un an ce que je veux vraiment en faire :'D D'ailleurs, si vous connaissez d'autres trucs de ce genre, n'hésitez pas à me prévenir xD Ce genre de machin ça aide à se concentrer sur un projet, à avancer.

Pour mes autres projets "plus long", ce sera sans doute au feeling, histoire de voir comment je ferais et tout ^^

Légende :
- Posté :bulletblue:
- Fini mais pas encore posté (généralement il me reste à relire xD) :bulletgreen:
- En cours / WIP actif :bulletyellow:
- Commencé / WIP peu actif  :bulletorange:
- En réflexion :bulletred:
- Pas encore commencé :bulletwhite:
- Inactif, doit être entièrement retravaillé. :bulletblack:

Nouvelles
:bulletblue: Zoomorphes
:bulletgreen: Le Dieu Corrompu

:bulletyellow: La Fin d'un Clan (comment le clan de Katos s'est fait massacrer)
:bulletorange: La Voie du Succès
:bulletred: Le Livre Noir
:bulletred: La Tour du Magicien

:bulletblack: Le Prix de la Paix (réécriture)
:bulletblack: Impasse
:bulletblack: Reset

Romans
:bulletorange: Fragment(s), de l'aube à la fin des temps (Roman court de fantasy, se passant dans un monde ou les personnages connaissent leurs statut.)

:bulletwhite: The Shifter, celle qui traversait les mondes (Roman basé sur The Shifter / Jenny Everywhere, basé sur des voyages à travers les mondes)
:bulletblack: Noosphère, la nouvelle frontière (Roman de SF très inspiré du numérique, se passant dans une ville-cité en Antarctique. C'est là ou devait apparaitre Vox, mon petit perso IA)
:bulletblack: Pantheon, le cercle des initié (Roman type "initiation", inspiré . Se passe dans un système solaire ou chaque personne possède une "classe" correspondant à une créature ou un être mythologique, et plus cet être est haut dans la hiérarchie de la magie, plus la personne est haute dans la société. Tout étant dirigé par le "Panthéon", composé de personne qui ont comme classe les Dieux Grecs.)
:bulletblack: La Guerre Spectrale (Roman de fantasy + un peu de steampunk : Raconte l'histoire de Lloyd Patterson, un jeune exorciste d'une grande famille de médium, qui va devoir se retrouver à résoudre une affaire de fantôme dans des colonies d'iles flottantes dans une athmosphère toxique, à la population prolétaire. S'en suivra moult péripétie)

Light Novels / Romans courts
:bulletblack: Project MAGICAL / Werenia (Un projet de fiction mi-sérieuse, mi-humoristique)

Jeux Video
:bulletorange: Imperium Porcorum (shmup avec dimension RPG, ou vous jouez un cochon qui est censé envahir une planète)
:bulletred: Bootleg (RPG style 8bit au tour par tour, inspiré de l'esthétique des glitchs, et des bootlegs sur NES)

:bulletblack: This World (RPG ou vous ne pouvez rien attaquer, et ou le monde est contre vous. :3)
:bulletblack: Upgrade (Jeu d'aventure/Visual Novel.)
:bulletblack: Go Away Zombies, I'm late (Run 'n Gun en vue du dessus, ou vous jouez un écolier en retard dans un monde post apocalyptique pleins de zombies)

Erratum (JDR)
:bulletorange: Création du système de jeu (D20 Simplifié)
:bulletred: Adaptations des éléments existant dans le système de personnages
:bulletred: Adaptation du monde et du contexte
:bulletwhite: Adaptation du bestiaire
:bulletblack: Création d'une section "pour aller plus loin"
