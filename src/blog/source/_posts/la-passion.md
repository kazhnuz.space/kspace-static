---
title: La passion
date: 2018/08/12 20:48:00
categories: Pensées
---

Parfois, je fatigue un peu de voir des gens balancer comme ça d'un artiste (ici c'était une équipe d'écrivains de jv qui était visé) « il a plus la passion » pour le critiquer, pour chaque truc que *eux* aime moins.

Attention, je ne critique pas le fait de dire qu'on trouve que la qualité s'est amoindrie, qu'on aime moins, qu'on trouvait qu'un artiste faisait mieux avant, qu'il était plus original. Ça c'est du fair game tant que ça part pas en attaque. Ce qui me gène, c'est le fait de voir une « **baisse de passion** » quand ça nous plaît moins à nous, et de considérer cette baisse de passion comme une sorte de **faute** de la part du créatif.

<!-- more -->

Mon premier soucis est que de l'extérieur c'est assez dur de juger la motivation.

Ça m'est arrivé parfois d'être hyper excité sur un texte qui en fait n'a intéressé personne et a paru banal pour les autres. Et je doute être le seul dans ce cas : Parfois, la motivation et la passion qu'on met dans un truc n'est  pas visible par les autres.

La raison pour cela est simple : c'est plus difficile de voir la passion dans un truc qui nous passionne moins. C'est tout bête, mais si la passion n'est pas présente en nous, on ne sera pas capable de ressentir dans l'oeuvre finale celle de l'artiste, puisque les points dans lesquels iel a mis son intérêt ne sont pas ceux que l'on va remarquer. Dans ce cas, n'est-ce pas très égocentrique ou orgueilleux d'une certaine manière de se considérer comme sûr que la motivation/la passion n'est pas présente jusque parce que *nous* on ne la ressent pas ?

Ensuite, même si c'était ça, les baisses de motivations, les textes moins inspiré, ça arrive. Parfois, on fait un truc lors d'un petit passage à vide, on sait pas trop quoi faire, alors qu'on a des trucs à publier (manger c'est cool), un projet qu'on voudrait pourtant avancer. Un⋅e créatif⋅ve est humain⋅e, et du coup, bah parfois c'est pas le top du fun d'avancer sur un projet. Et c'est d'autant plus vrai qu'on doit gagner sa croûte.

Et parfois, ce qu'il faut pour se relancer c'est tenter autre chose, retrouver la forme, passer un moment chez des potes, trouver un truc trop cool qui nous inspire à fond…

Et ce qui m'embête, c'est que si on reproche aux artistes ce manque d'inspiration, ça ne va pas aider. Bah oui, parce qu'avoir tout le monde qui considère que t'as fait une faute morale de ne pas avoir été à fond ce jour là, c'est pas le top du fun. Et ça, pour moi c'est aussi un peu un signe de cette sorte de comportement chelou qu'on a envers les créatifs. D'un côté on glorifie leur talent, ce qu'il font... Mais à côté, "c'est pas un vrai métier", "tu fais ça pour la passion".

Pour faire bref : il n'y a aucune "faute" dans un manque d'inspiration lorsqu'on créer, par un manque de passion à un moment, par un passage à vide.

Et parfois, même en étant hyper motivé par un projet, on procrastine à fond.

( Comment ça, j'en suis la preuve ? )
