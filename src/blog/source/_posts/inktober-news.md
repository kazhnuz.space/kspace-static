---
title: Inktober et nouvelles
date: 2017/10/02 10:53:00
categories: Niouzes
---
Hey, ça fait un bail que je n'avais pas fait de petit post pour donner de mes nouvelles ! Et j'ai décidé de faire quelques changements dans ma manière de communiquer avec le monde, dans ma manière d'être présent sur internet.

<!-- more -->

Tout d'abord, j'ai décidé hier juste avant minuit de participer à l'Inktober de 2017. Un grand merci à Micku qui m'a fait remarquer la possibilité de faire l'Inktober à l'écrit (j'avais pas suivi la discussion sur le sujet xD). Je tenterais pour chaque jour de faire un texte d'au plus 650 mots, si possible dans des styles différents et des themes différents. J'utilise la liste officiel, traduite en français.

Ce n'est pas spécialement un secret, j'ai une légère tendance à la disparition spontanée, malgré des tentatives régulières de revenir et de plus participer. Je vais pas me chercher des excuses, même si ma formation me bouffe du temps, j'ai aussi tendance à perdre énromément de temps à strictement rien faire (regarder son plafond, une activité intellectuelle et spirituelle intense). Donc on va tenter de changer ça ! Du coup, je compte profiter de l'Inktober (et puis du NaNoWriMo) pour apprendre à gérer à la fois le boulot que j'ai à faire pour l'ESPE (et pour mes cours) et ma présence sur internet en tant que créateur, et pour me réhabituer à une participation régulière sur deviantART et autre.

Pour cela, j'ai décidé de retaper un peu mes différents outils de publication afin de me faire tout un petit cocon pour publier mes trucs. En plus de deviantART qui forme une gallerie, j'ai donc retapé kazhnuz.space pour en faire un blog créatif ! J'ai rajouté quelques trucs pour ça (notamment un article suivant/précédant même s'il semble bien buggué pour le moment xD). J'y publierais mes petits textes, que je publierais aussi sur deviantART. Aussi, du coup, j'ai décidé d'utiliser mes comptes sur les réseaux sociaux pour y partager mes textes… ce qui fait que je me suis finalement décidé à retourner sur facebook, pour y repartager et commencer à me réhabituer à plus parler aux gens, à mieux communiquer. C'est bête, mais quitter FB a bien participé à ma tendance à m'isoler.

Au niveau de mes projets, pour l'instant je me concentre surtout sur l'Inktober, le concours de Misical, et sur la préparation du NaNoWriMo. Voilà voilà !
