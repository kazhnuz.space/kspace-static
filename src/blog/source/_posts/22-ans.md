---
title: 22 ans 1 mois et 16 jours
date: 2015/03/16 12:00:00
categories: Niouzes
---
Hum, bref, ça fait un bail depuis mes 22 ans, et j'avais eut envie d'écrire quelque chose, mais en fait j'ai rien écrit et c'est donc maintenant que je donne des nouvelles.

J'ai pas été super présent sur internet ces derniers temps, j'ai râlé sur divers trucs sur Twitter, j'ai fait quelques messages sur quelques forums, mais a part ça, j'ai pas été super présent... La raison est toute simple : Je suis dans une période ou j'ai un peu de mal à me motiver, tout en étant en train de changer quelques trucs dans mon mode de vie (parce que bon, faut être honnête : c'était devenu un poil catastrophique xD)

<!-- more -->

Pour faire simple, pendant les grande vacances j'ai hésité entre faire une L3 science de l'éducation et un M1 Philosophie. Le M1 me semblait être "la voie de la sécurité" (ce qui en y repensant est pas malin du tout) et je l'ai choisit, parce que j'avais peur que changer de filière alors que mon changement de fac se passait super mal (par exemple je reçois mes bourses d'études que depuis quelques semaines, c'est vous dire comment ça a été amusant). Bon, le résultat a été sans équivoque : J'en ai eut marre une semaine après le début des cours, et je vais donc me réorienté en M1 MEEF Premier Degré, pour devenir instit o/

Bon, le résultat c'est que je suis du coup en train de foirer magistralement mon M1 Philo (pas de motiv + prépare une réorientation + fatigue = BOUM), ce qui est quand même pas super agréable, mais bon c'est pas super grave xD

Et donc à côté de ça, j'essaie d'améliorer un poil mon mode de vie, les trucs classique : dormir plus et mieux (donc ne plus faire du 3 heure du mat – midi), passer un peu moins de temps devant les écrans, mieux manger (parce qu'on va dire que l'aspect "végétal" de mon végétarisme est un poil pas super visible. xD Je suis plus en ce moment "nouillivore". Faites donc gaffe si je vous traite de nouille °O°.).

Résultat, je suis dans une période ou je cherche un peu mes nouveaux rythmes, et du coup j'ai tendance à être moins présent ^^ Cependant, cela veut pas dire que j'ai pas de nouvelles plus intéressantes que raconter ma vie !

Parce que j'ai avancé sur pas mal de trucs o/ Le dossier sur Erratum avance pas mal, mais je vais devoir accélérer les choses parce que je vais y intégrer un set de règles de jeux de rôles, qui vont servir de bases pour pas mal de truc xD Ce sera basé sur D&D 3.5 et Pathfinder ^^ Par contre, Fragment(s), mon roman de fantasy ou devrais-je dire de "metaphysic-fiction" se passant à l'intérieur d'un univers de fiction qui a découvert son aspect fictionnel n'avance pas super vite. J'ai pleins d'idée, mais je manque de temps pour bien écrire x.x A côté de ça, j'ai quelques petits trucs qui avancent bien, et qui se mettent en place :3 Je dépoussières quelques travaux terminés, et tente de leur donner une nouvelle jeunesse ^^

Je compte aussi un de ces quatre restructurer un peu quarante-douze, mon site web, pour tenter de présenter les différents trucs que je fais, et peut-être héberger quelques petits services en lignes
