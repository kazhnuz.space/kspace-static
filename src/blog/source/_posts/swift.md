---
title: Swift
date: 2017/10/02 09:45:00
categories: Pensées
---
Quand j’étais petit, j’ai régulièrement fait ce même cauchemar. Je courais, j’essayais d’éviter quelque chose, mais je ne faisais qu’allez de moins en moins vite. La lenteur me prenait, et j’avais l’impression que tout ce que je voyais au loin, tous les moyens de fuite étaient inaccessibles. Peut-être était-ce pour cela que je n’aimais pas trop courir, peut-être était-ce juste de la paresse.

<!-- more -->

Cependant, la vitesse gardait quelque chose d’attirant, malgré les faibles capacités de mes jambes. Être rapide, ce n’était pas simplement être dans une grosse voiture et jouer de l’accélérateur, non. Ce n’était non plus un intérêt pour les sportifs, qui à travers entraînements intensifs et produits chimiques avaient fait de leur vitesse leur métier. C’était rendre accessible ce qui était trop loin. C’était également la souplesse et la fluidité. Ce n’était pas temps la vitesse d’une fusée Ariane que ces mouvements souples et rapides qui en devenaient presque invisibles. Était-ce une attirance personnelle, où faisait-ce partie de ces nombreux fantasmes de l’enfance ?

Quand on est enfant, on court tout le temps. Il faut dire qu’on aime pas trop attendre. Courir, être rapide, c’est aussi le moyen de baisser la terrible, la funeste attente. On traîne des pieds pour éviter ce qu’on ne veut pas, mais on court vers ce que l’on veut. Même si ce n’était pas trop ce que je préférais, il m’arrivait de courir dans tous les sens quand j’attendais quelque chose. On nous dit d’attendre cinq minutes – c’est-à-dire une éternité. Mais nous, ce qu’on veut, c’est la suite. Mais ce n’était pas que de l’impatience. C’était aussi des sensations.

Dans la voiture, j’aimais par-dessus tout ouvrir la vitre. Je sentais le vent s’engouffrer, ébouriffer mes cheveux. D’un seul coup, je n’étais plus dans un véhicule incroyablement plus rapide que ma marche a pied, mais je faisais moi-même partie de cette sensation. Je regardais dehors, sur le côté de la route. J’imaginais quelqu’un avançait sur ces obstacles, ce jeu classique de l’enfance. Chaque élément du décor faisait alors partie de cette chorégraphie fantasmée, de ce ballet de mouvements imaginaires d’un personnage inexistant à travers des obstacles physiques. Un saut pour éviter un rocher, un tournoiement habile pour passer à travers une branche. Des flexions successives autour des poteaux qui défilaient à toute vitesse. À travers, ce jeu classique de l’enfance, c’était ce même attrait des mouvements rapides et souples, de la vitesse qui jouait.

Et le mieux, c’était d’avoir son propre véhicule. S’allonger sur une chaise de bureau – que dis-je, dans un fier et somptueux char – et courir pour se laisser emporter par l’inertie. On devenait d’un coup des fusées, capable de parcourir des distances incroyables, tels qu’un couloir et quelques salles. Se mettre sur un tourniquet, s’y accrocher pendant que des camarades le tournait. Dans ces moments, on était grisé par la vitesse. Dans ces moments, des sensations que l’on ne rencontrait pas dans la « vraie vie ».

Peut-être était-ce aussi ça : un des nombreux échappatoires que nos jeux et nos fantaisies nous offraient face à cette affliction qu’était le quotidien. Un de ces moments où il n’était plus question d’algèbre et de grammaire, de questions compliquées « réservées aux adultes » mais d’expériences et de plaisirs.
