---
title: Créer pour soi
date: 2020/02/27 11:57:00
categories: Pensées
---

Sur internet comme dans la vrai vie, on trouve de nombreuses "morales du quotidien", ces petits jugements qui rentrent un peu partout, et qui doivent selon moi être questionné. Parmi celles qui m'interroge beaucoup dans l'internet créatif, c'est le « il faut que tu créer pour soi ».

<!-- more -->

Avant de commencer, j'aimerais dire que je ne compte pas critiquer les gens qui vivent comme ça. Je ne vais pas dire non plus qu'il ne faut pas quand on créer faire ce qu'il nous plait. Chaque créateur⋅ice à son mode de fonctionnement, et ce n'est pas à moi de critiquer celui des autres.

Cependant, ce qui m'embête dans cette maxime est plus la manière dont à chaque fois qu'un.e créatif avait mal d'avoir peu de réaction, on jartait d'un geste de main son ressenti avec ça. Comme si cette souffrance ne comptait plus. Voir, plus génant, un peu comme si le fais de vouloir que ce qu'on fait plaise, de vouloir recevoir des commentaires, des réactions et tout était être superficiel, ne "pas avoir un vrai amour pour créer. Parfois, y'a une tentative de trop chercher à différencier la personne qui créerait « pour iel-même » (læ « bon⋅ne artiste ») d'autres qui le ferait « pour les comm/like » (læ « mauvais⋅e artiste »).

Déjà, pour moi y'a tout un nuancier entre deux extrême que serait « je fais ma création juste pour moi » et une vision caricaturale que serait « je ne fais ça que pour la fame ». Créer pour le plaisir de créer, de construire des choses n'est pas incompatible avec le plaisir de voir que des gens réagissent à ça. D'autant plus qu'il y a autant d'inspiration, de volonté de créer, de façon de penser la place de sa création dans sa vie qu'il y a de créatif⋅ve⋅s.

Chaque personne créer, construit, imagine... pour ses propres raisons, et je pense pas qu'il y en ai tellement de « mauvaise » (si ce n'est créer contre quelqu'un ? Créer pour exprimer de la haine et tout…).

Comme je l'ai indiqué plus haut, cette vision je l'ai souvent vu utilisée quand des personnes recevant peu de retour sur leur création dans des commu exprimait leur mal-être face à ça.

Si parfois effectivement y'avais ce côté "te pleins pas", je crois que bien des fois, c'était avec une volonté de bien faire, avec cette idée de "si on fait ça pour nous, alors c'est pas grave de pas avoir de réaction", comme pour trouver une solution au mal-être de la personne à travers l'idée de rejeter la source de la souffrance. Ce qui du coup tombait selon-moi souvent dans le cliché du "si t'as un mal-être c'est à toi de changer", comme pas mal de "conseils qui pensent bien faire face à un mal-être" (et oui, je n'hésite pas à parler de mal-être, parce que pleins de jeunes créatif⋅ve⋅s le ressentent comme ça).

Cela provoque selon moi deux soucis :

- Tout d'abord, augmenter ce mal-être. Non-seulement on se sent mal d'avoir peu de réaction, ce qui affecte la self-estime, mais EN PLUS bah on a honte de ce mal-être. C'est double peine, ce qui fait que cela n'aide pas du tout.

- Mais en plus de ça, a été fait une hiérarchisation entre "l'artiste qui le fait pour soi" et "l'artiste qui le fait pour les commentaires". Peut-être est-ce teinté un peu de l'image romantique de l'artiste qui le fait pour "l'amour de l'art" ? Je ne sais pas.

Et c'est là où j'ai un problème – en plus du fait que tout les « tu te sens mal ? Mais pense autrement ! » sont de très très mauvais conseils : en quoi serait-ce plus mal de créer pour les autres ?

Personnellement, cela me semble pas illogique de vouloir que ce qu'on créer plait. Parce que créer pour les autres, bah c'est créer aussi pour plaire, faire plaisir aux autres. C'est pour moi comme le plaisir de préparer un plat à quelqu'un, le plaisir que l'on a de voir ce que qu'on a fait a fait passer un bon moment à quelqu'un. Pour moi, ce n'est pas de la superficialité, mais juste une manière de créer **sociale**, en rapport avec les autres.

Une manière de partager un bout de soi, à travers des histoires, des créations artistiques, de la chanson... Et ça me semble beau, aussi ? Cela me semble beau, ce plaisir de voir que quelqu'un a passé un beau moment ? Et pas incompatible avec avoir passé un bon moment à créer. C'est la célèbre phrase : "j'espère que vous aurez autant de plaisir à *lire/regarder/écouter* ceci que moi à le créer". Et si je ne pense pas que créer pour soi soit un mal, je pense que créer pour les autres du coup aussi est très bien.

Et je pense que cela rend tout a fait compréhensible le fait de se sentir mal en ne voyant aucune réaction face à ce que l'on créer. Surtout que pour certaines personnes, cette création sociale est un moyen de combler des difficultés sociales autrement. Un moyen d'entamer la discussion, d'avoir quelque chose à dire, de s'exprimer.  C'est du coup très triste, et pas hyper sympa on va dire, de balayer tout ces ressentis avec une "solution miracle".

Pour conclure : je pense qu'il n'y a pas spécialement de mauvaise manière de créer, de chercher à passer un bon moment à créer. Je sais pas spécialement pourquoi j'ai fait ce thread, juste... ça m'a traversé l'esprit.

Si un jour vous avez eu ou avez encore l'impression que ce que vous faites n'a pas de valeur à cause d'un manque de réaction, ne culpabilisez pas en plus en vous disant que vous êtes superficiel⋅le. Vous ne l'êtes pas : c'est difficile d'avoir pleinement confiance en soi quand on créer. C'est normal que cela vous fasse mal, c'est tout a fait compréhensible que ce soit désagréable.

Il n'y a pas de "règle" parfaite pour nous dire quoi faire, on sait pas trop où on va, on fait des erreurs. Souvent en plus, on commence à une période où on manque déjà de confiance en soi. C'est pas facile.

Cependant, cela ne veut pas dire pour autant que c'est vrai que le manque de réaction indiquerait un manque de qualité de ce que vous avez fait. Bien des gens n'ont pas forcément le temps où l'envie de réagir, et même si c'est frustrant pour les créatifs, c'est aussi compréhensible de leur part (iels ont peut-être d'autres chose à penser).  Ça arrive, les gens ne peuvent pas toujours voir, et c'est difficile. C'est toujours difficile de commencer, et parfois longtemps.

Pour tout⋅e⋅s celleux qui suivent des artistes sur internet, cependant n'hésitez pas à laisser votre ressentit, un petit message pour indiquer que vous avez apprécié. Vous ferez des heureux⋅e⋅s.
