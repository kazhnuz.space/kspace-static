---
title: L'ombre des flammes
date: 2018/05/25 14:39:00
categories: Pensées
---

Aujourd'hui, la "newsletter" d'un vieux forumactif où j'étais membre et postait mes fanfictions m'a rappelé que j'avais écrit une fic il y a 9 ans avec comme sous-titre « dans **l'ombre** des **flammes** ».

<!-- more -->

Le pire c'est que je suis *certain* qu'à l'époque je devais être genre mais trop trop fier de ce titre, en mode "comment c'est trop profond mon titre, j'inverse le fait que les flammes éclairent pour montrer le danger qu'elles représentent". Mais aujourd'hui, je ne peux qu'être amusé face à cela, et me dire « wow, carrément ? » C'est parfois très drôle (et quand même un peu gênant/embarassant) de retomber sur des vieux trucs comme ça.

Une sorte de mélange entre de la gène, et un peu de tendresse pour ce vieux souvenir.

Je tiens à préciser : il ne s'agit pas de me moquer de ce que je faisais à l'époque et que d'autres personnes quelque part dans le monde font sûrement aussi, mais plus de m'amuser de ce qu'on peut ressentir en retombant sur de vieux travaux qu'on avait quasiment oublié.

Parce qu'en fait, c'est quelque chose de plutôt positif, selon moi, le fait que je ressente cela. Je pense qu'il n'y a aucune véritable honte à avoir de nos premiers travaux, tout imparfaits qu'ils soient, parce que c'est eux qui nous ont permis, en faisant des erreurs, des trucs pas hyper top, d'évoluer et de faire de mieux en mieux par la suite…

Personne n'est hyper-doué dès le départ sans rien avoir fait avant, personne ne découvre magiquement son style sans avoir à le travailler. C'est pour cela qu'il faut se lancer, et ne pas laisser la peur de se rater nous paralyser et nous empêcher de produire si on le veut. Attention : Je ne dis et ne dirais pas "il ne faut pas avoir peur de se rater", parce que c'est un sentiment, et avoir peur n'est pas anormal. Par contre, réussir à la dépasser (notamment avec de l'aide si besoin) est important pour réussir à avancer créativement.

Par contre, cette gêne qu'on peut ressentir par rapport à des vieux trucs est normale aussi. Et cela peut être pour certaines personnes rigolo de se dire "wooo punaise non j'avais pas fait ça, quand même". Pas pour tout le monde cependant, et encore une fois c'est normal. On a chacun nos ressentit et tout.

Cependant, je pense que tout cela, c'est aussi un moyen de se rendre compte à quel point on a grandis. Je pense que c'est important de ne pas avoir honte de ses débuts. Non, vous n'étiez pas pire que les autres débutants. Vous avez débuté, fais des erreurs, comme tout le monde. Je pense que vous n'avez pas à vous sentir « nuls » face à ces vieux souvenirs. Non, vous ne l'êtes pas. C'était normal.

C'est pour cela que ce titre m'a fait sourire.

C'est pour cela que j'ai une certaine tendresse pour mes premiers écrits. Je n'y connaissais rien. Je n'avais aucune idée de ce que je faisais. Mais je m'amusais. J'inventais mondes et personnages, et je les faisais vivre dans ma tête, puis par les mots.

C'était pas terrible.

Mais c'était ce qui m'a fait apprendre.
