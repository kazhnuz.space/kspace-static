---
title: L'épopée ordinaire
date: 2016/07/05 10:21:00
categories: Pensées
---
C’était une de ces nuits calmes et tièdes, rafraîchit par une petite brise. Ces nuits ou l’on aimerait rester debout, la fenêtre ouverte, pour profiter de ces instants de songes éveillés nocturnes. Un de ces moments qui semblait idéal pour l’introspection, pour voyager au cœur de son esprit. Le calme des soirées, les sons des animaux, c'était un peu la musique idéale pour réfléchir, pour se poser des questions. Le doux silence du crépuscule éclaire les pensées du rêveur solitaire, les rendant plus clair à sa propre conscience.

<!-- more -->

Parfois, dans ces nuits, un sentiment étrange peut nous parvenir : celui d'avoir accompli, en fin de compte, quelque chose. Ce genre de sensations qu'on devrait pourtant plus sans doute ressentir lors qu’après un long périple, on rentre chez soi. Celle d'avoir vécu une épopée. Mais nul voyage épique d'un Ulysse ou d'un Jason, juste le périple banal, celui que traversent les gens ordinaires moult fois dans leur vie. Un périple construit de péripétie, de coup de théâtres, de remontée héroïque après avoir touché le fond et de descentes aux enfers.

La vie apporte son lot de malheur, de souffrance et de difficultés. Quand on y pense, on ne reste jamais bien longtemps dans une situations paisible. C’est comme si toujours, une nouvelle péripétie venait déranger le statut-quo que l’on avait créé. Des changements dans son cercle d’amis. Les choses qui changent. Des pertes. Des malheurs. Parfois des bonheurs ?

En fin de compte, peut-être que le héros au mille visage, cet être qui a vécu toute les aventures sous des noms et des personnalités diverses, dans moult vies, est également monsieur tout-le-monde. Les atomes de notre histoire sont tout les souvenirs qui nous compose, tout ces instants qui ont participé à nous définir. Des histoires, qui ont fait partie de notre processus évolutif. Notre quotidien est une histoire, dont on est toujours protagoniste et co-écrivain.

Le voyage initiatique de chacun se forme à travers l'espace et l'esprit, chaque pas étant aussi une évolution, chaque évolution étant aussi un pas.

Des histoires d'amours banales, mais avec leur lot de rires et des chagrins. Des disputes. Des réconciliations. Des séparations. De nouvelles histoires.

L'envie de vivre ses passions, les difficultés que l'on rencontre. Les échecs. La persévérance. Ce moment où on peut enfin se dire "je suis fier de moi". Ceux où quoi qu’on n’y fasse, nous n’arrivons pas à être satisfait de nous. L'évolution de nos capacités. L’envie de tout abandonner. Et si nous n’y arrivions jamais ? Et si tout ce que l’on voulait était une erreur, un simple caprice que l’on aurait jamais du faire ? Peut-être aurions-nous dû être sage au lieu de vouloir « faire ce qu’on veut » ? Persévérer.

La haine ordinaire, les insultes du quotidien, juste parce que l'on est pas "comme tout le monde". Le rejet. L'acceptation. Devoir affronter sa peur, dire ce que l'on est. S’accepter. Il y a aura toujours dans le monde des gens pour nous refuser le droit d’être ce que l’on est. Il y aura toujours des gens pour ne parler que le langage de la haine, et dans l’étroitesse de leur esprit être les bourreau du quotidien, pervertissant le mot même de « morale » afin d’en faire une arme contre tout ce qui est différent d’eux.

La peur de perdre un proche. Cette perte quand elle arrive. Le deuil. La dépression, mal invisible qui nous ronge de l'intérieur. Se lever, se laver, manger, se préparer pour la journée. Tenir lorsqu’on a qu’une seule envie, c’est de se rendormir pour ne plus jamais se réveiller. Se donner quelque claque : Déjà finir cette étape, passer la journée. Quelques entrées dans un logiciel servant de liste des tâches à accomplir. Jeter un coup d’œil. Encore tout ça à faire ? Se décourager. Tenter l’auto-motivation. Échouer. Se dire que de toute façon on a pas le choix.

Cet instant, ou l'on pense avoir perdu, que tout est fini, mais ou on décide de ne pas en finir avec notre vie, par espoir que ça aille mieux ou pour ne pas faire pleurer les siens. Ce même moment, ou le sort ou un ami nous arrête. Ces échecs, ces histoires qui se terminent trop tôt. Il est toujours trop tôt pour conclure une histoire.

Ces instants où la force de nous reconstruire nous vient, et petit à petit retrouve une joie de vivre, ou une volonté de continuer.

Les moments où l'on retombe. Ceux où on se relève. Les mains tendues.

Telle est l’épopée ordinaire, l’épique banal de ceux qui ne sont ni des "super-héros", ni s'estiment nés de divinités modernes. Des vrais gens. Des êtres normaux, sans rien d’extraordinaire. Nul aède ne chantera nos louanges, mais tout ce que nous construirons, tout ce que nous apporterons aux êtres dont les histoires croiseront les nôtres seront une trace indélébile. Une marque qui participera à façonner, à apporter notre pierre dans l’édifice de la véritable histoire, celle foisonnante et riche des vies ordinaires.

Je jette à nouveau un regard par la fenêtre. La lune éclaire doucement les arbres, les routes. Il fait calme. Il fait frais. Je repense au passé. Parfois il est joyeux, parfois il est triste. Parfois il est léger, parfois il est dur. Il est rempli de frousses, de hontes, de ratages totaux, de grosses conneries et de regrets. Mais aussi de rires et de joies, de bons moments qui me font sourire, parfois teintés d’un goût doux-amer. Il m'a façonné, sans lui, je ne serais pas qui je suis. Il est mon épopée personne, une parmi tant d’autre, ni pire, ni mieux qu’une autre. Une histoire anonyme, noyée dans la foule, mais qui est importante. Comme tout les autres. Nous sommes tous protagoniste d’une histoire : la notre. Et tous nos proches en sont les personnages principaux, ceux qui en font la substance. Les co-auteurs de notre histoire, qui ont participé à créer ce qu'on est. Sans mes amis, ma famille, mes amours et mes relations plus complexes, parfois plus conflictuelles, je ne serais rien. Sans toutes ces histoires infiniment importantes qui ont croisés la mienne, ma propre histoire n’aurait jamais pu se construire.

Mais ce long périple banal prenait-il sa fin aujourd'hui, dans cette sensations de résolution ? Non, il continuerait. La fin de l'histoire n'existe pas tant qu'il y a le moindre personnage encore en vie. Quand je vois tout ce que j'aimerais encore faire dans ma vie, tout ce que je n'ai pas fait, cela me fait toujours un peu peur. Cependant, avoir peur n’est-il pas aussi le signe que nous avons de l’espoir ?

Je me lève et vais fermer la fenêtre. Il est temps d'aller dormir.

Demain sera un autre jour.

Et il y aura des choses à faire.
