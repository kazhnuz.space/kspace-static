---
title: rebuild.
date: 2020/03/09 10:00:00
categories: Niouzes
---

Rebuild. Reconstruire.

<!-- more -->

Cela fait un moment que je n'avais pas fait plus sur internet que de squatter des canaux public de chan Discord pour faire suer le monde avec de la philo (je suis toujours aussi incapable de parler aux gens malgré les 300 000 fois où j'ai stupidement dit "nan mais cette fois c'est la bonne, je vais résoudre tout mes soucis et réussir à lancer une discussion normal") ou à raler sur twitter.

Les tendance à l'isolement, ce n'est pas quelque chose qui se gère comme ça. Il y a quelques années, j'ai compris que ce n'était pas en bourinant intellectuellement que je pourrais résoudre ça. Cependant, j'ai pris eu une attitude classique de ma part, j'ai "tenté d'être plus malin". Sauf qu'être plus malin que soi-même mon coco, ça marche pas super bien. Surtout que ça a été un cercle vicieux ensuite : après tant de fois où j'ai dit que c'était bon, j'allais me remettre à parler aux gens, j'ai commencé à flipper. Et si on m'en voulait ? Plus la culpabilisation d'avoir échoué plusieurs fois un objetif que je m'étais donné. Bref, le fun :V

Cependant, un truc m'a beaucoup aidé : le JDR. Si j'ai du mal à aller parler "comme ça", même aux gens que j'adore, en fait tout ce qui m'apporte une raison de le faire étant plus que "parce que j'aurais envie" m'aide à le faire. Parce qu'en gros, la raison me permet de dépasser plus facilement mon "nan mais je dérangerais"  constant, plus le fait que des gens s'amusent qui font que je me sens moins inutile.

De plus, grace à tout ça, en arrière plan, j'ai été occupé, et j'ai préparé beaucoup de choses.

- J'ai terminé la version 2 de Rulebook, un système de règle de JDR, pour l'instant encore très foireux et bourré de soucis d'équilibrage (mes boss se font laminer un truc de fou). La version 3 commencera à être bossé (pour mes futurs jdr), avec mon premier JDR de test fin Avril sur un scenar bien débile o/

- Lié à rulebook : On est rendu avec des potes IRL et ma fratrie au premier tiers de la saison 2 de DanseRonce, mon JDR de Fantasy :D

- J'ai refondé tout le scenar d'Ailleurs, ma saga de Nouvelles racontant l'histoire d'un trio d'idiot voyageant à travers les mondes pour sauver des gens.

- Je suis en train de transformer tout mon blog en espace perso "statique" (en gros tout va être géré par fichier). D'ailleurs, je vais apprendre à tenir un vrai blog ! Où je parlerais aussi bien de trucs perso que de réflections philosophique sur la vie, la création, etc.

- J'ai refait ma page de profile toyhouse ( https://toyhou.se/kazhnuz ) et je suis en train de me developper un petit outil pour pouvoir écrire mes fiches de persos dans des fichiers .txt et les transformer à la volée en fiche pour toyhou.se avec le html et tout.

Maintenant, je suis sur le point de relancer mon blog, de partir vers un nouveau point dans ma vie. Se pose la nouvelle question : que faire ? J'ai un soucis à régler depuis des années, de nouveaux objectifs, diverse passion.

Je compte lancer un nouveau JDR (même si je ferais jamais plus de deux campagnes simultanément xD), notamment un petit que j'aimerais faire en ligne avec des potos online via Discord (d'ailleurs si des gens sont intéressé, n'hésitez pas :3). Et chercher toujours comment avoir une sociabilité normale et ne pas rester une sorte de bestiole restant reclu dans son terrier :V

Comme on dit, advienne que pourra !
