---
title: Ailleurs et autres news
date: 2018/10/21 12:00:00
categories: Niouzes
---

Oui je fait mon post pour parler de mon Inktober le 21, alors que y'a six jours de retard. TOUT VA BIEN.

<!-- more -->

Breef, je viens un peu dire des nouvelles, comment ça se passe et tout ! o/ Je ferais comme les dernières fois, je commence par une partie plus "update de ma situation" avant de parler de tout ce qui est projet.

Tout d'abord, sur le plan personnel, j'essaie de me préparer à améliorer tout ce qui est social et tout (notamment avec de l'introspection pour mieux cibler l'origine de ma peur de déranger xD). Donc une fois que j'aurais arrêté de tourner autour du pot avec des "préparation de préparation de préparation", normalement je commencerais par devenir plus actif et recommencer à venir parler sur les trucs de messageries et tout. Vous inquiétez pas je vais pas venir parler de Linux ou de consoles de jeux obscures, juste de mes animaux.

Sinon, je suis toujours en recherche d'emploi et de logement, et ça commence à devenir un peu serré xD Une grosse partie du soucis en fait c'est que je n'ai pas le permis ce qui provoque une boucle de : "il me faudrait un logement pour avoir un boulot" (dans le sens où même si j'ai des possibilités d'hébergement temporaire sur Nantes, les employeurs sont plus réticent) et "un boulot c'est plus pratique pour payer le logement". Donc je tente de voir avec les différents interlocuteur et tout pour ça, c'est le fun.



Sinon du coup, je suis en train de réfléchir à suivre une formation sur des trucs genre site web et un peu de développement (par contre il va falloir que j'aille voir pour la faire financer par Pôle Emploi) histoire de valider mes acquis en autodidacte, ce qui fait que du coup je suis en train de retaper mes sites, histoire de retourner sous Wordpress o/ (et du coup j'ai revu genre mes premiers themes wordpress et j'ai bien cringe). Du coup Quarante-Douze va avoir un tout nouveau design plus "magazine", tandis que kazhnuz.space va être entièrement retravaillé avec un theme ajoutant des fonctionnalités de galleries.

L'idée pour kazhnuz.space ce serait de créer un theme qui profite à fond des fonctionnalités de wordpress, notamment le fait de pouvoir rajouté ses propres "taxonomies" (en gros des types de catégories) et types de postes (grossomodo je pourrais faire des news, des "contenus créatifs", des "fiches techniques"/"dossier", des "personnages) histoire de créer un gros espace qui permettrait d'y mettre à la fois ce que je posterais aussi sur deviantART et sur toyhou.se.



Du coup, qu'est-ce que je vais poster (vous avez vu ces transitions de ouf ? :D) ? Un certains nombre de trucs. Mes gros projets sont pour l'instant un peu "en pause", puisque j'essaie de bosser sur des trucs plus "possible à court terme".

Niveau roman, je continue de bosser sur Ailleurs (dont je lancerais la partie "je dois tout améliorer" une fois l'Inktober terminé). Donc, qu'est-ce que Ailleurs précisément ? Ailleurs est un projet d'hybride roman/série de nouvelle se passant dans mon multivers. Le but est de servir d'introduction à quelques concepts qui vont avoir des répercussions sur tout mes univers. C'est pour ça que j'y ai placé quelques éléments comme la Corruption, le Paradoxe et quelques personnages qui reviendront comme Norman. Certains des éléments font aussi référence à des projets futurs (j'ai ma petite liste de trucs à référencer :p), mais le but premier est de raconter une histoire dont les plot-line seront terminé au chapitre/épisode 31.

Après Ailleurs, j'aurais un second projet : Nanites. Il racontera l'histoire d'une poignée de jeunes adultes (genre "on-sort-de-fac") qui se retrouveront à supperviser la terraformation de Mars… qui doit durer plusieurs millier d'année. Cela fait que ces jeunes superviseur seront "robotisé" avec l'utilisation de nano-machines. Le corps composé de nano-machines pouvant se modifier à l'infini, resistant au choc et modulable, il semblerait que les limitations de la condition humaine ne soit pour eux qu'un souvenir…
Mais est-ce une bonne chose ? Est-ce qu'il est possible pour l'esprit de supporter vivre aussi longtemps ? Et est-ce qu'ils ont véritablement un avenir après la terraformation ? Le projet visera à explorer leur relations, leur doutes, leur peurs, et qu'est-ce que peut se passer dans une telle société… tout en s'amusant un peu des possibilités cartoonesque qu'offre un tel univers, pour des moments plus léger.



Niveau jeux vidéo, j'ai un projets en cours de développement, et deux en cours de préparation :

- Imperium Porcorum, mon plateformer en style Master System avec des cochons, est en train de voir son code porté sur de nouvelles bases (c'est toujours du love2D), histoire de rendre le tout plus propre. Je vais bientôt passer tout le développement sur code.kazhnuz.space histoire de rendre plus visible les évolutions et de bosser mieux.

- Æstrus est mon projet de RPG web, inspiré de sites comme Woltar. Vous y contrôlerer un ou plusieurs membres des espèces qui vivent sur la planète Æstrus, et pourrez choisir entre une vie d'aventure où faire votre petite popote de votre côté. Le but du site sera d'être fun et sans prise de tête, et d'encourager les interactions entre membre et le RP. Pour la petite histoire, la race des Raginh vient de ce projet, et Katos vient de cet univers :) Ce projet est lié à mon univers nommé Radiant Skies, un univers dont je parlerais un de ces quatres. (le projet est encore loin par contre d'avoir sa préparation fini, et je compte bosser dessus qu'une fois mes autres projets web finis xD)

- Hesperus (nom provisoire) est un RPG de monster-capture en style Master System (oui j'aime cette console), inspiré de Pokémon mais aussi de titre comme Bravely Default, EarthBound… mais également des vieux fangame Pokémon sous RPG Maker 95. Vous y controlerez un jeune Ranger ayant pour but de protéger les gens des Mirages, créatures qui apparaissent dans son monde. Vous controlerez en combat votre héros et des monstres capturés, votre héros pouvant adopter l'une des huit classes du jeu et étant le seul ayant accès à l'inventaire.

Bref, comme vous voyez, cela fait pas mal de petits projets… mais bien moins que ce que j'ai eut à une époque, et surtout la plupars des projets sont moins ambitieux (sauf sur le plan de l'histoire où j'essaie toujours de faire des histoires cool, même Æstrus y'aura des trucs d'histoire et tout). Mes autres univers (Nouvelle Frontière, Erratum) existent toujours et ne sont pas abandonné, c'est juste que je pense qu'adopter une meilleur méthode de gestion de mes projets et commencer par bosser par des plus petit projet est plus sage pour le moment ^^

J'essaie d'adopter des méthodes de gestion du temps et du travail type Getting Things Done, Agile, etc, histoire d'apprendre à mieux gérer mes projets et tout. J'ai pendant longtemps eut tendance à m'enfermer dans des projets hyper ambitieux qui n'était pas réaliste à des échelles possible pour moi, du coup j'essaie d'améliorer ça.
