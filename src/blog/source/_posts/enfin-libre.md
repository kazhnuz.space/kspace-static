---
title: Enfin libre !
date: 2015/05/04 12:00:00
categories: Niouzes
---
Et voilà, une semaine avant le rendu final, j'ai rendu ces §$£%# de mini-mémoires (bon, j'ai rendu des trucs minables, puisque de toute façon je me barre de la philo :') Ce sera un départ en beauté.). Mon année est donc terminée puisque je n'ai pas de partiels (oui, moi aussi ça me fait bizarre), et que j'ai récupéré mon diplôme du C2i. Il me reste plus qu'à régler des trucs niveau de l'année prochaine pour le crous, puis je devrais attendre de savoir dans quelle ville, je ferais mon master MEEF Premier Degré (pour devenir instit). Y'a de forte chance que je sois sur Le Mans ^^ Il me reste également à ranger et nettoyer ma chambre étudiante, j'essaie de rendre ma chambre étudiante avant le 31 mai, ce qui devrait être plutôt faisable.

<!-- more -->

Sinon, comme j'ai enfin fini cette année, je peux me remettre plus sérieusement à mes projets ! Niveau d'Erratum, je suis en train de bosser sur deux trucs à la fois : Déjà, je bosse sur la première nouvelle de la première saga (Dreieacht), qui présentera la chute de la Fédération d'Astrus face à l'empire des cochons ^^ Mais j'avance aussi sur les documents qui servent de base au projet, là plus précisément sur les différents groupes religieux, ordres et guildes. J'étais un peu en "pause" dût à divers trucs, mais avec la fin de cette année, je vais pouvoir avancer plus vite !

Sinon, j'avance également sur Fragment(s), un projet de roman avec un univers moins gros que Erratum, mais ou je m'amuse plus avec tout ce qui est méta et tout ^^ Je met aussi sur papier – ou devrais-je dire sur document opendocument – quelques idées pour des projets futurs, notamment un peu de SF et quelques projets de jeux. Peut-être que pendant cet été, je me lancerais des défis de faire des petits projets de jeux pas très gros pendant des temps limité, histoire de retirer toute la rouille que j'ai du accumuler en dev ^^

Voilà pour les news ! Et vous, ça va comment =3 ?
