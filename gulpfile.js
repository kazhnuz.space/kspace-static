const { src, dest, parallel } = require('gulp');
const include = require('gulp-include');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

function html() {
  return src('src/static/*.html')
    .pipe(include())
    .pipe(dest('dist'))
}

function htmlunivers() {
  return src('src/static/univers/*.html')
    .pipe(include())
    .pipe(dest('dist/univers'))
}

function css() {
  return src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('dist'))
}

function js() {
  return src('src/js/*.js')
    .pipe(dest('dist/js'))
}

function dep() {
  return src(['src/dep/**/*'])
    .pipe(dest('dist/dep'));
}

function assets() {
  return src(['src/assets/**/*'])
    .pipe(dest('dist'));
}

function univers() {
  return src(['src/univers/**/*'])
    .pipe(dest('dist/univers'));
}

exports.html = html;
exports.css  = css;
exports.js  = js;
exports.dep  = dep;
exports.assets  = assets;
exports.default = parallel(html, css, js, dep, assets, htmlunivers, univers);
